#!/usr/bin/env python3

import itertools
import os
import os.path
import subprocess
import sys


NUM_PASSES = 3

RTREE_VARIANTS = ['quadratic', 'rstar']
QUERIES = [
    (
        'weighted_avg_pressure_rtree',
        'calculate sums and distances',
    ),
    (
        'satellite_path_hourly_rtree',
        'get satellite locations',
    ),
]
SHUFFLED = [False, True]


if len(sys.argv) < 2:
    print('Usage:', sys.argv[0], 'db-file', file=sys.stderr)
    sys.exit(2)
else:
    db_file = sys.argv[1]

if not os.path.isfile(db_file):
    print('Invalid file:', db_file, file=sys.stderr)
    sys.exit(2)

for rtree_variant in RTREE_VARIANTS:
    proc = subprocess.run(
        ['cmake', '-DRTREE_VARIANT={}'.format(rtree_variant), '.'],
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    if proc.returncode != 0:
        print('error with cmake', file=sys.stderr)
    proc = subprocess.run(
        ['make', '-j', 'mipas-queries'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    if proc.returncode != 0:
        print('error with make', file=sys.stderr)
        sys.exit(1)

    for shuffled, (query, query_time_label) in itertools.product(SHUFFLED, QUERIES):
        print(
            'testing variant={}, shuffled={}, query {}'
            ''.format(rtree_variant, shuffled, query),
            file=sys.stderr, flush=True
        )
        for p in range(NUM_PASSES):
            mipas_queries_args = ['./mipas/mipas-queries', db_file, query]
            if shuffled:
                mipas_queries_args.insert(1, '--shuffle')
            proc = subprocess.Popen(
                mipas_queries_args,
                stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, universal_newlines=True
            )
            stdout, stderr = proc.communicate()
            if proc.returncode != 0:
                print('error with mipas-queries', file=sys.stderr)

            data = {}
            for line in stderr.splitlines():
                if ':' not in line:
                    continue
                name, _, time = line.rpartition(':')
                data[name.strip()] = time.strip()

            try:
                insert_time = data['load data into rtree'].rpartition('μs')[0]
                query_time = data[query_time_label].rpartition('μs')[0]
                fields = [query, rtree_variant, int(shuffled), p, insert_time, query_time]
                line = ';'.join(str(f) for f in fields)
                print(line, flush=True)
            except (KeyError, IndexError) as e:
                print(stderr)
                print('error:', e, file=sys.stderr)
                sys.exit(1)
