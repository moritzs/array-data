#!/usr/bin/env python3

import itertools
import os
import os.path
import subprocess
import sys


NUM_PASSES = 3
R_VALUES = [3000, 4000, 5000]
B_VALUES = [5]

if len(sys.argv) < 2:
    print('Usage:', sys.argv[0], 'db-file', file=sys.stderr)
    sys.exit(2)
else:
    db_file = sys.argv[1]

if not os.path.isfile(db_file):
    print('Invalid file:', db_file, file=sys.stderr)
    sys.exit(2)

for b, r in itertools.product(B_VALUES, R_VALUES):
    if b > r:
        continue
    print('testing r={}, b={}'.format(r, b), file=sys.stderr, flush=True)

    env = os.environ.copy()
    env['MAX_TUPLES_IN_INTERVAL'] = str(r)
    env['MAX_TUPLES_BASE'] = str(b)
    proc = subprocess.run(
        ['make', 'clean'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    if proc.returncode != 0:
        print('error with make clean', file=sys.stderr)
    proc = subprocess.Popen(
        ['make', '-j', 'mipas-queries'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        env=env
    )
    proc.wait()
    if proc.returncode != 0:
        print('error with make', file=sys.stderr)

    proc = subprocess.Popen(
        ['./mipas/mipas-queries', db_file, 'insert_only'],
        stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, universal_newlines=True,
        bufsize=1
    )
    for line in iter(lambda: proc.stderr.readline(), ''):
        if not line.startswith('split occurred at dimension '):
            continue
        line = line[len('split occurred at dimension '):]
        dim, _, line = line.partition('with')
        tuples = line.partition('tuples')[0]
        print('{};{};{};{}'.format(r, b, dim.strip(), tuples.strip()))
    proc.wait()
    if proc.returncode != 0:
        print('error with mipas-queries', file=sys.stderr)
