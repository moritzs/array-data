#!/usr/bin/env python3

import itertools
import os
import os.path
import subprocess
import sys


NUM_PASSES = 3
R_VALUES = [100, 1000, 5000, 10000, 20000, 50000, 100000, 150000, 200000]
B_VALUES = [5, 10, 50, 100, 200, 500, 1000]


queries = {
    'weighted_avg': (
        'weighted_avg_pressure_sparse_storage',
        'calculate sums and distances',
    ),
    'satellite_path': (
        'satellite_path_hourly_sparse_storage',
        'get satellite locations',
    ),
}

shuffled = False

if len(sys.argv) < 2:
    print('Usage:', sys.argv[0], '[query] db-file', file=sys.stderr)
    sys.exit(2)
elif len(sys.argv) == 2:
    query = 'weighted_avg'
    db_file = sys.argv[1]
else:
    query = sys.argv[1]
    db_file = sys.argv[2]

if query.endswith('_shuffled'):
    shuffle = True
    query = query[:-len('_shuffled')]

if query not in queries:
    print('Invalid query:', query, file=sys.stderr)
    sys.exit(2)

if not os.path.isfile(db_file):
    print('Invalid file:', db_file, file=sys.stderr)
    sys.exit(2)

query, query_time_label = queries[query]

for b, r in itertools.product(B_VALUES, R_VALUES):
    if b > r:
        continue
    print('testing r={}, b={}'.format(r, b), file=sys.stderr, flush=True)

    env = os.environ.copy()
    env['MAX_TUPLES_IN_INTERVAL'] = str(r)
    env['MAX_TUPLES_BASE'] = str(b)
    proc = subprocess.run(
        ['make', 'clean'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )
    if proc.returncode != 0:
        print('error with make clean', file=sys.stderr)
    proc = subprocess.Popen(
        ['make', '-j', 'mipas-queries'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        env=env
    )
    proc.wait()
    if proc.returncode != 0:
        print('error with make', file=sys.stderr)

    for p in range(NUM_PASSES):
        mipas_queries_args = ['./mipas/mipas-queries', db_file, query]
        if shuffled:
            mipas_queries_args.insert(1, '--shuffle')
        proc = subprocess.Popen(
            mipas_queries_args,
            stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, universal_newlines=True
        )
        stdout, stderr = proc.communicate()
        if proc.returncode != 0:
            print('error with mipas-queries', file=sys.stderr)

        data = {}
        for line in stderr.splitlines():
            if ':' not in line:
                continue
            name, _, time = line.rpartition(':')
            data[name.strip()] = time.strip()

        try:
            insert_time = data['load data into storage'].rpartition('μs')[0]
            query_time = data[query_time_label].rpartition('μs')[0]
            print('{};{};{};{};{}'.format(r, b, p, insert_time, query_time), flush=True)
        except (KeyError, IndexError) as e:
            print(stderr)
            print('error:', e, file=sys.stderr)
            sys.exit(1)
