select
    ST_Polygon(ST_Makeline(ARRAY[
        ST_MakePoint(longitude - 1, latitude + 1),
        ST_MakePoint(longitude + 1, latitude + 1),
        ST_MakePoint(longitude + 1, latitude - 1),
        ST_MakePoint(longitude - 1, latitude - 1),
        ST_MakePoint(longitude - 1, latitude + 1)
    ]), 4326) as area,
    sum(
        case when m.pressure='NaN' then 0
        else
            m.pressure * (16 - ST_3DDistance(
                ST_MakePoint(longitude, latitude, 20),
                ST_SetSRID(cast(m.location as geometry), 0)
            ))
        end
    ) / sum(
        case when m.pressure='NaN' then 0
        else
            16 - ST_3DDistance(
                ST_MakePoint(longitude, latitude, 20),
                ST_SetSRID(cast(m.location as geometry), 0)
            )
        end
    ) as pressure
from
    generate_series(-180 + 8, 180 - 8, 2) longitude cross join
    generate_series(-90 + 8, 90 - 8, 2) latitude left outer join
    mipas m on (
        ST_X(cast(m.location as geometry)) between longitude - 8 and longitude + 8 and
        ST_Y(cast(m.location as geometry)) between latitude - 8 and latitude + 8 and
        ST_Z(cast(m.location as geometry)) between 15 and 25 and
        extract(year from m.datetime) = 2003 and
        extract(month from m.datetime) = 7
    )
group by longitude, latitude
