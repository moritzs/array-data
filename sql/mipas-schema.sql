create table mipas (
    id serial primary key,
    datetime timestamp without time zone,
    location geography(pointz, 4326),
    pressure float,
    pressure_variance float,
    temperature float,
    temperature_variance float,
    O3_vmr float,
    O3_vmr_variance float,
    O3_concentration float,
    O3_concentration_variance float
);

create table regions (
    id serial primary key,
    scalerank float,
    featurecla varchar(100),
    name varchar(100),
    namealt varchar(100) NULL,
    region varchar(100) NULL,
    subregion varchar(100) NULL,
    area geography(multipolygon, 4326)
);

create table grid (
    area geography(polygon, 4326),
    temperature float
);

create index mipas_datetime on mipas (datetime);
create index mipas_location_gist on mipas using gist (location);
