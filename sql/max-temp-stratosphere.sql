select
    ST_Polygon(ST_Makeline(ARRAY[
        ST_MakePoint(x * 2 - 1, y * 2 - 1),
        ST_MakePoint(x * 2 + 1, y * 2 - 1),
        ST_MakePoint(x * 2 + 1, y * 2 + 1),
        ST_MakePoint(x * 2 - 1, y * 2 + 1),
        ST_MakePoint(x * 2 - 1, y * 2 - 1)
    ]), 4326) as area,
    max(case when m.temperature='NaN' then NULL else m.temperature end) as temperature
from (
    select
        round(ST_X(cast(m.location as geometry)) / 2) as x,
        round(ST_Y(cast(m.location as geometry)) / 2) as y,
        m.temperature as temperature
    from mipas m
    where
        ST_Z(cast(m.location as geometry)) between 10 and 50 and
        extract(year from m.datetime) = 2003
) as m
where
    x between -89 and 89 and
    y between -44 and 44
group by x, y
