with squares as (
    select
        round(ST_X(cast(m.location as geometry)) / 2) as x,
        round(ST_Y(cast(m.location as geometry)) / 2) as y,
        m.*
    from mipas m
    where
        ST_Z(cast(m.location as geometry)) between 15 and 25 and
        extract(year from m.datetime) = 2003 and
        extract(month from m.datetime) = 7
),

squares_distances as (
    select
        x, y,
        ST_3DDistance(
            ST_MakePoint((x - 2) * 2, (y + 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m2_2,
        ST_3DDistance(
            ST_MakePoint((x - 1) * 2, (y + 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m1_2,
        ST_3DDistance(
            ST_MakePoint((x + 0) * 2, (y + 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_0_2,
        ST_3DDistance(
            ST_MakePoint((x + 1) * 2, (y + 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_1_2,
        ST_3DDistance(
            ST_MakePoint((x + 2) * 2, (y + 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_2_2,
        ST_3DDistance(
            ST_MakePoint((x - 2) * 2, (y + 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m2_1,
        ST_3DDistance(
            ST_MakePoint((x - 1) * 2, (y + 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m1_1,
        ST_3DDistance(
            ST_MakePoint((x + 0) * 2, (y + 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_0_1,
        ST_3DDistance(
            ST_MakePoint((x + 1) * 2, (y + 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_1_1,
        ST_3DDistance(
            ST_MakePoint((x + 2) * 2, (y + 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_2_1,
        ST_3DDistance(
            ST_MakePoint((x - 2) * 2, (y + 0) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m2_0,
        ST_3DDistance(
            ST_MakePoint((x - 1) * 2, (y + 0) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m1_0,
        ST_3DDistance(
            ST_MakePoint((x + 0) * 2, (y + 0) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_0_0,
        ST_3DDistance(
            ST_MakePoint((x + 1) * 2, (y + 0) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_1_0,
        ST_3DDistance(
            ST_MakePoint((x + 2) * 2, (y + 0) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_2_0,
        ST_3DDistance(
            ST_MakePoint((x - 2) * 2, (y - 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m2_m1,
        ST_3DDistance(
            ST_MakePoint((x - 1) * 2, (y - 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m1_m1,
        ST_3DDistance(
            ST_MakePoint((x + 0) * 2, (y - 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_0_m1,
        ST_3DDistance(
            ST_MakePoint((x + 1) * 2, (y - 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_1_m1,
        ST_3DDistance(
            ST_MakePoint((x + 2) * 2, (y - 1) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_2_m1,
        ST_3DDistance(
            ST_MakePoint((x - 2) * 2, (y - 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m2_m2,
        ST_3DDistance(
            ST_MakePoint((x - 1) * 2, (y - 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_m1_m2,
        ST_3DDistance(
            ST_MakePoint((x + 0) * 2, (y - 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_0_m2,
        ST_3DDistance(
            ST_MakePoint((x + 1) * 2, (y - 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_1_m2,
        ST_3DDistance(
            ST_MakePoint((x + 2) * 2, (y - 2) * 2, 20),
            ST_SetSRID(cast(m.location as geometry), 0)
        ) as distance_2_m2,
        m.pressure as pressure
    from squares m
),

squares_union (x, y, distance, pressure) as (
    select
        x - 2,
        y + 2,
        distance_m2_2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 1,
        y + 2,
        distance_m1_2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 0,
        y + 2,
        distance_0_2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 1,
        y + 2,
        distance_1_2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 2,
        y + 2,
        distance_2_2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 2,
        y + 1,
        distance_m2_1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 1,
        y + 1,
        distance_m1_1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 0,
        y + 1,
        distance_0_1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 1,
        y + 1,
        distance_1_1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 2,
        y + 1,
        distance_2_1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 2,
        y + 0,
        distance_m2_0 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 1,
        y + 0,
        distance_m1_0 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 0,
        y + 0,
        distance_0_0 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 1,
        y + 0,
        distance_1_0 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 2,
        y + 0,
        distance_2_0 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 2,
        y - 1,
        distance_m2_m1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 1,
        y - 1,
        distance_m1_m1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 0,
        y - 1,
        distance_0_m1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 1,
        y - 1,
        distance_1_m1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 2,
        y - 1,
        distance_2_m1 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 2,
        y - 2,
        distance_m2_m2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x - 1,
        y - 2,
        distance_m1_m2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 0,
        y - 2,
        distance_0_m2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 1,
        y - 2,
        distance_1_m2 as distance,
        pressure
    from
        squares_distances
    union all
    select
        x + 2,
        y - 2,
        distance_2_m2 as distance,
        pressure
    from
        squares_distances
)

select
    ST_Polygon(ST_Makeline(ARRAY[
        ST_MakePoint(x * 2 - 1, y * 2 - 1),
        ST_MakePoint(x * 2 + 1, y * 2 - 1),
        ST_MakePoint(x * 2 + 1, y * 2 + 1),
        ST_MakePoint(x * 2 - 1, y * 2 + 1),
        ST_MakePoint(x * 2 - 1, y * 2 - 1)
    ]), 4326) as area,
    sum(
        case when pressure='NaN' then 0
        else pressure * (16 - distance) end
    ) / sum(
        case when pressure='NaN' then 0
        else 16 - distance end
    ) as pressure
from squares_union
where
    x between -89 and 89 and
    y between -44 and 44
group by x, y
