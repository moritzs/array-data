select
    r.id,
    r.name,
    extract(year from m.datetime) as year,
    extract(month from m.datetime) as month,
    avg(case when m.temperature='NaN' then NULL else m.temperature end) as temperature,
    avg(case when m.pressure='NaN' then NULL else m.pressure end) as pressure
from
    regions r,
    mipas m
where
    r.featurecla = 'Continent' and
    cast(m.location as geometry) @ cast(r.area as geometry) and
    ST_Z(cast(m.location as geometry)) between 10 and 12
group by r.id, r.name, year, month
order by r.name, year, month
