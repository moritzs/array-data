#!/usr/bin/env python3

import binascii
import ftplib
import math
import multiprocessing
import os.path
import struct
import subprocess
import sys
import tempfile
import threading


HOST = 'mip-ftp-ds.eo.esa.int'
USER = 'miP_datA'
PASSWD = 'e1_mIp$12'
DIR = 'MIP_NL__2P'
FIRST_ID = 1


# http://edndoc.esri.com/arcsde/9.0/general_topics/wkb_representation.htm
EWKB_FORMAT = '<IIddd'
LITTLE_ENDIAN = b'\x01'
POINTZ = 0xa0000001
SRID = 4326


class FTPError(Exception):
    def __init__(self, filename, error):
        super().__init__(filename, error)
        self.filename = filename
        self.error = error


def recursive_ftp_download(generator, host, user, passwd, path):
    try:
        generator.send(None)
    except StopIteration:
        return
    with ftplib.FTP(host=host, user=user, passwd=passwd) as ftp, \
            tempfile.TemporaryDirectory() as tmpdir:
        dirs = [path]
        while dirs:
            dir = dirs.pop()
            try:
                subdirs = ftp.nlst(dir)
                # check if dir is actually a regular file
                if not subdirs or (len(subdirs) == 1 and subdirs[0] == dir):
                    filename = os.path.basename(dir)
                    tmpfile = os.path.join(tmpdir, filename)
                    with open(tmpfile, 'wb') as f:
                        ftp.retrbinary('RETR ' + dir, f.write)
                    try:
                        generator.send((dir, tmpfile))
                    except StopIteration:
                        break
                else:
                    subdirs.sort(reverse=True)
                    dirs.extend(subdirs)
            except ftplib.all_errors as e:
                exception = FTPError(dir, e)
                try:
                    generator.throw(FTPError, exception)
                except StopIteration:
                    break
    generator.close()


def mipas_csv_to_pgsql(infile, outfile, start_number=1):
    for i, line in enumerate(infile, start_number):
        time, latitude, longitude, altitude, rest = line.split(';', 4)
        latitude = float(latitude)
        longitude = float(longitude)
        altitude = float(altitude)
        location = struct.pack(EWKB_FORMAT, POINTZ, SRID, longitude, latitude, altitude)
        location_text = binascii.hexlify(LITTLE_ENDIAN + location).decode('ascii')
        outfile.write(str(i) + ';' + time + ';' + location_text + ';' + rest)
    return i + 1


class ConverterProcess(multiprocessing.Process):
    def __init__(self, queue, **kwargs):
        super().__init__(**kwargs)
        self._queue = queue

    def run(self):
        next_id = FIRST_ID
        while True:
            msg = self._queue.get()
            if msg is None:
                break
            filename, tmpfile = msg
            filename = os.path.basename(filename)
            proc = subprocess.Popen(
                ['./mipas_to_csv', tmpfile], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE, universal_newlines=True
            )
            sys.stdout.write(
                'COPY mipas (id, datetime, location, pressure, pressure_variance, temperature, '
                'temperature_variance, O3_vmr, O3_vmr_variance, O3_concentration, '
                'O3_concentration_variance) FROM STDIN (FORMAT csv, DELIMITER \';\');\n'
            )
            new_next_id = mipas_csv_to_pgsql(proc.stdout, sys.stdout, next_id)
            err = proc.stderr.readline()
            if proc.poll() is None:
                proc.stdout.close()
                proc.stderr.close()
            proc.wait()
            if proc.returncode == 0:
                print(filename, new_next_id - next_id, file=sys.stderr)
                next_id = new_next_id
            else:
                sys.stdout.write('error\n')
                print(
                    '{} {}: {}'.format(filename, num_entries, err.strip()),
                    file=sys.stderr
                )
            sys.stdout.write('\\.\n')
            os.remove(tmpfile)


def mipas_ftp_downloader():
    queue = multiprocessing.Queue(10)
    proc = ConverterProcess(queue)
    proc.start()
    while True:
        try:
            filename, tmpfile = yield
        except FTPError as e:
            print('{}: {}'.format(e.filename, e.error), file=sys.stderr)
            continue
        except GeneratorExit:
            queue.put(None)
            proc.join()
            break
        queue.put((filename, tmpfile))


recursive_ftp_download(mipas_ftp_downloader(), HOST, USER, PASSWD, DIR)
