#!/usr/bin/env python3

import os
import os.path
import subprocess
import sys

from mipas_csv_pgsql import mipas_csv_to_pgsql


next_id = 1

for dirpath, dirnames, filenames in os.walk(sys.argv[1]):
    dirnames.sort()
    filenames.sort()
    for filename in filenames:
        filename = os.path.join(dirpath, filename)
        proc = subprocess.Popen(
            ['./mipas_to_csv', filename], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, universal_newlines=True
        )
        new_next_id = mipas_csv_to_pgsql(proc.stdout, sys.stdout, next_id)
        err = proc.stderr.readline()
        if proc.poll() is None:
            proc.stdout.close()
            proc.stderr.close()
        proc.wait()
        if proc.returncode == 0:
            print(filename, new_next_id - next_id, file=sys.stderr)
        else:
            print(
                '{} {}: {}'.format(filename, new_next_id - next_id, err.strip()),
                file=sys.stderr
            )
        next_id = new_next_id
