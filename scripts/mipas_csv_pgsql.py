#!/usr/bin/env python3

import binascii
import math
import struct


# http://edndoc.esri.com/arcsde/9.0/general_topics/wkb_representation.htm
EWKB_FORMAT = '<IIddd'
LITTLE_ENDIAN = b'\x01'
POINTZ = 0xa0000001
SRID = 4326


def mipas_csv_to_pgsql(infile, outfile, start_number=1):
    for i, line in enumerate(infile, start_number):
        time, latitude, longitude, altitude, rest = line.split(';', 4)
        latitude = float(latitude)
        longitude = float(longitude)
        altitude = float(altitude)
        location = struct.pack(EWKB_FORMAT, POINTZ, SRID, longitude, latitude, altitude)
        location_text = binascii.hexlify(LITTLE_ENDIAN + location).decode('ascii')
        outfile.write(str(i) + ';' + time + ';' + location_text + ';' + rest)
    return i + 1


if __name__ == '__main__':
    import sys
    mipas_csv_to_pgsql(sys.stdin, sys.stdout)
