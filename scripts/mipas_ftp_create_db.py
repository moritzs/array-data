#!/usr/bin/env python3

import ftplib
import os
import os.path
import subprocess
import sys
import tempfile
import threading


HOST = 'mip-ftp-ds.eo.esa.int'
USER = 'miP_datA'
PASSWD = 'e1_mIp$12'
DIR = 'MIP_NL__2P'


class FTPError(Exception):
    def __init__(self, filename, error):
        super().__init__(filename, error)
        self.filename = filename
        self.error = error


def recursive_ftp_download(generator, host, user, passwd, path):
    try:
        generator.send(None)
    except StopIteration:
        return
    try:
        with ftplib.FTP(host=host, user=user, passwd=passwd) as ftp, \
                tempfile.TemporaryDirectory() as tmpdir:
            dirs = [path]
            while dirs:
                dir = dirs.pop()
                try:
                    subdirs = ftp.nlst(dir)
                    # check if dir is actually a regular file
                    if not subdirs or (len(subdirs) == 1 and subdirs[0] == dir):
                        filename = os.path.basename(dir)
                        tmpfile = os.path.join(tmpdir, filename)
                        with open(tmpfile, 'wb') as f:
                            ftp.retrbinary('RETR ' + dir, f.write)
                        try:
                            generator.send((dir, tmpfile))
                        except StopIteration:
                            break
                    else:
                        subdirs.sort(reverse=True)
                        dirs.extend(subdirs)
                except ftplib.all_errors as e:
                    exception = FTPError(dir, e)
                    try:
                        generator.throw(FTPError, exception)
                    except StopIteration:
                        break
    finally:
        generator.close()


class DeleterThread(threading.Thread):
    def __init__(self, createdb_stdout, **kwargs):
        super().__init__(**kwargs)
        self._stdout = createdb_stdout

    def run(self):
        while True:
            filename = self._stdout.readline()
            if not filename:
                return
            filename = filename.strip()
            os.remove(filename)
            print(os.path.basename(filename))


def mipas_ftp_downloader():
    proc = subprocess.Popen(
        ['./create-db', '--append', 'mipas-ftp-columnstore'], stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, universal_newlines=True, bufsize=1, preexec_fn=os.setpgrp
    )
    thread = DeleterThread(proc.stdout)
    thread.start()
    while True:
        try:
            filename, tmpfile = yield
        except FTPError as e:
            print('{}: {}'.format(e.filename, e.error), file=sys.stderr)
            continue
        except GeneratorExit:
            proc.stdin.close()
            proc.wait()
            thread.join()
            break
        proc.stdin.write(tmpfile + '\n')


recursive_ftp_download(mipas_ftp_downloader(), HOST, USER, PASSWD, DIR)
