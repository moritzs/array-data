#ifndef _ARRAY_DATA_ALLOCATOR_HPP
#define _ARRAY_DATA_ALLOCATOR_HPP

#include <x86intrin.h>


namespace utils {

template <typename T, size_t Align>
class aligned_allocator {
public:
    using value_type = T;

    T* allocate(const size_t n) const noexcept {
        return reinterpret_cast<T*>(_mm_malloc(n * sizeof(T), Align));
    }

    void deallocate(T* ptr, const size_t) const noexcept {
        _mm_free(ptr);
    }

    template <typename U>
    struct rebind {
        using other = aligned_allocator<U, Align>;
    };
};


template <typename T, size_t Align>
bool operator==(const aligned_allocator<T, Align>, const aligned_allocator<T, Align>) {
    return true;
}

template <typename T, size_t Align>
bool operator!=(const aligned_allocator<T, Align>, const aligned_allocator<T, Align>) {
    return false;
}

}

#endif
