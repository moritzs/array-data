#ifndef _ARRAY_DATA_SIMD_HPP
#define _ARRAY_DATA_SIMD_HPP

#include <x86intrin.h>


namespace utils {

inline double mm128d_sum(__m128d values) {
    alignas(__m128d) double unpacked_values[2];
    _mm_store_pd(unpacked_values, values);
    return unpacked_values[0] + unpacked_values[1];
}

inline double mm256d_sum(__m256d values) {
    alignas(__m256d) double unpacked_values[4];
    _mm256_store_pd(unpacked_values, values);
    return unpacked_values[0] + unpacked_values[1] + unpacked_values[2] + unpacked_values[3];
}

}


#endif
