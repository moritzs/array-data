#include <cassert>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <x86intrin.h>
#include "simd.hpp"
#include "threading.hpp"


template <typename Derived>
class mem_bandwidth_base {
private:
    struct thread_result {
        size_t nanoseconds_read;
        size_t nanoseconds_write;
    };

    const size_t num_passes;

public:
    struct result {
        size_t read_bytes_per_second;
        size_t write_bytes_per_second;
    };

    mem_bandwidth_base(const size_t num_passes) : num_passes(num_passes) {}

    result run(const size_t num_threads, const size_t data_size) {
        assert(data_size % 64 == 0);
        std::vector<thread_result> results(num_threads * num_passes);
        std::vector<std::thread> threads;
        auto derived = static_cast<Derived*>(this);
        char* data = derived->allocate_data(num_threads, data_size);
        threading::barrier<std::mutex> barrier(num_threads);
        for (size_t i = 0; i < num_threads; ++i) {
            threads.emplace_back(
                [this, derived, i, num_threads, data, data_size, &results, &barrier] {
                    for (size_t j = 0; j < num_passes; ++j) {
                        results[j + i * num_passes].nanoseconds_write = derived->run_write(
                            i, num_threads, data_size, data
                        );
                        barrier.wait();
                        results[j + i * num_passes].nanoseconds_read = derived->run_read(
                            i, num_threads, data_size, data
                        );
                        barrier.wait();
                    }
                }
            );
        }
        for (auto& t : threads) {
            t.join();
        }
        derived->free_data(data);
        size_t read = 0;
        size_t write = 0;
        for (const auto& tr : results) {
            read += tr.nanoseconds_read;
            write += tr.nanoseconds_write;
        }
        read = data_size / (read / (num_threads * num_passes * 1000000000.0));
        write = data_size / (write / (num_threads * num_passes * 1000000000.0));
        return result{read, write};
    }
};


class mem_bandwidth
: public mem_bandwidth_base<mem_bandwidth> {
public:
    using mem_bandwidth_base::mem_bandwidth_base;

    char* allocate_data(const size_t, const size_t data_size) {
        char* data = reinterpret_cast<char*>(_mm_malloc(data_size, 64));
        // Force all pages to be allocated
        std::memset(data, 0xaa, data_size);
        return data;
    }

    void free_data(char* data) {
        _mm_free(data);
    }

    size_t run_write(
        const size_t thread_id, const size_t num_threads, const size_t data_size, char* data
    ) {
        const size_t num_double_packs = data_size / 32;
        double values[4] = {1.1, 2.12, 3.14, 10.0};
        __m256d values_pack = _mm256_loadu_pd(values);
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = thread_id; i < num_double_packs; i += num_threads) {
            _mm256_store_pd(reinterpret_cast<double*>(data + i * 32), values_pack);
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }

    size_t run_read(
        const size_t thread_id, const size_t num_threads, const size_t data_size, char* data
    ) {
        const size_t num_double_packs = data_size / 32;
        __m256d values_pack = _mm256_setzero_pd();
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = thread_id; i < num_double_packs; i += num_threads) {
            values_pack = _mm256_load_pd(reinterpret_cast<double*>(data + i * 32));
            // need this to prevent the memory loads from being optimized away
            asm volatile ("" : : "v" (values_pack));
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }
};


class mem_cache_bandwidth
: public mem_bandwidth_base<mem_cache_bandwidth> {
private:
    size_t cache_size_kib_bits;

public:
    mem_cache_bandwidth(const size_t num_passes, const size_t cache_size_kib)
    : mem_bandwidth_base(num_passes) {
        size_t bits = 0;
        while ((1ull << bits) < cache_size_kib) {
            ++bits;
        }
        cache_size_kib_bits = bits;
    }

    char* allocate_data(const size_t, const size_t) {
        const size_t allocate_size = 1ul << (10 + cache_size_kib_bits);
        char* data = reinterpret_cast<char*>(_mm_malloc(allocate_size, 64));
        // Force all pages to be allocated
        std::memset(data, 0xaa, allocate_size);
        return data;
    }

    void free_data(char* data) {
        _mm_free(data);
    }

    size_t run_write(
        const size_t thread_id, const size_t num_threads, const size_t data_size, char* data
    ) {
        const size_t num_double_packs = data_size / 32;
        const size_t num_packs_mask = ((1ul << (10 + cache_size_kib_bits)) / 32) - 1;
        double values[4] = {1.1, 2.12, 3.14, 10.0};
        __m256d values_pack = _mm256_loadu_pd(values);
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = 2 * thread_id; i < num_double_packs; i += 2 * num_threads) {
            _mm256_store_pd(
                reinterpret_cast<double*>(data + (i & num_packs_mask) * 32),
                values_pack
            );
            _mm256_store_pd(
                reinterpret_cast<double*>(data + ((i + 1) & num_packs_mask) * 32),
                values_pack
            );
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }

    size_t run_read(
        const size_t thread_id, const size_t num_threads, const size_t data_size, char* data
    ) {
        const size_t num_double_packs = data_size / 32;
        const size_t num_packs_mask = ((1ul << (10 + cache_size_kib_bits)) / 32) - 1;
        __m256d values_pack = _mm256_setzero_pd();
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = 2 * thread_id; i < num_double_packs; i += 2 * num_threads) {
            values_pack = _mm256_load_pd(
                reinterpret_cast<double*>(data + (i & num_packs_mask) * 32)
            );
            // need this to prevent the memory loads from being optimized away
            asm volatile ("" : : "v" (values_pack));
            values_pack = _mm256_load_pd(
                reinterpret_cast<double*>(data + ((i + 1) & num_packs_mask) * 32)
            );
            asm volatile ("" : : "v" (values_pack));
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }
};


class ymm_register_bandwidth
: public mem_bandwidth_base<ymm_register_bandwidth> {
public:
    using mem_bandwidth_base::mem_bandwidth_base;

    char* allocate_data(const size_t, const size_t) {
        return nullptr;
    }

    void free_data(char*) {}

    size_t run_write(const size_t, const size_t num_threads, const size_t data_size, char*) {
        const size_t num_double_packs = data_size / 32 / num_threads;
        double values1[4] = {1.1, 2.12, 3.14, 10.0};
        __m256d values1_pack = _mm256_loadu_pd(values1);
        double values2[4] = {123.456, 12.13, 0.25, -40.41};
        __m256d values2_pack = _mm256_loadu_pd(values2);
        __m256d result = _mm256_setzero_pd();
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < num_double_packs; ++i) {
            asm volatile ("" : "+v" (values1_pack), "+v" (values2_pack));
            __m256d tmp = _mm256_mul_pd(values1_pack, values2_pack);
            result = _mm256_add_pd(result, tmp);
            asm volatile ("" : : "v" (result));
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }

    size_t run_read(const size_t, const size_t num_threads, const size_t data_size, char*) {
        const size_t num_double_packs = data_size / 32 / num_threads;
        double values1[4] = {1.1, 2.12, 3.14, 10.0};
        __m256d values1_pack = _mm256_loadu_pd(values1);
        double values2[4] = {123.456, 12.13, 0.25, -40.41};
        __m256d values2_pack = _mm256_loadu_pd(values2);
        __m256d result = _mm256_setzero_pd();
        auto start = std::chrono::high_resolution_clock::now();
        for (size_t i = 0; i < num_double_packs; ++i) {
            asm volatile ("" : "+v" (values1_pack), "+v" (values2_pack));
            __m256d tmp = _mm256_add_pd(values1_pack, values2_pack);
            result = _mm256_add_pd(result, tmp);
            asm volatile ("" : : "v" (result));
        }
        auto end = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    }
};


int main(int argc, char** argv) {
    size_t data_size = (1ull << 30) * 16;
    if (argc >= 2) {
        data_size = (1ull << 30) * std::atoi(argv[1]);
    }
    double factor = 1ull << 30;
    {
        std::cout << "No Cache:" << std::endl;
        auto result = mem_bandwidth{5}.run(8, data_size);
        std::cout << std::fixed;
        std::cout << "Read:  " << (result.read_bytes_per_second / factor) << " GiB/s" << std::endl;
        std::cout << "Write: " << (result.write_bytes_per_second / factor) << " GiB/s" << std::endl;
    }
    std::cout << std::endl;
    {
        std::cout << "L3 Cache (4 MiB):" << std::endl;
        auto result = mem_cache_bandwidth{20, 4096}.run(8, data_size);
        std::cout << "Read:  " << (result.read_bytes_per_second / factor) << " GiB/s" << std::endl;
        std::cout << "Write: " << (result.write_bytes_per_second / factor) << " GiB/s" << std::endl;
    }
    std::cout << std::endl;
    {
        std::cout << "L2 Cache (128 KiB):" << std::endl;
        auto result = mem_cache_bandwidth{20, 128}.run(8, data_size);
        std::cout << "Read:  " << (result.read_bytes_per_second / factor) << " GiB/s" << std::endl;
        std::cout << "Write: " << (result.write_bytes_per_second / factor) << " GiB/s" << std::endl;
    }
    std::cout << std::endl;
    {
        std::cout << "L1 Cache (16 KiB):" << std::endl;
        auto result = mem_cache_bandwidth{20, 16}.run(8, data_size);
        std::cout << "Read:  " << (result.read_bytes_per_second / factor) << " GiB/s" << std::endl;
        std::cout << "Write: " << (result.write_bytes_per_second / factor) << " GiB/s" << std::endl;
    }
    std::cout << std::endl;
    {
        std::cout << "ymm register:" << std::endl;
        auto result = ymm_register_bandwidth{30}.run(8, data_size);
        std::cout << "Add:   " << (result.read_bytes_per_second / factor) << " GiB/s" << std::endl;
        std::cout << "Mul:   " << (result.write_bytes_per_second / factor) << " GiB/s" << std::endl;
    }
    return 0;
}
