cmake_minimum_required(VERSION 3.7)
project(array-data)

set(CMAKE_CXX_STANDARD 14)

add_compile_options(-Wall -Wextra -Wno-unused-function)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

find_package(Threads)
find_package(GTest)

find_package(Boost 1.62.0 REQUIRED COMPONENTS date_time filesystem)
include_directories(${Boost_INCLUDE_DIRS})

mark_as_advanced(RTREE_VARIANT)
set(RTREE_VARIANT CACHE STRING "Variant of R-Tree used (quadratic or rstar)")
if(RTREE_VARIANT)
    add_definitions(-DRTREE_VARIANT=${RTREE_VARIANT})
endif()

add_subdirectory(storage)
add_subdirectory(mipas)
add_subdirectory(imagedb)
add_subdirectory(misc)
if(GTEST_FOUND)
    enable_testing()
    add_subdirectory(tests)
endif()
