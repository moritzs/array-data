#ifndef _ARRAY_DATA_THREADING
#define _ARRAY_DATA_THREADING

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <x86intrin.h>


namespace threading {

class spin_lock {
private:
    std::atomic_flag is_locked;

public:
    spin_lock() {
        is_locked.clear();
    }
    spin_lock(const spin_lock&) = delete;
    spin_lock(spin_lock&&) = delete;

    void lock() {
        while (is_locked.test_and_set()) {
            _mm_pause();
        }
    }

    bool try_lock() {
        return !is_locked.test_and_set();
    }

    void unlock() {
        is_locked.clear();
    }
};


template<typename Mutex>
class barrier {
private:
    const size_t num_threads;
    size_t current_threads;
    bool is_returning;
    Mutex mutex;
    std::condition_variable_any cond;

public:
    barrier(const size_t num_threads)
    : num_threads(num_threads), current_threads(0), is_returning(false) {}

    void wait() {
        wait_run_once([] {});
    }

    template <typename F>
    void wait_run_once(const F& func) {
        std::unique_lock<Mutex> lock{mutex};
        if (is_returning) {
            while (true) {
                if (is_returning && current_threads == 0) {
                    is_returning = false;
                    break;
                } else if (!is_returning) {
                    break;
                }
                cond.wait(lock);
            }
            cond.notify_all();
        }
        ++current_threads;
        while (true) {
            if (is_returning) {
                --current_threads;
                break;
            } else if (current_threads == num_threads) {
                is_returning = true;
                --current_threads;
                func();
                break;
            }
            cond.wait(lock);
        }
        cond.notify_all();
    }

};

}

#endif
