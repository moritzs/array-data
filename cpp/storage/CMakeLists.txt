find_package(PythonInterp REQUIRED)
execute_process(
    COMMAND ${PYTHON_EXECUTABLE} -c "import jinja2"
    ERROR_QUIET
    RESULT_VARIABLE PYTHON_TEST_JINJA2
)
if (NOT PYTHON_TEST_JINJA2 EQUAL 0)
    message(FATAL_ERROR "Couldn't import Jinja2")
endif()

execute_process(
    COMMAND ${PYTHON_EXECUTABLE} storage_gen.py --print-depends
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    OUTPUT_VARIABLE STORAGE_GEN_DEPENDS
    RESULT_VARIABLE STORAGE_GEN_RETURN_VALUE
)
if (NOT STORAGE_GEN_RETURN_VALUE EQUAL 0)
    message(FATAL_ERROR "Failed to get storage_gen.py depends")
endif()

execute_process(
    COMMAND ${PYTHON_EXECUTABLE} storage_gen.py --print-outputs
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    OUTPUT_VARIABLE STORAGE_GEN_OUTPUTS
    RESULT_VARIABLE STORAGE_GEN_RETURN_VALUE
)
if (NOT STORAGE_GEN_RETURN_VALUE EQUAL 0)
    message(FATAL_ERROR "Failed to get storage_gen.py outputs")
endif()

add_custom_command(
    OUTPUT ${STORAGE_GEN_OUTPUTS}
    COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/storage_gen.py
    MAIN_DEPENDENCY storage_gen.py
    DEPENDS ${STORAGE_GEN_DEPENDS}
    COMMENT "Generating storage files"
)

add_custom_target(generate-storage DEPENDS ${STORAGE_GEN_OUTPUTS})
