#ifndef _ARRAY_DATA_BTREE_HPP
#define _ARRAY_DATA_BTREE_HPP

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <cstring>
#include <functional>
#include <iterator>
#include <memory>
#include <ostream>
#include <queue>
#include <tuple>
#include <type_traits>
#include <utility>
#include "utils.hpp"


template <
    typename Key,
    typename Value,
    size_t K,
    size_t Kv,
    typename Compare = std::less_equal<Key>,
    typename Allocator = std::allocator<Key>
>
class BTree {
public:
    using key_type = Key;
    using value_type = Value;

private:
    struct InnerNode;
    struct LeafNode;

    union node_pointer_t {
        InnerNode* inner;
        LeafNode* leaf;

        node_pointer_t() {
            inner = nullptr;
        }

        node_pointer_t(std::nullptr_t) {
            inner = nullptr;
        }

        node_pointer_t(InnerNode* inner) {
            this->inner = inner;
        }

        node_pointer_t(LeafNode* leaf) {
            this->leaf = leaf;
        }

        bool operator==(const node_pointer_t p) const {
            return std::memcmp(this, &p, sizeof(node_pointer_t)) == 0;
        }
    };

    struct InnerNode {
        size_t num_values;
        Key keys[2 * K];
        node_pointer_t pointers[2 * K + 1];

        InnerNode() : num_values(0) {}

        inline Key& last_key() {
            return keys[num_values - 1];
        }
    };

    struct LeafNode {
        size_t num_values;
        Key keys[2 * Kv];
        Value values[2 * Kv];
        LeafNode* prev_leaf;
        LeafNode* next_leaf;

        LeafNode() : num_values(0), prev_leaf(nullptr), next_leaf(nullptr) {}

        inline Key& last_key() {
            return keys[num_values - 1];
        }
    };

    template <bool is_const>
    class iterator_base {
    public:
        using pair_value_type = std::conditional_t<is_const, const Value, Value>;
        using value_type = std::pair<const Key&, pair_value_type&>;
        using difference_type = ssize_t;
        using reference = value_type;
        using pointer = utils::value_pointer<value_type>;
        using iterator_category = std::bidirectional_iterator_tag;

    private:
        friend class BTree;

        using LeafNode_t = std::conditional_t<is_const, const LeafNode, LeafNode>;
        LeafNode_t* prev;
        LeafNode_t* node;
        size_t index;

        iterator_base(LeafNode_t* node, const size_t index)
        : prev(nullptr), node(node), index(index) {
            if (node != nullptr) {
                prev = node->prev_leaf;
            }
        }

        iterator_base(LeafNode_t* prev, LeafNode_t* node, const size_t index)
        : prev(prev), node(node), index(index) {}

    public:
        iterator_base() : prev(nullptr), node(nullptr), index(0) {}

        operator iterator_base<true>() const {
            return iterator_base<true>{prev, node, index};
        }

        reference operator*() const {
            return reference{node->keys[index], node->values[index]};
        }

        pointer operator->() const {
            return pointer{*(*this)};
        }

        iterator_base& operator++() {
            ++index;
            if (index >= node->num_values) {
                prev = node;
                node = node->next_leaf;
                index = 0;
            }
            return *this;
        }

        iterator_base operator++(int) const {
            iterator_base it{*this};
            ++it;
            return it;
        }

        iterator_base& operator--() {
            if (index == 0) {
                node = prev;
                prev = node->prev_leaf;
                index = node->num_values - 1;
            } else {
                --index;
            }
            return *this;
        }

        iterator_base operator--(int) const {
            iterator_base it{*this};
            --it;
            return it;
        }

        bool operator==(const iterator_base& it) const {
            return node == it.node && index == it.index;
        }

        bool operator!=(const iterator_base& it) const {
            return node != it.node || index != it.index;
        }
    };

    using InnerNodeAllocator = \
        typename std::allocator_traits<Allocator>::template rebind_alloc<InnerNode>;
    using LeafNodeAllocator = \
        typename std::allocator_traits<Allocator>::template rebind_alloc<LeafNode>;

    InnerNodeAllocator inner_alloc;
    LeafNodeAllocator leaf_alloc;

    node_pointer_t root;
    size_t depth;

    void deallocate() {
        if (depth == 0) {
            return;
        }
        std::queue<std::tuple<size_t, node_pointer_t>> nodes;
        nodes.emplace(1, root);
        while (!nodes.empty()) {
            size_t node_depth;
            node_pointer_t node;
            std::tie(node_depth, node) = nodes.front();
            nodes.pop();
            if (node_depth < depth) {
                for (size_t i = 0; i <= node.inner->num_values; ++i) {
                    nodes.emplace(node_depth + 1, node.inner->pointers[i]);
                }
                node.inner->~InnerNode();
                inner_alloc.deallocate(node.inner, 1);
            } else {
                node.leaf->~LeafNode();
                leaf_alloc.deallocate(node.leaf, 1);
            }
        }
        root.leaf = nullptr;
        depth = 0;
    }

    static LeafNode& rightmost_leaf(const node_pointer_t root, const size_t depth) {
        assert(depth > 0);
        node_pointer_t node = root;
        for (size_t i = 1; i < depth; ++i) {
            node = node.inner->pointers[node.inner->num_values];
        }
        return *node.leaf;
    }

    static Key& rightmost_key(const node_pointer_t root, const size_t depth) {
        LeafNode& leaf = rightmost_leaf(root, depth);
        return leaf.keys[leaf.num_values - 1];
    }

    static LeafNode& leftmost_leaf(const node_pointer_t root, const size_t depth) {
        assert(depth > 0);
        node_pointer_t node = root;
        for (size_t i = 1; i < depth; ++i) {
            node = node.inner->pointers[0];
        }
        return *node.leaf;
    }

    template <typename Node>
    static size_t key_index(const Node& node, const Key& key) {
        for (size_t i = 0; i < node.num_values; ++i) {
            if (Compare{}(key, node.keys[i])) {
                return i;
            }
        }
        return node.num_values;
    }

    LeafNode& find_leaf(const Key& key) {
        assert(depth > 0);
        node_pointer_t current_node = root;
        // Traverse inner nodes up to the last two layers.
        for (size_t i = 2; i < depth; ++i) {
            auto* node = current_node.inner;
            size_t index = key_index(*node, key);
            current_node.inner = node->pointers[index].inner;
        }
        // Traverse from last inner layer to leaf layer.
        if (depth > 1) {
            auto* node = current_node.inner;
            size_t index = key_index(*node, key);
            current_node.leaf = node->pointers[index].leaf;
        }
        return *current_node.leaf;
    }

    std::vector<node_pointer_t> find_leaf_with_parents(const Key& key) {
        assert(depth > 0);
        std::vector<node_pointer_t> nodes;
        nodes.reserve(depth);
        nodes.push_back(root);
        node_pointer_t current_node = root;
        for (size_t i = 2; i < depth; ++i) {
            auto* node = current_node.inner;
            size_t index = key_index(*node, key);
            auto next_node = node->pointers[index];
            nodes.push_back(next_node);
            current_node.inner = next_node.inner;
        }
        if (depth > 1) {
            auto* node = current_node.inner;
            size_t index = key_index(*node, key);
            auto leaf = node->pointers[index];
            nodes.push_back(leaf);
            current_node.leaf = leaf.leaf;
        }
        return nodes;
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_keys(Node& node, const size_t index) {
        assert(
            index < node.num_values &&
            node.num_values < sizeof(node.keys) / sizeof(Key)
        );
        std::memmove(
            &node.keys[index + 1],
            &node.keys[index],
            (node.num_values - index) * sizeof(Key)
        );
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<!std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_keys(Node& node, const size_t index) {
        assert(
            index < node.num_values &&
            node.num_values < sizeof(node.keys) / sizeof(Key)
        );
        std::move_backward(
            &node.keys[index],
            &node.keys[node.num_values],
            &node.keys[node.num_values + 1]
        );
    }

    template <
        typename T = Value,
        std::enable_if_t<std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_values(LeafNode& leaf, const size_t index) {
        assert(
            index < leaf.num_values &&
            leaf.num_values < 2 * Kv
        );
        std::memmove(
            &leaf.values[index + 1],
            &leaf.values[index],
            (leaf.num_values - index) * sizeof(Value)
        );
    }

    template <
        typename T = Value,
        std::enable_if_t<!std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_values(LeafNode& leaf, const size_t index) {
        assert(
            index < leaf.num_values &&
            leaf.num_values < 2 * Kv
        );
        std::move_backward(
            &leaf.values[index],
            &leaf.values[leaf.num_values],
            &leaf.values[leaf.num_values + 1]
        );
    }

    static void move_pointers(InnerNode& node, const size_t index) {
        assert(
            index < node.num_values + 1 &&
            node.num_values < 2 * K
        );
        std::memmove(
            &node.pointers[index + 1],
            &node.pointers[index],
            (node.num_values + 1 - index) * sizeof(node_pointer_t)
        );
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_backwards_keys(Node& node, const size_t index) {
        assert(
            index < node.num_values &&
            node.num_values <= sizeof(node.keys) / sizeof(Key)
        );
        std::memmove(
            &node.keys[index],
            &node.keys[index + 1],
            (node.num_values - index) * sizeof(Key)
        );
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<!std::is_trivially_copyable<T>::value, int> = 0
    >
    static void move_backwards_keys(Node& node, const size_t index) {
        assert(
            index < node.num_values &&
            node.num_values <= sizeof(node.keys) / sizeof(Key)
        );
        std::move(
            &node.keys[index + 1],
            &node.keys[node.num_values],
            &node.keys[index]
        );
    }

    static void move_backwards_pointers(InnerNode& node, const size_t index) {
        assert(
            index < node.num_values + 1 &&
            node.num_values <= 2 * K
        );
        std::memmove(
            &node.pointers[index],
            &node.pointers[index + 1],
            (node.num_values + 1 - index) * sizeof(node_pointer_t)
        );
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<std::is_trivially_copyable<T>::value, int> = 0
    >
    static void copy_keys(Node& old_node, Node& new_node, const size_t index) {
        assert(
            index < old_node.num_values &&
            old_node.num_values <= sizeof(old_node.keys) / sizeof(Key)
        );
        std::memcpy(
            &new_node.keys[0],
            &old_node.keys[index],
            (old_node.num_values - index) * sizeof(Key)
        );
    }

    template <
        typename Node,
        typename T = Key,
        std::enable_if_t<!std::is_trivially_copyable<T>::value, int> = 0
    >
    static void copy_keys(Node& old_node, Node& new_node, const size_t index) {
        assert(
            index < old_node.num_values &&
            old_node.num_values <= sizeof(old_node.keys) / sizeof(Key)
        );
        std::move(
            &old_node.keys[index],
            &old_node.keys[old_node.num_values],
            &new_node.keys[0]
        );
    }

    template <
        typename T = Value,
        std::enable_if_t<std::is_trivially_copyable<T>::value, int> = 0
    >
    static void copy_values(LeafNode& old_leaf, LeafNode& new_leaf, const size_t index) {
        assert(
            index < old_leaf.num_values &&
            old_leaf.num_values <= 2 * Kv
        );
        std::memcpy(
            &new_leaf.values[0],
            &old_leaf.values[index],
            (old_leaf.num_values - index) * sizeof(Value)
        );
    }

    template <
        typename T = Value,
        std::enable_if_t<!std::is_trivially_copyable<T>::value, int> = 0
    >
    static void copy_values(LeafNode& old_leaf, LeafNode& new_leaf, const size_t index) {
        assert(
            index < old_leaf.num_values &&
            old_leaf.num_values <= 2 * Kv
        );
        std::move(
            &old_leaf.values[index],
            &old_leaf.values[old_leaf.num_values],
            &new_leaf.values[0]
        );
    }

    static void copy_pointers(
        InnerNode& old_node,
        InnerNode& new_node,
        const size_t old_index,
        const size_t new_index = 0
    ) {
        assert(
            old_index < old_node.num_values + 1 &&
            old_node.num_values <= 2 * K
        );
        std::memcpy(
            &new_node.pointers[new_index],
            &old_node.pointers[old_index],
            (old_node.num_values + 1 - old_index) * sizeof(node_pointer_t)
        );
    }

public:
    using iterator = iterator_base<false>;
    using const_iterator = iterator_base<true>;

    BTree() : depth(0) {
        root.leaf = nullptr;
    }

    BTree(const Allocator& alloc)
    : inner_alloc(alloc), leaf_alloc(alloc), depth(0) {
        root.leaf = nullptr;
    }

    ~BTree() {
        deallocate();
    }

    BTree(BTree&& b) : BTree() {
        deallocate();
        root = b.root;
        depth = b.depth;
        b.root.leaf = nullptr;
        b.depth = 0;
    }

    BTree& operator=(BTree&& b) {
        deallocate();
        root = b.root;
        depth = b.depth;
        b.root.leaf = nullptr;
        b.depth = 0;
        return *this;
    }

    inline size_t get_depth() const {
        return depth;
    }

    inline bool empty() const {
        return depth == 0;
    }

    bool check_consistency() const {
        if (depth == 0) {
            return true;
        }
        std::queue<std::tuple<size_t, Key*, Key*, node_pointer_t>> nodes;
        nodes.emplace(1, nullptr, nullptr, root);
        while (!nodes.empty()) {
            size_t node_depth;
            Key* left_key;
            Key* right_key;
            node_pointer_t node;
            std::tie(node_depth, left_key, right_key, node) = nodes.front();
            nodes.pop();
            if (node_depth < depth) {
                // inner node
                if (node.inner->num_values == 0) {
                    return false;
                }
                if (node.inner->num_values > 2 * K) {
                    return false;
                }
                // check that keys are ordered
                for (size_t i = 1; i < node.inner->num_values; ++i) {
                    const Key& k1 = node.inner->keys[i - 1];
                    const Key& k2 = node.inner->keys[i];
                    if (!(Compare{}(k1, k2) && k1 != k2)) {
                        return false;
                    }
                }
                // check that all keys are smaller or equal than right key
                if (right_key != nullptr) {
                    for (size_t i = 0; i < node.inner->num_values; ++i) {
                        if (!Compare{}(node.inner->keys[i], *right_key)) {
                            return false;
                        }
                    }
                }
                // check that all keys are greater than left key
                if (left_key != nullptr) {
                    for (size_t i = 0; i < node.inner->num_values; ++i) {
                        if (Compare{}(node.inner->keys[i], *left_key)) {
                            return false;
                        }
                    }
                }
                Key* next_left_key;
                Key* next_right_key;
                for (size_t i = 0; i < node.inner->num_values; ++i) {
                    if (i > 0) {
                        next_left_key = &node.inner->keys[i - 1];
                    } else {
                        next_left_key = nullptr;
                    }
                    next_right_key = &node.inner->keys[i];
                    nodes.emplace(
                        node_depth + 1,
                        next_left_key,
                        next_right_key,
                        node.inner->pointers[i]
                    );
                }
                next_left_key = &node.inner->keys[node.inner->num_values - 1];
                next_right_key = right_key;
                nodes.emplace(
                    node_depth + 1,
                    next_left_key,
                    next_right_key,
                    node.inner->pointers[node.inner->num_values]
                );
            } else {
                // leaf node
                if (node.leaf->num_values == 0) {
                    return false;
                }
                if (node.leaf->num_values > 2 * Kv) {
                    return false;
                }
                // check that keys are ordered
                for (size_t i = 1; i < node.leaf->num_values; ++i) {
                    const Key& k1 = node.leaf->keys[i - 1];
                    const Key& k2 = node.leaf->keys[i];
                    if (!(Compare{}(k1, k2) && k1 != k2)) {
                        return false;
                    }
                }
                // check that all keys are smaller or equal than right key
                if (right_key != nullptr) {
                    for (size_t i = 0; i < node.leaf->num_values; ++i) {
                        if (!Compare{}(node.leaf->keys[i], *right_key)) {
                            return false;
                        }
                    }
                }
                // check that all keys are greater than left key
                if (left_key != nullptr) {
                    for (size_t i = 0; i < node.leaf->num_values; ++i) {
                        if (Compare{}(node.leaf->keys[i], *left_key)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    void draw_dot(std::ostream& stream) {
        stream << "digraph tree {" << std::endl;
        stream << "graph [ranksep = 3, splines = line];" << std::endl;
        stream << "node [shape = record, height = 0.1];" << std::endl;
        size_t node_id_counter = 0;
        std::queue<std::tuple<size_t, size_t, node_pointer_t>> nodes;
        if (depth > 0) {
            size_t num_values;
            Key* keys;
            if (depth > 1) {
                num_values = root.inner->num_values;
                keys = root.inner->keys;
                nodes.emplace(0, 1, root);
            } else {
                num_values = root.leaf->num_values;
                keys = root.leaf->keys;
            }
            stream << "node0 [label = \"";
            for (size_t i = 0; i < num_values; ++i) {
                stream << "<p" << i << "> ";
                stream << "|" << keys[i] << "| ";
            }
            stream << "<p" << num_values << ">";
            stream << "\"];" << std::endl;
            ++node_id_counter;
        }
        while (!nodes.empty()) {
            size_t node_id;
            size_t node_depth;
            node_pointer_t node;
            std::tie(node_id, node_depth, node) = nodes.front();
            nodes.pop();
            for (size_t i = 0; i <= node.inner->num_values; ++i) {
                size_t child_node_id = node_id_counter;
                ++node_id_counter;
                size_t num_values;
                Key* keys;
                if (node_depth + 1 < depth) {
                    num_values = node.inner->pointers[i].inner->num_values;
                    keys = node.inner->pointers[i].inner->keys;
                    nodes.emplace(child_node_id, node_depth + 1, node.inner->pointers[i]);
                } else {
                    num_values = node.inner->pointers[i].leaf->num_values;
                    keys = node.inner->pointers[i].leaf->keys;
                }
                stream << "node" << child_node_id << " [label = \"";
                for (size_t i = 0; i < num_values; ++i) {
                    stream << "<p" << i << "> ";
                    stream << "|" << keys[i] << "| ";
                }
                stream << "<p" << num_values << ">";
                stream << "\"];" << std::endl;
                stream << "\"node" << node_id << "\":p" << i << ":s -> ";
                stream << "\"node" << child_node_id << '"' << std::endl;
            }
        }
        stream << "}" << std::endl;
    }

    template <typename Keys, typename Values>
    static BTree bulk_load(Keys keys, Values values, const size_t size) {
        // Given Kv and l (number of leaves), we know that the number of
        // elements n the tree can hold is:
        // n <= 2 * Kv * l
        // Solving for l results in:
        // l => n / (2 * Kv)
        // We write l = (n + 2 * Kv - 1) / (2 * Kv) here, so that using integer
        // division gives the correct result.
        const size_t num_leaves = (size + 2 * Kv - 1) / (2 * Kv);
#ifndef NDEBUG
        size_t input_index = 0;
#endif
        BTree tree{};
        if (num_leaves == 0) {
            return tree;
        }
        if (num_leaves == 1) {
            // New tree consists of only one leaf node.
            LeafNode* leaf = tree.leaf_alloc.allocate(1);
            new (leaf) LeafNode{};
            leaf->num_values = size;
            for (size_t i = 0; i < size; ++i) {
#ifndef NDEBUG
                assert(input_index < size);
#endif
                leaf->keys[i] = *keys;
                leaf->values[i] = *values;
                ++keys;
                ++values;
#ifndef NDEBUG
                ++input_index;
#endif
            }
            tree.root.leaf = leaf;
            tree.depth = 1;
            return tree;
        }
        auto leaves = std::make_unique<LeafNode*[]>(num_leaves);
        // Here and in the following code we can't allocate multiple node at
        // once (i.e. write tree.leaf_alloc.allocate(num_leaves)) because we
        // want to be able to deallocate them independently.
        for (size_t i = 0; i < num_leaves; ++i) {
            leaves[i] = tree.leaf_alloc.allocate(1);
        }
        // Fill all but last two leaves with input keys and values. The last
        // two leaves are handled separately because have to make sure that
        // all leaves contain at least Kv values, even if the number of
        // elements is not divisible by 2 * Kv.
        for (size_t i = 0; i < num_leaves - 2; ++i) {
            LeafNode* leaf = leaves[i];
            new (leaf) LeafNode{};
            leaf->num_values = 2 * Kv;
            if (i > 0) {
                leaf->prev_leaf = leaves[i - 1];
            }
            leaf->next_leaf = leaves[i + 1];
            for (size_t j = 0; j < 2 * Kv; ++j) {
#ifndef NDEBUG
                assert(input_index < size);
#endif
                leaf->keys[j] = *keys;
                leaf->values[j] = *values;
                ++keys;
                ++values;
#ifndef NDEBUG
                ++input_index;
#endif
            }
        }
        {
            // We completely filled num_leaves - 2 leaf nodes, which contain
            // (num_leaves - 2) * 2 * Kv values. We divide the remaining values
            // so that the last two leaves contain at least Kv values each.
            const size_t remaining = size - (num_leaves - 2) * 2 * Kv;
            const size_t values_first = (remaining + 1) / 2;
            const size_t values_second = remaining - values_first;
            LeafNode* first = leaves[num_leaves - 2];
            LeafNode* second = leaves[num_leaves - 1];
            new (first) LeafNode{};
            first->num_values = values_first;
            if (num_leaves > 2) {
                first->prev_leaf = leaves[num_leaves - 3];
            }
            first->next_leaf = second;
            for (size_t i = 0; i < values_first; ++i) {
                first->keys[i] = *keys;
                first->values[i] = *values;
                ++keys;
                ++values;
            }
            new (second) LeafNode{};
            second->num_values = values_second;
            second->prev_leaf = first;
            for (size_t i = 0; i < values_second; ++i) {
                second->keys[i] = *keys;
                second->values[i] = *values;
                ++keys;
                ++values;
            }
        }
        tree.depth = 2;
        // Same calculation as above. If we have n inner nodes and know K, the
        // number l of leaves that can be referenced is given by:
        // l <= (2 * K + 1) * n
        // Solving for n:
        // n => l / (2 * K + 1)
        // Again, because of integer division we write
        // n = (l + 2 * K) / (2 * K + 1)
        size_t num_inner_nodes = (num_leaves + 2 * K) / (2 * K + 1);
        if (num_inner_nodes == 1) {
            // New tree consists of one inner node (the root) and the leaves.
            InnerNode* node = tree.inner_alloc.allocate(1);
            new (node) InnerNode{};
            node->num_values = num_leaves - 1;
            for (size_t i = 0; i < num_leaves - 1; ++i) {
                node->keys[i] = leaves[i]->last_key();
                node->pointers[i].leaf = leaves[i];
            }
            node->pointers[num_leaves - 1].leaf = leaves[num_leaves - 1];
            tree.root.inner = node;
            return tree;
        }
        auto nodes = std::make_unique<InnerNode*[]>(num_inner_nodes);
        for (size_t i = 0; i < num_inner_nodes; ++i) {
            nodes[i] = tree.inner_alloc.allocate(1);
        }
        {
            // Fill all inner nodes of the next layer with the leaves.
            LeafNode** leaf_p = leaves.get();
            for (size_t i = 0; i < num_inner_nodes - 2; ++i) {
                new (nodes[i]) InnerNode{};
                nodes[i]->num_values = 2 * K;
                for (size_t j = 0; j < 2 * K; ++j) {
                    LeafNode* leaf = *leaf_p;
                    ++leaf_p;
                    nodes[i]->keys[j] = leaf->last_key();
                    nodes[i]->pointers[j].leaf = leaf;
                }
                nodes[i]->pointers[2 * K].leaf = *leaf_p;
                ++leaf_p;
            }
            const size_t remaining = num_leaves - (num_inner_nodes - 2) * (2 * K + 1);
            const size_t values_first = (remaining + 1) / 2;
            const size_t values_second = remaining - values_first;
            InnerNode* first = nodes[num_inner_nodes - 2];
            InnerNode* second = nodes[num_inner_nodes - 1];
            new (first) InnerNode{};
            first->num_values = values_first - 1;
            for (size_t i = 0; i < values_first - 1; ++i) {
                LeafNode* leaf = *leaf_p;
                ++leaf_p;
                first->keys[i] = leaf->last_key();
                first->pointers[i].leaf = leaf;
            }
            first->pointers[values_first - 1].leaf = *leaf_p;
            ++leaf_p;
            new (second) InnerNode{};
            second->num_values = values_second - 1;
            for (size_t i = 0; i < values_second - 1; ++i) {
                LeafNode* leaf = *leaf_p;
                ++leaf_p;
                second->keys[i] = leaf->last_key();
                second->pointers[i].leaf = leaf;
            }
            second->pointers[values_second - 1].leaf = *leaf_p;
            ++leaf_p;
        }
        leaves.reset(nullptr);
        while (true) {
            // Now create more layers of inner nodes until we reach a layer
            // that only has one node.
            ++tree.depth;
            size_t new_num_inner_nodes = (num_inner_nodes + 2 * K) / (2 * K + 1);
            if (new_num_inner_nodes == 1) {
                // Last layer, create root node.
                InnerNode* node = tree.inner_alloc.allocate(1);
                new (node) InnerNode{};
                node->num_values = num_inner_nodes - 1;
                for (size_t i = 0; i < num_inner_nodes - 1; ++i) {
                    node->keys[i] = rightmost_key(nodes[i], tree.depth - 1);
                    node->pointers[i].inner = nodes[i];
                }
                node->pointers[num_inner_nodes - 1].inner = nodes[num_inner_nodes - 1];
                tree.root.inner = node;
                return tree;
            }
            auto new_nodes = std::make_unique<InnerNode*[]>(new_num_inner_nodes);
            for (size_t i = 0; i < new_num_inner_nodes; ++i) {
                new_nodes[i] = tree.inner_alloc.allocate(1);
            }
            {
                InnerNode** node_p = nodes.get();
                for (size_t i = 0; i < new_num_inner_nodes - 2; ++i) {
                    new (new_nodes[i]) InnerNode{};
                    new_nodes[i]->num_values = 2 * K;
                    for (size_t j = 0; j < 2 * K; ++j) {
                        InnerNode* node = *node_p;
                        ++node_p;
                        new_nodes[i]->keys[j] = rightmost_key(node, tree.depth - 1);
                        new_nodes[i]->pointers[j].inner = node;
                    }
                    new_nodes[i]->pointers[2 * K].inner = *node_p;
                    ++node_p;
                }
                const size_t remaining = num_inner_nodes - (new_num_inner_nodes - 2) * (2 * K + 1);
                const size_t values_first = (remaining + 1) / 2;
                const size_t values_second = remaining - values_first;
                InnerNode* first = new_nodes[new_num_inner_nodes - 2];
                InnerNode* second = new_nodes[new_num_inner_nodes - 1];
                new (first) InnerNode{};
                first->num_values = values_first - 1;
                for (size_t i = 0; i < values_first - 1; ++i) {
                    InnerNode* node = *node_p;
                    ++node_p;
                    first->keys[i] = rightmost_key(node, tree.depth - 1);
                    first->pointers[i].inner = node;
                }
                first->pointers[values_first - 1].inner = *node_p;
                ++node_p;
                new (second) InnerNode{};
                second->num_values = values_second - 1;
                for (size_t i = 0; i < values_second - 1; ++i) {
                    InnerNode* node = *node_p;
                    ++node_p;
                    second->keys[i] = rightmost_key(node, tree.depth - 1);
                    second->pointers[i].inner = node;
                }
                second->pointers[values_second - 1].inner = *node_p;
                ++node_p;
            }
            num_inner_nodes = new_num_inner_nodes;
            nodes = std::move(new_nodes);
        }
    }

    iterator begin() {
        if (depth == 0) {
            return end();
        }
        return iterator{&leftmost_leaf(root, depth), 0};
    }

    const_iterator begin() const {
        return static_cast<const_iterator>(const_cast<BTree*>(this)->begin());
    }

    const_iterator cbegin() const {
        return begin();
    }

    iterator end() {
        if (depth == 0) {
            return iterator{nullptr, nullptr, 0};
        }
        return iterator{&rightmost_leaf(root, depth), nullptr, 0};
    }

    const_iterator end() const {
        return static_cast<const_iterator>(const_cast<BTree*>(this)->end());
    }

    const_iterator cend() const {
        return end();
    }

    iterator find(const Key& key) {
        if (depth == 0) {
            return end();
        }
        auto& leaf = find_leaf(key);
        size_t index = key_index(leaf, key);
        if (index < leaf.num_values && key == leaf.keys[index]) {
            return iterator{&leaf, index};
        }
        return end();
    }

    const_iterator find(const Key& key) const {
        return static_cast<const_iterator>(const_cast<BTree*>(this)->find(key));
    }

    iterator find_greater_eq(const Key& key) {
        if (depth == 0) {
            return end();
        }
        auto& leaf = find_leaf(key);
        size_t index = key_index(leaf, key);
        if (index < leaf.num_values) {
            return iterator{&leaf, index};
        } else {
            return iterator{&leaf, leaf.next_leaf, 0};
        }
    }

    const_iterator find_greater_eq(const Key& key) const {
        return static_cast<const_iterator>(const_cast<BTree*>(this)->find_greater_eq(key));
    }

    template <typename KeyF, typename ValueF>
    iterator insert(KeyF&& key, ValueF&& value, const bool update = true) {
        if (depth == 0) {
            LeafNode* new_root = leaf_alloc.allocate(1);
            new (new_root) LeafNode{};
            new_root->num_values = 1;
            new_root->keys[0] = std::forward<KeyF>(key);
            new_root->values[0] = std::forward<ValueF>(value);
            root.leaf = new_root;
            depth = 1;
            return iterator{new_root, 0};
        }
        LeafNode& leaf = find_leaf(key);
        size_t index = key_index(leaf, key);
        if (index < leaf.num_values && key == leaf.keys[index]) {
            if (update) {
                // Update existing key.
                leaf.values[index] = std::forward<ValueF>(value);
            }
            return iterator{&leaf, index};
        }
        // Leaf node has space left.
        if (leaf.num_values < 2 * Kv) {
            if (index < leaf.num_values) {
                move_keys(leaf, index);
                move_values(leaf, index);
            } else {
                assert(index == leaf.num_values);
            }
            leaf.keys[index] = std::forward<KeyF>(key);
            leaf.values[index] = std::forward<ValueF>(value);
            ++leaf.num_values;
            return iterator{&leaf, index};
        }
        // Split leaf node.
        assert(leaf.num_values == 2 * Kv);
        LeafNode* it_leaf;
        std::vector<node_pointer_t> nodes = find_leaf_with_parents(key);
        LeafNode* new_leaf = leaf_alloc.allocate(1);
        new (new_leaf) LeafNode{};
        if (index <= Kv) {
            // New key is inserted into left node, i.e. the one that contains
            // the smaller keys after splitting.
            copy_keys(leaf, *new_leaf, Kv);
            copy_values(leaf, *new_leaf, Kv);
            leaf.num_values = Kv;
            new_leaf->num_values = Kv;
            if (index < Kv) {
                move_keys(leaf, index);
                move_values(leaf, index);
            }
            leaf.keys[index] = std::forward<KeyF>(key);
            leaf.values[index] = std::forward<ValueF>(value);
            leaf.num_values = Kv + 1;
            it_leaf = &leaf;
        } else {
            // New key is inserted into right node.
            copy_keys(leaf, *new_leaf, Kv + 1);
            copy_values(leaf, *new_leaf, Kv + 1);
            leaf.num_values = Kv + 1;
            new_leaf->num_values = Kv - 1;
            index = index - (Kv + 1);
            if (index < Kv - 1) {
                move_keys(*new_leaf, index);
                move_values(*new_leaf, index);
            } else {
                assert(index == Kv - 1);
            }
            new_leaf->keys[index] = std::forward<KeyF>(key);
            new_leaf->values[index] = std::forward<ValueF>(value);
            new_leaf->num_values = Kv;
            it_leaf = new_leaf;
        }
        if (leaf.next_leaf != nullptr) {
            leaf.next_leaf->prev_leaf = new_leaf;
        }
        new_leaf->next_leaf = leaf.next_leaf;
        new_leaf->prev_leaf = &leaf;
        leaf.next_leaf = new_leaf;
        assert(
            leaf.num_values == Kv + 1 &&
            new_leaf->num_values == Kv
        );
        iterator return_it{it_leaf, index};
        node_pointer_t left_child;
        left_child.leaf = &leaf;
        node_pointer_t right_child;
        right_child.leaf = new_leaf;
        Key push_key = leaf.keys[Kv];
        const auto end = nodes.rend();
        auto it = nodes.rbegin();
        ++it;
        // Push key into parent nodes until one node has free space or a new
        // root is created.
        while (true) {
            if (it == end) {
                // Create new root.
                InnerNode* new_root = inner_alloc.allocate(1);
                new (new_root) InnerNode{};
                new_root->keys[0] = push_key;
                new_root->pointers[0] = left_child;
                new_root->pointers[1] = right_child;
                new_root->num_values = 1;
                root.inner = new_root;
                ++depth;
                return return_it;
            }
            InnerNode& node = *it->inner;
            index = key_index(node, push_key);
            assert(node.pointers[index] == left_child);
            // Node has space left.
            if (node.num_values < 2 * K) {
                if (index < node.num_values) {
                    move_keys(node, index);
                    move_pointers(node, index + 1);
                } else {
                    assert(index == node.num_values);
                }
                node.keys[index] = std::move(push_key);
                node.pointers[index + 1] = right_child;
                ++node.num_values;
                return return_it;
            }
            // Split inner node.
            assert(node.num_values == 2 * K);
            InnerNode* new_node = inner_alloc.allocate(1);
            new (new_node) InnerNode{};
            if (index == K) {
                // Pushed key is the new middle element again.
                copy_keys(node, *new_node, K);
                copy_pointers(node, *new_node, K + 1, 1);
                new_node->pointers[0] = right_child;
                node.num_values = K;
                new_node->num_values = K;
            } else if (index < K) {
                // Pushed key is inserted into left node.
                copy_keys(node, *new_node, K);
                copy_pointers(node, *new_node, K);
                node.num_values = K;
                new_node->num_values = K;
                move_keys(node, index);
                move_pointers(node, index + 1);
                node.keys[index] = std::move(push_key);
                node.pointers[index + 1] = right_child;
                push_key = std::move(node.keys[K]);
            } else {
                // Pushed key is inserted into right node.
                copy_keys(node, *new_node, K);
                copy_pointers(node, *new_node, K + 1, 1);
                node.num_values = K;
                new_node->num_values = K;
                index = index - K;
                if (index < K) {
                    move_keys(*new_node, index);
                    move_pointers(*new_node, index + 1);
                } else {
                    assert(index == K);
                }
                new_node->keys[index] = std::move(push_key);
                new_node->pointers[index + 1] = right_child;
                new_node->num_values = K + 1;
                push_key = std::move(new_node->keys[0]);
                move_backwards_keys(*new_node, 0);
                move_backwards_pointers(*new_node, 0);
                new_node->num_values = K;
            }
            assert(
                node.num_values == K &&
                new_node->num_values == K
            );
            left_child.inner = &node;
            right_child.inner = new_node;
            ++it;
        }
    }
};


template <
    typename Key,
    typename Value,
    size_t K,
    size_t Kv,
    typename Compare = std::less_equal<Key>,
    typename Allocator = std::allocator<Key>
>
class LinkedBTree
: public BTree<Key, Value, K, Kv, Compare, Allocator> {
public:
    using Base = BTree<Key, Value, K, Kv, Compare, Allocator>;

    char* parent;
    LinkedBTree* prev;
    LinkedBTree* next;

    LinkedBTree() : Base(), parent(nullptr), prev(nullptr), next(nullptr)  {}

    LinkedBTree(const Allocator& alloc)
    : Base(alloc), parent(nullptr), prev(nullptr), next(nullptr) {}

    LinkedBTree(LinkedBTree&& b) : Base(static_cast<Base&&>(b)) {
        parent = b.parent;
        prev = b.prev;
        next = b.next;
    }

    LinkedBTree& operator=(LinkedBTree&& b) {
        static_cast<Base&>(*this) = static_cast<Base&&>(b);
        parent = b.parent;
        prev = b.prev;
        next = b.next;
        return *this;
    }
};


#endif
