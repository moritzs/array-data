#ifndef _ARRAY_DATA_STORAGE_DENSE_{{ name }}_HPP
#define _ARRAY_DATA_STORAGE_DENSE_{{ name }}_HPP

#include <algorithm>
#include <array>
#include <iterator>
{% if enable_timers %}
#include <memory>
{% endif %}
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
#include "column_store.hpp"
#include "threading.hpp"
{% if enable_timers %}
#include "timer.hpp"
{% endif %}
#include "utils.hpp"
{% for include in extra_includes %}
#include {{ include }}
{% endfor %}


class storage_dense_{{ name }} {
public:
    static constexpr size_t NUM_DIMENSIONS = {{ num_dimensions }};
    using tuple_type = std::tuple<{{ data_types_list }}>;
    using dim_tuple_type = std::tuple<{{ dimension_types_list }}>;
    using data_t = column_store<{{ data_types_list }}>;

private:
    data_t data;
    std::array<ssize_t, NUM_DIMENSIONS> dim_min;
    std::array<ssize_t, NUM_DIMENSIONS> dim_max;
    std::array<size_t, NUM_DIMENSIONS> dim_size;
    std::array<size_t, NUM_DIMENSIONS> dim_step;

    template <typename Dim, typename C>
    class dimension_iterator {
    public:
        using value_type = std::pair<Dim, C>;
        using difference_type = ssize_t;
        using reference = value_type;
        using pointer = utils::value_pointer<value_type>;
        using iterator_category = std::random_access_iterator_tag;

    private:
        friend class storage_dense_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;

        C container;
        Dim dim_value;
        const size_t step;

        dimension_iterator(const C& container, const Dim dim_value, const size_t step)
        : container(container), dim_value(dim_value), step(step) {}

    public:
        dimension_iterator() = default;

        reference operator*() const {
            return std::make_pair(dim_value, container);
        }

        pointer operator->() const {
            return pointer{std::make_pair(dim_value, container)};
        }

        dimension_iterator& operator++() {
            container.base_offset += step;
            ++dim_value;
            return *this;
        }

        dimension_iterator operator++(int) const {
            dimension_iterator it{*this};
            ++it;
            return it;
        }

        dimension_iterator& operator+=(const difference_type diff) {
            container.base_offset += step * diff;
            dim_value += diff;
            return *this;
        }

        dimension_iterator operator+(const difference_type diff) const {
            dimension_iterator it{*this};
            it += diff;
            return it;
        }

        dimension_iterator& operator--() {
            container.base_offset -= step;
            --dim_value;
            return *this;
        }

        dimension_iterator operator--(int) const {
            dimension_iterator it{*this};
            --it;
            return it;
        }

        dimension_iterator& operator-=(const difference_type diff) {
            container.base_offset -= step * diff;
            dim_value -= diff;
            return *this;
        }

        dimension_iterator operator-(const difference_type diff) const {
            dimension_iterator it{*this};
            it -= diff;
            return it;
        }

        difference_type operator-(const dimension_iterator& it) const {
            return static_cast<difference_type>(dim_value) - it.dim_value;
        }

        reference operator[](const difference_type offset) const {
            C container_copy{container};
            container_copy.base_offset += step * offset;
            Dim new_dim_value = dim_value + offset;
            return std::make_pair(new_dim_value, container_copy);
        }

        bool operator==(const dimension_iterator& it) const {
            return dim_value == it.dim_value;
        }

        bool operator!=(const dimension_iterator& it) const {
            return dim_value != it.dim_value;
        }

        bool operator<(const dimension_iterator& it) const {
            return dim_value < it.dim_value;
        }

        bool operator<=(const dimension_iterator& it) const {
            return dim_value <= it.dim_value;
        }

        bool operator>(const dimension_iterator& it) const {
            return dim_value > it.dim_value;
        }

        bool operator>=(const dimension_iterator& it) const {
            return dim_value >= it.dim_value;
        }
    };

    template <bool is_const, size_t D, typename = void>
    class dimension_container_base;

    template <bool is_const, size_t D>
    class dimension_container_base<is_const, D, std::enable_if_t<(D + 2 < NUM_DIMENSIONS)>> {
    public:
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using iterator = dimension_iterator<next_dim_t, dimension_container_base<is_const, D + 1>>;

    private:
        friend class storage_dense_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;
        template <typename, typename> friend class dimension_iterator;

        using storage_t = std::conditional_t<
            is_const, const storage_dense_{{ name }}, storage_dense_{{ name }}
        >;

        storage_t* storage;
        size_t base_offset;

        dimension_container_base(storage_t* storage, const size_t base_offset)
        : storage(storage), base_offset(base_offset) {}

    public:
        dimension_container_base() : storage(nullptr) {}

        dimension_container_base<is_const, D + 1> operator[](
            const next_dim_t next_dim_value
        ) const {
            size_t new_offset = (
                base_offset +
                storage->dim_step[D + 1] * (next_dim_value - storage->dim_min[D + 1])
            );
            return dimension_container_base<is_const, D + 1>{storage, new_offset};
        }

        iterator begin() const {
            return iterator{
                dimension_container_base<is_const, D + 1>{storage, base_offset},
                static_cast<next_dim_t>(storage->dim_min[D + 1]),
                storage->dim_step[D + 1]
            };
        }

        iterator end() const {
            auto dim_max = storage->dim_max[D + 1] + 1;
            auto step = storage->dim_step[D + 1];
            auto offset = base_offset + storage->dim_size[D + 1] * step;
            return iterator{
                dimension_container_base<is_const, D + 1>{storage, offset},
                static_cast<next_dim_t>(dim_max),
                step
            };
        }
    };

    template <bool is_const>
    class data_iterator {
    private:
        using column_store_ref = std::conditional_t<
            is_const, data_t::const_tuple_reference, data_t::tuple_reference
        >;
        using Dim = std::tuple_element_t<NUM_DIMENSIONS - 1, dim_tuple_type>;

    public:
        using value_type = std::pair<Dim, column_store_ref>;
        using difference_type = ssize_t;
        using reference = value_type;
        using pointer = utils::value_pointer<value_type>;
        using iterator_category = std::random_access_iterator_tag;

    private:
        friend class storage_dense_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;

        data_ref_t* data;
        size_t index;
        Dim dim_value;

        data_iterator(data_ref_t* data, const size_t index, const Dim dim_value)
        : data(data), index(index), dim_value(dim_value) {}

    public:
        data_iterator() = default;

        reference operator*() const {
            return std::make_pair(dim_value, (*data)[index]);
        }

        pointer operator->() const {
            return pointer{std::make_pair(dim_value, (*data)[index])};
        }

        data_iterator& operator++() {
            ++index;
            ++dim_value;
            return *this;
        }

        data_iterator operator++(int) const {
            data_iterator it{*this};
            ++it;
            return it;
        }

        data_iterator& operator+=(const difference_type diff) {
            index += diff;
            dim_value += diff;
            return *this;
        }

        data_iterator operator+(const difference_type diff) const {
            data_iterator it{*this};
            it += diff;
            return it;
        }

        data_iterator& operator--() {
            --index;
            --dim_value;
            return *this;
        }

        data_iterator operator--(int) const {
            data_iterator it{*this};
            --it;
            return it;
        }

        data_iterator& operator-=(const difference_type diff) {
            index -= diff;
            dim_value -= diff;
            return *this;
        }

        data_iterator operator-(const difference_type diff) const {
            data_iterator it{*this};
            it -= diff;
            return it;
        }

        difference_type operator-(const data_iterator& it) const {
            return static_cast<difference_type>(dim_value) - it.dim_value;
        }

        reference operator[](const difference_type offset) const {
            return std::make_pair(dim_value + offset, (*data)[index + offset]);
        }

        bool operator==(const data_iterator& it) const {
            return dim_value == it.dim_value;
        }

        bool operator!=(const data_iterator& it) const {
            return dim_value != it.dim_value;
        }

        bool operator<(const data_iterator& it) const {
            return dim_value < it.dim_value;
        }

        bool operator<=(const data_iterator& it) const {
            return dim_value <= it.dim_value;
        }

        bool operator>(const data_iterator& it) const {
            return dim_value > it.dim_value;
        }

        bool operator>=(const data_iterator& it) const {
            return dim_value >= it.dim_value;
        }
    };

    template <bool is_const, size_t D>
    class dimension_container_base<is_const, D, std::enable_if_t<D + 2 == NUM_DIMENSIONS>> {
    public:
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using iterator = data_iterator<is_const>;

        template <size_t I>
        using value_type_t = std::conditional_t<
            is_const,
            const std::tuple_element_t<I, tuple_type>,
            std::tuple_element_t<I, tuple_type>
        >;
        using column_store_ref = std::conditional_t<
            is_const, data_t::const_tuple_reference, data_t::tuple_reference
        >;

    private:
        friend class storage_dense_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;
        template <typename, typename> friend class dimension_iterator;

        using storage_t = std::conditional_t<
            is_const, const storage_dense_{{ name }}, storage_dense_{{ name }}
        >;

        storage_t* storage;
        size_t base_offset;

        dimension_container_base(storage_t* storage, const size_t base_offset)
        : storage(storage), base_offset(base_offset) {}

    public:
        dimension_container_base() : storage(nullptr) {}

        template <size_t I>
        value_type_t<I>& get(const next_dim_t next_dim_value) const {
            return storage->get<I>(base_offset + (next_dim_value - storage->dim_min[D]));
        }

        template <size_t I>
        value_type_t<I>* get() const {
            return &storage->get<I>(base_offset);
        }

        column_store_ref operator[](const next_dim_t dim) const {
            return storage->data[base_offset + (dim - storage->dim_min[D + 1])];
        }

        iterator begin() const {
            return iterator{
                &storage->data,
                base_offset,
                static_cast<next_dim_t>(storage->dim_min[D + 1])
            };
        }

        iterator end() const {
            auto dim_max = storage->dim_max[D + 1] + 1;
            auto offset = base_offset + dim_max;
            return iterator{
                &storage->data,
                offset,
                static_cast<next_dim_t>(dim_max)
            };
        }
    };

public:
    template <size_t D> using dimension_container = dimension_container_base<false, D>;
    template <size_t D> using const_dimension_container = dimension_container_base<true, D>;
    using iterator = dimension_iterator<
        std::tuple_element_t<0, dim_tuple_type>,
        dimension_container<0>
    >;
    using const_iterator = dimension_iterator<
        std::tuple_element_t<0, dim_tuple_type>,
        const_dimension_container<0>
    >;
    using first_dim_t = std::tuple_element_t<0, dim_tuple_type>;

private:
    void update_dim_step() {
        dim_step[NUM_DIMENSIONS - 1] = 1;
        for (size_t i = 1; i < NUM_DIMENSIONS; ++i) {
            dim_step[NUM_DIMENSIONS - 1 - i] = (
                dim_step[NUM_DIMENSIONS - i] *
                dim_size[NUM_DIMENSIONS - i]
            );
        }
    }

    size_t get_index(
        {% for type in dimension_types %}
            const {{ type }} dim{{ loop.index0 }}{% if not loop.last %},{% endif %}

        {% endfor %}
    ) const {
        return (
            {% for _ in dimension_types %}
                (dim{{ loop.index0 }} - dim_min[{{ loop.index0 }}])
                    {% for acc in range(loop.index0 + 1, num_dimensions) %}
                        * dim_size[{{ acc }}]
                    {% endfor %}
                    {% if not loop.last %}
                        +
                    {% endif %}
            {% endfor %}
        );
    }

public:
    storage_dense_{{ name }}() = default;

    storage_dense_{{ name }}(
        const std::array<ssize_t, NUM_DIMENSIONS> dim_min,
        const std::array<ssize_t, NUM_DIMENSIONS> dim_max
    ) : dim_min(dim_min), dim_max(dim_max) {
        for (size_t i = 0; i < NUM_DIMENSIONS; ++i) {
            dim_size[i] = dim_max[i] - dim_min[i] + 1;
        }
        update_dim_step();
        size_t size = 1;
        for (size_t i = 0; i < NUM_DIMENSIONS; ++i) {
            size *= dim_size[i];
        }
        data = data_t{size};
    }

    storage_dense_{{ name }}(const column_store<{{ input_types_list }}>& input) {
        {% for index in dimension_indexes %}
        {
            ssize_t value = input.get<{{ index }}>(0);
            dim_min[{{ loop.index0 }}] = value;
            dim_max[{{ loop.index0 }}] = value;
        }
        {% endfor %}
        for (size_t i = 1; i < input.size(); ++i) {
            {% for index in dimension_indexes %}
            {
                ssize_t value = input.get<{{ index }}>(i);
                ssize_t& min = dim_min[{{ loop.index0 }}];
                ssize_t& max = dim_max[{{ loop.index0 }}];
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }
            }
            {% endfor %}
        }
        for (size_t i = 0; i < NUM_DIMENSIONS; ++i) {
            dim_size[i] = dim_max[i] - dim_min[i] + 1;
        }
        update_dim_step();
        data = data_t{input.size()};
        for (size_t i = 0; i < input.size(); ++i) {
            size_t index = get_index(
                {% for index in dimension_indexes %}
                    input.get<{{ index }}>(i){% if not loop.last %},{% endif %}

                {% endfor %}
            );
            {% for index in data_indexes %}
                data.get<{{ loop.index0 }}>(index) = input.get<{{ index }}>(i);
            {% endfor %}
        }
    }

    class threaded_loader {
    private:
        friend class storage_dense_{{ name }};

        const size_t num_threads;
        storage_dense_{{ name }}& storage;
        const column_store<{{ input_types_list }}>& input;
        threading::barrier<threading::spin_lock> barrier;
        std::vector<std::array<ssize_t, NUM_DIMENSIONS>> thread_dim_min;
        std::vector<std::array<ssize_t, NUM_DIMENSIONS>> thread_dim_max;
        {% if enable_timers %}
            std::unique_ptr<utils::timer> timer;
        {% endif %}

        threaded_loader(
            const size_t num_threads,
            storage_dense_{{ name }}& storage,
            const column_store<{{ input_types_list }}>& input
        ) : num_threads(num_threads), storage(storage), input(input), barrier{num_threads} {}

    public:
        threaded_loader(threaded_loader&& ts)
        : num_threads(ts.num_threads), storage(ts.storage), input(ts.input), barrier{0} {
            // This class is not movable because the barrier is not movable. But to be able to
            // return this from make_threaded() a move constructor must be defined. There, the
            // compiler won't actually use it but instead elides the copy. In C++17 this return
            // value optimization is guaranteed to happen, but in C++14 it's optional and at least
            // GCC requires a move constructor even if it doesn't use it (not even with -O0).
            throw "not movable";
        }

        void operator()(const size_t thread_id) {
            barrier.wait_run_once([this] {
                {% if enable_timers %}
                    timer = std::make_unique<utils::timer>("get min and max values");
                {% endif %}
                thread_dim_min = std::vector<std::array<ssize_t, NUM_DIMENSIONS>>(num_threads);
                thread_dim_max = std::vector<std::array<ssize_t, NUM_DIMENSIONS>>(num_threads);
            });
            auto& dim_min = thread_dim_min[thread_id];
            auto& dim_max = thread_dim_max[thread_id];
            {% for index in dimension_indexes %}
            {
                ssize_t value = input.get<{{ index }}>(0);
                dim_min[{{ loop.index0 }}] = value;
                dim_max[{{ loop.index0 }}] = value;
            }
            {% endfor %}
            for (size_t i = thread_id; i < input.size(); i += num_threads) {
                {% for index in dimension_indexes %}
                {
                    ssize_t value = input.get<{{ index }}>(i);
                    ssize_t& min = dim_min[{{ loop.index0 }}];
                    ssize_t& max = dim_max[{{ loop.index0 }}];
                    if (value < min) {
                        min = value;
                    }
                    if (value > max) {
                        max = value;
                    }
                }
                {% endfor %}
            }
            barrier.wait_run_once([this] {
                std::vector<ssize_t> min_values(num_threads);
                std::vector<ssize_t> max_values(num_threads);
                for (size_t i = 0; i < NUM_DIMENSIONS; ++i) {
                    for (size_t j = 0; j < num_threads; ++j) {
                        min_values[j] = thread_dim_min[j][i];
                        max_values[j] = thread_dim_max[j][i];
                    }
                    storage.dim_min[i] = *std::min_element(min_values.begin(), min_values.end());
                    storage.dim_max[i] = *std::max_element(max_values.begin(), max_values.end());
                }
                for (size_t i = 0; i < NUM_DIMENSIONS; ++i) {
                    storage.dim_size[i] = storage.dim_max[i] - storage.dim_min[i] + 1;
                }
                storage.dim_step[NUM_DIMENSIONS - 1] = 1;
                for (size_t i = 1; i < NUM_DIMENSIONS; ++i) {
                    storage.dim_step[NUM_DIMENSIONS - 1 - i] = (
                        storage.dim_step[NUM_DIMENSIONS - i] *
                        storage.dim_size[NUM_DIMENSIONS - i]
                    );
                }
                {% if enable_timers %}
                    timer->replace("allocate data");
                {% endif %}
                storage.data = data_t{input.size()};
                {% if enable_timers %}
                    timer->replace("insert values");
                {% endif %}
            });
            for (size_t i = thread_id; i < input.size(); i += num_threads) {
                size_t index = storage.get_index(
                    {% for index in dimension_indexes %}
                        input.get<{{ index }}>(i){% if not loop.last %},{% endif %}

                    {% endfor %}
                );
                {% for index in data_indexes %}
                    storage.data.get<{{ loop.index0 }}>(index) = input.get<{{ index }}>(i);
                {% endfor %}
            }
            {% if enable_timers %}
                barrier.wait_run_once([this] {
                    timer.reset(nullptr);
                });
            {% else %}
                barrier.wait();
            {% endif %}
        }
    };

    threaded_loader load_threaded(
        const column_store<{{ input_types_list }}>& input,
        const size_t num_threads
    ) {
        return threaded_loader{num_threads, *this, input};
    }

    std::unique_ptr<threaded_loader> load_threaded_ptr(
        const column_store<{{ input_types_list }}>& input,
        const size_t num_threads
    ) {
        return std::unique_ptr<threaded_loader>(new threaded_loader{num_threads, *this, input});
    }

    const data_t& get_data() const {
        return data;
    }

    const std::array<ssize_t, NUM_DIMENSIONS>& get_dim_min() const {
        return dim_min;
    }

    const std::array<ssize_t, NUM_DIMENSIONS>& get_dim_max() const {
        return dim_max;
    }

    const std::array<size_t, NUM_DIMENSIONS>& get_dim_size() const {
        return dim_size;
    }

    template <size_t I>
    typename std::tuple_element<I, tuple_type>::type& get(
        {% for type in dimension_types %}
            const {{ type }} dim{{ loop.index0 }}{% if not loop.last %},{% endif %}

        {% endfor %}
    ) {
        size_t index = get_index(
            {% for _ in dimension_types %}
                dim{{ loop.index0 }}{% if not loop.last %},{% endif %}

            {% endfor %}
        );
        return data.get<I>(index);
    }

    dimension_container<0> operator[](const first_dim_t dim) {
        size_t offset = (dim - dim_min[0]) * dim_step[0];
        return dimension_container<0>{this, offset};
    }

    const_dimension_container<0> operator[](const first_dim_t dim) const {
        size_t offset = (dim - dim_min[0]) * dim_step[0];
        return const_dimension_container<0>{this, offset};
    }

    iterator begin() {
        return iterator{
            dimension_container<0>{this, 0},
            static_cast<first_dim_t>(dim_min[0]),
            dim_step[0]
        };
    }

    iterator end() {
        auto offset = dim_size[0] * dim_step[0];
        return iterator{
            dimension_container<0>{this, offset},
            static_cast<first_dim_t>(dim_max[0] + 1),
            dim_step[0]
        };
    }

    const_iterator cbegin() const {
        return const_iterator{
            const_dimension_container<0>{this, 0},
            static_cast<first_dim_t>(dim_min[0]),
            dim_step[0]
        };
    }

    const_iterator begin() const {
        return cbegin();
    }

    const_iterator cend() const {
        auto offset = dim_size[0] * dim_step[0];
        return const_iterator{
            const_dimension_container<0>{this, offset},
            static_cast<first_dim_t>(dim_max[0] + 1),
            dim_step[0]
        };
    }

    const_iterator end() const {
        return cend();
    }
};

#endif
