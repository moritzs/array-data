#!/usr/bin/env python3

import copy
import os
import os.path
import jinja2


class StorageGenBase:
    template_name = None
    filename_template = None
    can_enable_timers = False

    def __init__(self, name, input_types, dimension_indexes, data_indexes, extra_includes=None,
                 enable_timers=False):
        self.name = name
        self.input_types = input_types
        self.dimension_indexes = dimension_indexes
        self.data_indexes = data_indexes
        if extra_includes is None:
            extra_includes = []
        else:
            extra_includes = list(extra_includes)
        self.extra_includes = extra_includes
        self.enable_timers = enable_timers

    def get_filename(self):
        if self.enable_timers:
            suffix = '_timers'
        else:
            suffix = ''
        return self.filename_template.format(name=self.name, suffix=suffix)

    def get_template_context(self):
        context = {
            'name': self.name,
            'input_types': self.input_types,
            'dimension_indexes': self.dimension_indexes,
            'data_indexes': self.data_indexes,
            'extra_includes': self.extra_includes,
            'enable_timers': self.enable_timers,
        }
        context['num_dimensions'] = len(self.dimension_indexes)
        context['input_types_list'] = ', '.join(self.input_types)
        context['dimension_types'] = [self.input_types[i] for i in self.dimension_indexes]
        context['dimension_type_indexes'] = [
            (self.input_types[i], i) for i in self.dimension_indexes
        ]
        context['dimension_types_list'] = ', '.join(context['dimension_types'])
        context['data_types'] = [self.input_types[i] for i in self.data_indexes]
        context['num_data_columns'] = len(self.data_indexes)
        context['data_type_indexes'] = [(self.input_types[i], i) for i in self.data_indexes]
        context['data_types_list'] = ', '.join(context['data_types'])
        return context

    def generate(self, env, fileobj):
        template = env.get_template(self.template_name)
        stream = template.stream(self.get_template_context())
        stream.dump(fileobj)


class DenseStorageGen(StorageGenBase):
    template_name = 'storage_dense.hpp.j2'
    filename_template = 'storage_dense_{name}{suffix}.hpp'
    can_enable_timers = True


class SparseStorageGen(StorageGenBase):
    template_name = 'storage_sparse.hpp.j2'
    filename_template = 'storage_sparse_{name}{suffix}.hpp'

    def get_tree_types(self, dimension_types, leaf_tree, inner_tree):
        trees = []
        dim_iter = reversed(dimension_types)
        trees.append(leaf_tree.format(dim_type=next(dim_iter)))
        for dim_type in dim_iter:
            child_tree = trees[-1]
            trees.append(inner_tree.format(dim_type=dim_type, child_tree=child_tree))
        trees.reverse()
        return trees

    def get_template_context(self):
        context = super().get_template_context()
        if 'MAX_TUPLES_IN_INTERVAL' in os.environ:
            context['max_tuples_in_interval'] = int(os.environ['MAX_TUPLES_IN_INTERVAL'])
        else:
            context['max_tuples_in_interval'] = 1000
        if 'MAX_TUPLES_BASE' in os.environ:
            context['max_tuples_base'] = int(os.environ['MAX_TUPLES_BASE'])
        else:
            context['max_tuples_base'] = 5

        def get_tree_types(*args, **kwargs):
            return self.get_tree_types(context['dimension_types'], *args, **kwargs)

        context['get_tree_types'] = get_tree_types
        return context


STORAGES = [
    DenseStorageGen(
        name = 'image',
        input_types = ['uint32_t', 'uint16_t', 'uint16_t', 'uint8_t', 'uint8_t', 'uint8_t'],
        dimension_indexes = [0, 1, 2],
        data_indexes = [3, 4, 5]
    ),
    DenseStorageGen(
        name = 'image_double',
        input_types = ['uint32_t', 'uint16_t', 'uint16_t', 'double', 'double', 'double'],
        dimension_indexes = [0, 1, 2],
        data_indexes = [3, 4, 5]
    ),
    SparseStorageGen(
        name = 'image',
        input_types = ['uint32_t', 'uint16_t', 'uint16_t', 'uint8_t', 'uint8_t', 'uint8_t'],
        dimension_indexes = [0, 1, 2],
        data_indexes = [3, 4, 5]
    ),
    SparseStorageGen(
        name = 'mipas_altitude',
        input_types = ['mipas::EnvisatTime', 'int32_t', 'int32_t', 'double', 'float', 'float'],
        dimension_indexes = [0, 3, 1, 2],
        data_indexes = [4, 5],
        extra_includes = ['"mipas.hpp"']
    ),
]


class StoragesGen:
    def __init__(self, storages=STORAGES):
        self.storages = []
        for storage in storages:
            self.add_storage(storage)

    def add_storage(self, storage):
        self.storages.append(storage)
        if storage.can_enable_timers:
            storage = copy.copy(storage)
            storage.enable_timers = not storage.enable_timers
            self.storages.append(storage)

    def get_template_names(self):
        templates = set()
        for storage in self.storages:
            templates.add(storage.template_name)
        return list(templates)

    def get_storage_filenames(self):
        for storage in self.storages:
            yield storage.get_filename()

    def generate(self, template_dir, output_dir):
        env = jinja2.Environment(
            loader = jinja2.FileSystemLoader(template_dir),
            trim_blocks = True,
            lstrip_blocks = True,
            keep_trailing_newline = True
        )
        for storage in self.storages:
            filename = storage.get_filename()
            path = os.path.join(output_dir, filename)
            with open(filename, 'w') as f:
                storage.generate(env, f)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Generate storage files')
    parser.add_argument('--print-depends', action='store_true')
    parser.add_argument('--print-outputs', action='store_true')

    args = parser.parse_args()

    gen = StoragesGen()

    if args.print_depends:
        print(';'.join(gen.get_template_names()), end='')
    elif args.print_outputs:
        print(';'.join(gen.get_storage_filenames()), end='')
    else:
        gen.generate(
            template_dir = os.path.dirname(__file__),
            output_dir = os.getcwd()
        )
