// vim: filetype=cpp foldmethod=marker foldmarker={<,>}
#ifndef _ARRAY_DATA_STORAGE_SPARSE_{{ name }}_HPP
#define _ARRAY_DATA_STORAGE_SPARSE_{{ name }}_HPP

#include <algorithm>
#include <iterator>
#include <memory>
#include <tuple>
#include <type_traits>
#include <utility>
#include "column_store.hpp"
#include "storage/btree.hpp"
#include "utils.hpp"
{% for include in extra_includes %}
#include {{ include }}
{% endfor %}


class storage_sparse_{{ name }} {
public:
    static constexpr size_t NUM_DIMENSIONS = {{ num_dimensions }};
    using tuple_type = std::tuple<{{ data_types_list }}>;
    using dim_tuple_type = std::tuple<{{ dimension_types_list }}>;
    using data_t = column_store<{{ data_types_list }}, {{ dimension_types_list }}>;

private:
    static constexpr size_t MAX_TUPLES_IN_INTERVAL = {{ max_tuples_in_interval }};
    static constexpr size_t MAX_TUPLES_BASE = {{ max_tuples_base }};

    static constexpr size_t max_tuples_divisor(const size_t dim, const size_t acc) {
        if (dim == 0) {
            return acc;
        } else {
            return max_tuples_divisor(dim - 1, acc * MAX_TUPLES_BASE);
        }
    }

    static constexpr size_t max_tuples(const size_t dim) {
        return std::max(
            MAX_TUPLES_IN_INTERVAL / max_tuples_divisor(dim, 1),
            static_cast<size_t>(1)
        );
    }

    template <typename T, typename D>
    struct inner_tree_value {
        std::unique_ptr<T> tree;
        size_t count;
        D min;
        D max;

        inner_tree_value() : tree{nullptr}, count(0) {}
        inner_tree_value(std::unique_ptr<T>&& tree) : tree{std::move(tree)}, count(0) {}

        void update(const D value) {
            if (count == 0) {
                min = value;
                max = value;
            } else {
                if (value < min) {
                    min = value;
                } else if (value > max) {
                    max = value;
                }
            }
            ++count;
        }
    };

    {%
       set tree_types = get_tree_types(
        leaf_tree = (
            'LinkedBTree<'
                'std::pair<{dim_type}, utils::uint_type_for_t<{dim_type}>>, '
                'size_t, 4, 2'
            '>'
        ),
        inner_tree = 'LinkedBTree<{dim_type}, inner_tree_value<{child_tree}, {dim_type}>, 4, 2>'
       )
    %}
    template <size_t D>
    using tree_type_t = std::tuple_element_t<D, std::tuple<{{ tree_types|join(', ') }}>>;
    {{ tree_types[0] }} tree;
    data_t data;


    // class link_iterator_base {<
    template <bool is_const>
    class link_iterator_base {
    private:
        using column_store_ref = std::conditional_t<
            is_const, data_t::const_tuple_reference, data_t::tuple_reference
        >;

    public:
        using value_type = column_store_ref;
        using difference_type = ssize_t;
        using reference = value_type;
        using pointer = utils::value_pointer<value_type>;
        using iterator_category = std::bidirectional_iterator_tag;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;
        using tree_t = tree_type_t<NUM_DIMENSIONS - 1>;
        using tree_iterator_t = typename tree_t::const_iterator;

        data_ref_t* data;
        const tree_t* prev_tree;
        const tree_t* tree;
        tree_iterator_t tree_begin;
        tree_iterator_t tree_end;
        tree_iterator_t tree_it;

        link_iterator_base(
            data_ref_t* data, const tree_t* prev_tree, const tree_t* tree,
            const tree_iterator_t& tree_begin, const tree_iterator_t& tree_end,
            const tree_iterator_t& tree_it
        ) : data(data), prev_tree(prev_tree), tree(tree), tree_begin(tree_begin),
            tree_end(tree_end), tree_it(tree_it) {}

        link_iterator_base(data_ref_t* data, const tree_t* tree, const tree_iterator_t& tree_it)
        : data(data), prev_tree(tree->prev), tree(tree), tree_begin(tree->begin()),
          tree_end(tree->end()), tree_it(tree_it) {
            if (tree_it == tree_end) {
                advance_next_tree();
            }
        }

        link_iterator_base(data_ref_t* data, const tree_t* prev_tree, const tree_t* tree)
        : data(data), prev_tree(prev_tree), tree(tree) {
            if (tree != nullptr) {
                tree_begin = tree->begin();
                tree_end = tree->end();
                tree_it = tree_begin;
            }
        }

        void advance_next_tree() {
            prev_tree = tree;
            tree = tree->next;
            if (tree == nullptr) {
                tree_begin = tree_iterator_t{};
                tree_end = tree_iterator_t{};
                tree_it = tree_iterator_t{};
            } else {
                tree_begin = tree->begin();
                tree_end = tree->end();
                tree_it = tree_begin;
            }
        }

    public:
        link_iterator_base() = default;

        operator link_iterator_base<true>() const {
            return link_iterator_base<true>{data, prev_tree, tree, tree_begin, tree_end, tree_it};
        }

        reference operator*() const {
            return (*data)[tree_it->second];
        }

        pointer operator->() const {
            return pointer{*(*this)};
        }

        link_iterator_base& operator++() {
            ++tree_it;
            if (tree_it == tree_end) {
                advance_next_tree();
            }
            return *this;
        }

        link_iterator_base operator++(int) const {
            link_iterator_base it{*this};
            ++it;
            return it;
        }

        link_iterator_base& operator--() {
            if (tree_it == tree_begin) {
                tree = prev_tree;
                prev_tree = prev_tree->prev;
                tree_begin = tree->begin();
                tree_end = tree->end();
                tree_it = tree_end;
            }
            --tree_it;
            return *this;
        }

        link_iterator_base operator--(int) const {
            link_iterator_base it{*this};
            --it;
            return it;
        }

        bool operator==(const link_iterator_base& it) const {
            return tree_it == it.tree_it;
        }

        bool operator!=(const link_iterator_base& it) const {
            return tree_it != it.tree_it;
        }
    };


    {% for tree_type in tree_types[:-1] %}
        static link_iterator_base<false> get_link_begin(
            data_t* data, const {{ tree_type }}* tree
        ) {
            return get_link_begin(data, tree->cbegin()->second.tree.get());
        }
    {% endfor %}

    static link_iterator_base<false> get_link_begin(
        data_t* data, const {{ tree_types[-1] }}* tree
    ) {
        return link_iterator_base<false>{data, tree, tree->cbegin()};
    }

    {% for tree_type in tree_types[:-1] %}
        static link_iterator_base<false> get_link_end(
            data_t* data, const {{ tree_type }}* tree
        ) {
            auto it = tree->cend();
            --it;
            return get_link_end(data, it->second.tree.get());
        }
    {% endfor %}

    static link_iterator_base<false> get_link_end(
        data_t* data, const {{ tree_types[-1] }}* tree
    ) {
        return link_iterator_base<false>{data, tree, tree->cend()};
    }
    // >}


    // class dimension_container_base {<
    template <bool is_const, size_t D, typename = void>
    class dimension_container_base;

    template <bool is_const, size_t D>
    class dimension_container_base<is_const, D, std::enable_if_t<(D + 2 < NUM_DIMENSIONS)>> {
    public:
        using dim_t = std::tuple_element_t<D, dim_tuple_type>;
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using link_iterator = link_iterator_base<is_const>;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;
        template <typename> friend class dimension_range_iterator;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;
        using tree_iterator_t = typename tree_type_t<D>::const_iterator;

        data_ref_t* data;
        tree_iterator_t tree_it;
        utils::sub_tuple_t<dim_tuple_type, D + 1> dim_values;

        dimension_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it,
            utils::sub_tuple_t<dim_tuple_type, D + 1> dim_values
        ) : data(data), tree_it(tree_it), dim_values(dim_values) {}

        dimension_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it,
            utils::sub_tuple_t<dim_tuple_type, D> previous_values
        ) : data(data), tree_it(tree_it) {
            dim_t dim_value = tree_it->second.min;
            dim_values = std::tuple_cat(previous_values, std::make_tuple(dim_value));
        }

    public:
        dimension_container_base() = default;

        dim_t get_dim_value() const {
            return std::get<D>(dim_values);
        }

        dimension_container_base<is_const, D + 1> operator[](
            const next_dim_t next_dim_value
        ) const {
            return dimension_container_base<is_const, D + 1>{
                data,
                tree_it->second.tree->find_greater_eq(next_dim_value),
                std::tuple_cat(dim_values, std::make_tuple(next_dim_value))
            };
        }

        link_iterator link_begin() const {
            return static_cast<link_iterator>(
                storage_sparse_{{ name }}::get_link_begin(
                    const_cast<data_t*>(data),
                    tree_it->second.tree.get()
                )
            );
        }

        link_iterator link_end() const {
            return static_cast<link_iterator>(
                storage_sparse_{{ name }}::get_link_end(
                    const_cast<data_t*>(data),
                    tree_it->second.tree.get()
                )
            );
        }
    };

    template <bool is_const, size_t D>
    class dimension_container_base<is_const, D, std::enable_if_t<D + 2 == NUM_DIMENSIONS>> {
    public:
        using dim_t = std::tuple_element_t<D, dim_tuple_type>;
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using link_iterator = link_iterator_base<is_const>;

        template <size_t I>
        using value_type_t = std::conditional_t<
            is_const,
            const std::tuple_element_t<I, tuple_type>,
            std::tuple_element_t<I, tuple_type>
        >;
        using column_store_ref = std::conditional_t<
            is_const, data_t::const_tuple_reference, data_t::tuple_reference
        >;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;
        using tree_iterator_t = typename tree_type_t<D>::const_iterator;

        data_ref_t* data;
        tree_iterator_t tree_it;
        utils::sub_tuple_t<dim_tuple_type, D + 1> dim_values;

        dimension_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it,
            utils::sub_tuple_t<dim_tuple_type, D + 1> dim_values
        ) : data(data), tree_it(tree_it), dim_values(dim_values) {}

        dimension_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it,
            utils::sub_tuple_t<dim_tuple_type, D> previous_values
        ) : data(data), tree_it(tree_it) {
            dim_t dim_value = tree_it->second.min;
            dim_values = std::tuple_cat(previous_values, std::make_tuple(dim_value));
        }

    public:
        dimension_container_base() = default;

        dim_t get_dim_value() const {
            return std::get<D>(dim_values);
        }

        column_store_ref operator[](const next_dim_t dim) const {
            auto last_it = tree_it->second.tree->find_greater_eq({dim, 0});
            while (true) {
                assert(last_it->first.first == dim);
                auto ref = (*data)[last_it->second];
                if (
                    {% for _ in dimension_types[:-1] %}
                        ref.template get<{{ num_data_columns + loop.index0 }}>() ==
                        std::get<{{ loop.index0 }}>(dim_values) &&
                    {% endfor %}
                    ref.template get<{{ num_data_columns + num_dimensions - 1 }}>() == dim
                ) {
                    return ref;
                }
                ++last_it;
            }
        }

        link_iterator link_begin() const {
            return link_iterator{
                data, tree_it->second.tree.get(), tree_it->second.tree->cbegin()
            };
        }

        link_iterator link_end() const {
            auto* next_tree = tree_it->second.tree->next;
            return link_iterator{data, tree_it->second.tree.get(), next_tree};
        }
    };
    // >}


    // class dimension_range_iterator {<
    template <typename C>
    class dimension_range_iterator {
    public:
        using value_type = C;
        using difference_type = ssize_t;
        using reference = value_type;
        using pointer = utils::value_pointer<value_type>;
        using iterator_category = std::bidirectional_iterator_tag;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_container_base;

        C container;

        dimension_range_iterator(const C& container) : container(container) {}

    public:
        dimension_range_iterator() = default;

        reference operator*() const {
            return container;
        }

        pointer operator->() const {
            return pointer{*(*this)};
        }

        dimension_range_iterator& operator++() {
            ++container.tree_it;
            return *this;
        }

        dimension_range_iterator operator++(int) const {
            dimension_range_iterator it{*this};
            ++it;
            return it;
        }

        dimension_range_iterator& operator--() {
            --container.tree_it;
            return *this;
        }

        dimension_range_iterator operator--(int) const {
            dimension_range_iterator it{*this};
            --it;
            return it;
        }

        bool operator==(const dimension_range_iterator& it) const {
            return container.tree_it == it.container.tree_it;
        }

        bool operator!=(const dimension_range_iterator& it) const {
            return container.tree_it != it.container.tree_it;
        }
    };
    // >}

    // class dimension_range_container_base {<
    template <bool is_const, size_t D, typename = void>
    class dimension_range_container_base;

    template <bool is_const, size_t D>
    class dimension_range_container_base<is_const, D, std::enable_if_t<(D + 2 < NUM_DIMENSIONS)>> {
    public:
        using dim_t = std::tuple_element_t<D, dim_tuple_type>;
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using iterator = dimension_range_iterator<dimension_range_container_base<is_const, D + 1>>;
        using link_iterator = link_iterator_base<is_const>;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_range_container_base;
        template <typename> friend class dimension_range_iterator;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;
        using tree_iterator_t = typename tree_type_t<D>::const_iterator;

        data_ref_t* data;
        tree_iterator_t tree_it;

        dimension_range_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it
        ) : data(data), tree_it(tree_it) {}

    public:
        dimension_range_container_base() = default;

        dim_t min() const {
            return tree_it->second.min;
        }

        dim_t max() const {
            return tree_it->second.max;
        }

        iterator find_greater_eq(const next_dim_t dim) {
            return iterator{
                dimension_range_container_base<is_const, D + 1>{
                    data, tree_it->second.tree->find_greater_eq(dim)
                }
            };
        }

        iterator begin() const {
            return iterator{
                dimension_range_container_base<is_const, D + 1>{
                    data, tree_it->second.tree->cbegin()
                }
            };
        }

        iterator end() const {
            return iterator{
                dimension_range_container_base<is_const, D + 1>{
                    data, tree_it->second.tree->cend()
                }
            };
        }

        link_iterator link_begin() const {
            return static_cast<link_iterator>(
                storage_sparse_{{ name }}::get_link_begin(
                    const_cast<data_t*>(data),
                    tree_it->second.tree.get()
                )
            );
        }

        link_iterator link_end() const {
            return static_cast<link_iterator>(
                storage_sparse_{{ name }}::get_link_end(
                    const_cast<data_t*>(data),
                    tree_it->second.tree.get()
                )
            );
        }
    };

    template <bool is_const, size_t D>
    class dimension_range_container_base<is_const, D, std::enable_if_t<D + 2 == NUM_DIMENSIONS>> {
    public:
        using dim_t = std::tuple_element_t<D, dim_tuple_type>;
        using next_dim_t = std::tuple_element_t<D + 1, dim_tuple_type>;
        using link_iterator = link_iterator_base<is_const>;
        using iterator = link_iterator;

        template <size_t I>
        using value_type_t = std::conditional_t<
            is_const,
            const std::tuple_element_t<I, tuple_type>,
            std::tuple_element_t<I, tuple_type>
        >;
        using column_store_ref = std::conditional_t<
            is_const, data_t::const_tuple_reference, data_t::tuple_reference
        >;

    private:
        friend class storage_sparse_{{ name }};
        template <bool, size_t, typename> friend class dimension_range_container_base;
        template <typename> friend class dimension_range_iterator;

        using data_ref_t = std::conditional_t<is_const, const data_t, data_t>;
        using tree_iterator_t = typename tree_type_t<D>::const_iterator;

        data_ref_t* data;
        tree_iterator_t tree_it;

        dimension_range_container_base(
            data_ref_t* data,
            const tree_iterator_t& tree_it
        ) : data(data), tree_it(tree_it) {}

    public:
        dimension_range_container_base() = default;

        dim_t min() const {
            return tree_it->second.min;
        }

        dim_t max() const {
            return tree_it->second.max;
        }

        iterator find_greater_eq(const next_dim_t dim) {
            return iterator{
                data, tree_it->second.tree.get(), tree_it->second.tree->find_greater_eq({dim, 0})
            };
        }

        iterator begin() const {
            return link_begin();
        }

        iterator end() const {
            return link_end();
        }

        link_iterator link_begin() const {
            return link_iterator{
                data, tree_it->second.tree.get(), tree_it->second.tree->cbegin()
            };
        }

        link_iterator link_end() const {
            auto* next_tree = tree_it->second.tree->next;
            return link_iterator{data, tree_it->second.tree.get(), next_tree};
        }
    };
    // >}

public:
    template <size_t D> using dimension_container = dimension_container_base<false, D>;
    template <size_t D> using const_dimension_container = dimension_container_base<true, D>;
    template <size_t D>
    using dimension_range_container = dimension_range_container_base<false, D>;
    template <size_t D>
    using const_dimension_range_container = dimension_range_container_base<true, D>;
    using iterator = dimension_range_iterator<dimension_range_container<0>>;
    using const_iterator = dimension_range_iterator<const_dimension_range_container<0>>;
    using link_iterator = link_iterator_base<false>;
    using const_link_iterator = link_iterator_base<true>;
    using first_dim_t = std::tuple_element_t<0, dim_tuple_type>;

private:
    // insert_in_trees() {<
    template <size_t I>
    struct MedianCompare {
        template <typename T>
        bool operator()(const T& first, const T& second) const {
            return first->template get<I>() < second->template get<I>();
        }
    };

    size_t& insert_in_trees(
        {% for type in dimension_types %}
            const {{ type }} dim{{ loop.index0 }}{% if not loop.last %},{% endif %}

        {% endfor %}
    ) {
        return insert_in_trees_dim0(
            {% for _ in dimension_types %}
               dim{{ loop.index0 }},
            {% endfor %}
            tree
        );
    }

    {% for tree_type in tree_types[:-1] %}
        {% set child_tree_type = tree_types[loop.index0 + 1] %}
        size_t& insert_in_trees_dim{{ loop.index0 }}(
            {% for type in dimension_types %}
                const {{ type }} dim{{ loop.index0 }},
            {% endfor %}
            {{ tree_type }}& dim_tree
        ) {
            {{ tree_type }}::iterator tree_it;
            if (dim_tree.empty()) {
                tree_it = dim_tree.insert(
                    utils::max_inf<{{ dimension_types[loop.index0] }}>(),
                    {{ tree_type }}::value_type{},
                    false
                );
            } else {
                tree_it = dim_tree.find_greater_eq(dim{{ loop.index0 }});
            }
            {
                auto& value = tree_it->second;
                if (
                    value.count >= max_tuples({{ loop.index0 }}) &&
                    value.min == value.max &&
                    dim{{ loop.index0 }} != value.min
                ) {
                    // This only happens when a range of length one was larger
                    // than the max_tuples value and then a value that would
                    // increase the size of the range is inserted.
                    if (dim{{ loop.index0 }} > value.min) {
                        // New value comes after the existing range, so the
                        // value of the existing range is used as "median".
                        {{ tree_type }}::value_type old_value{std::move(value)};
                        auto new_tree_it = dim_tree.insert(
                            value.min, std::move(old_value), false
                        );
                        ++new_tree_it;
                        new_tree_it->second = {{ tree_type }}::value_type{};
                        tree_it = new_tree_it;
                    } else {
                        assert(dim{{ loop.index0 }} < value.min);
                        tree_it = dim_tree.insert(
                            dim{{ loop.index0 }}, {{ tree_type }}::value_type{}, false
                        );
                    }
                }
            }
            auto& value = tree_it->second;
#ifndef NDEBUG
            auto upper_bound = tree_it->first;
#endif
            if (value.count >= max_tuples({{ loop.index0 }}) && value.max > value.min) {
#ifdef STORAGE_PERF_STATS
                std::cerr << "split occurred at dimension {{ loop.index0 }} with ";
                std::cerr << value.count << " tuples" << std::endl;
#endif
                assert(value.count == max_tuples({{ loop.index0 }}));
                auto link_begin = get_link_begin(&data, tree_it->second.tree.get());
                auto link_end = get_link_end(&data, tree_it->second.tree.get());
                {{ dimension_types[loop.index0] }} median = utils::median(
                    link_begin,
                    link_end,
                    MedianCompare<{{ num_data_columns + loop.index0 }}>{}
                )->get<{{ num_data_columns + loop.index0 }}>();
                if (median == value.max) {
                    median = value.min;
                }
                assert(median != upper_bound);
#ifndef NDEBUG
                size_t old_count = value.count;
#endif
                value.count = 0;
                {{ child_tree_type }} old_tree{std::move(*value.tree)};
                *value.tree = {{ child_tree_type }}{};
                std::unique_ptr<{{ child_tree_type }}> new_tree = \
                    std::make_unique<{{ child_tree_type }}>();
                new_tree->parent = reinterpret_cast<char*>(&dim_tree);
                new_tree->prev = old_tree.prev;
                if (old_tree.prev != nullptr) {
                    old_tree.prev->next = new_tree.get();
                }
                new_tree->next = value.tree.get();
                value.tree->parent = new_tree->parent;
                value.tree->prev = new_tree.get();
                value.tree->next = old_tree.next;
#ifndef NDEBUG
                auto* right_tree_ptr = value.tree.get();
#endif
                auto new_tree_it = dim_tree.insert(
                    median,
                    {{ tree_type }}::value_type{std::move(new_tree)}
                );
                auto& left_value = new_tree_it->second;
                ++new_tree_it;
                auto& right_value = new_tree_it->second;
#ifndef NDEBUG
                assert(right_value.tree.get() == right_tree_ptr);
                size_t distance = std::distance(link_begin, link_end);
                assert(distance == old_count);
#endif
                for (auto it = link_begin; it != link_end; ++it) {
                    size_t* index;
                    if (it->get<{{ num_data_columns + loop.index0 }}>() <= median) {
                        left_value.update(it->get<{{ num_data_columns + loop.index0 }}>());
                        index = &insert_in_trees_dim{{ loop.index0 + 1 }}(
                            {% for _ in dimension_types %}
                                it->get<{{ num_data_columns + loop.index0 }}>(),
                            {% endfor %}
                            *left_value.tree
                        );
                    } else {
                        right_value.update(it->get<{{ num_data_columns + loop.index0 }}>());
                        index = &insert_in_trees_dim{{ loop.index0 + 1 }}(
                            {% for _ in dimension_types %}
                                it->get<{{ num_data_columns + loop.index0 }}>(),
                            {% endfor %}
                            *right_value.tree
                        );
                    }
                    *index = it->get_index();
                }
                assert(!(left_value.tree->empty() || right_value.tree->empty()));
                if (dim{{ loop.index0 }} <= median) {
                    left_value.update(dim{{ loop.index0 }});
                    return insert_in_trees_dim{{ loop.index0 + 1 }}(
                        {% for _ in dimension_types %}
                           dim{{ loop.index0 }},
                        {% endfor %}
                        *left_value.tree
                    );
                } else {
                    right_value.update(dim{{ loop.index0 }});
                    return insert_in_trees_dim{{ loop.index0 + 1 }}(
                        {% for _ in dimension_types %}
                           dim{{ loop.index0 }},
                        {% endfor %}
                        *right_value.tree
                    );
                }
            }
            value.update(dim{{ loop.index0 }});
            if (value.tree == nullptr) {
                std::unique_ptr<{{ child_tree_type }}> new_tree = \
                    std::make_unique<{{ child_tree_type }}>();
                new_tree->parent = reinterpret_cast<char*>(&dim_tree);
                {{ child_tree_type }}* prev_tree = nullptr;
                {{ child_tree_type }}* next_tree = nullptr;
                if (tree_it != dim_tree.begin()) {
                    auto prev_it = tree_it--;
                    prev_tree = prev_it->second.tree.get();
                } else if (dim_tree.prev != nullptr && !dim_tree.prev->empty()) {
                    prev_tree = (--dim_tree.prev->end())->second.tree.get();
                }
                auto next_it = tree_it++;
                if (next_it != dim_tree.end()) {
                    next_tree = next_it->second.tree.get();
                } else if (dim_tree.next != nullptr && !dim_tree.next->empty()) {
                    next_tree = dim_tree.next->begin()->second.tree.get();
                }
                new_tree->prev = prev_tree;
                new_tree->next = next_tree;
                if (prev_tree != nullptr) {
                    prev_tree->next = new_tree.get();
                }
                if (next_tree != nullptr) {
                    next_tree->prev = new_tree.get();
                }
                tree_it->second.tree = std::move(new_tree);
            }
            return insert_in_trees_dim{{ loop.index0 + 1 }}(
                {% for _ in dimension_types %}
                   dim{{ loop.index0 }},
                {% endfor %}
                *tree_it->second.tree
            );
        }
    {% endfor %}

    size_t& insert_in_trees_dim{{ num_dimensions - 1 }}(
        {% for type in dimension_types %}
            const {{ type }}{% if loop.last %} dim{{ loop.index0 }}{% endif %},
        {% endfor %}
        {{ tree_types[-1] }}& dim_tree
    ) {
        utils::uint_type_for_t<{{ dimension_types[-1] }}> num_duplicates = 0;
        auto existing_value_it = dim_tree.find_greater_eq(
            std::make_pair(dim{{ num_dimensions - 1 }}, 0)
        );
        auto tree_end = dim_tree.end();
        while (existing_value_it != tree_end) {
            auto& key = existing_value_it->first;
            if (key.first != dim{{ num_dimensions - 1 }}) {
                break;
            }
            num_duplicates = key.second + 1;
            ++existing_value_it;
        }
        return dim_tree.insert(
            std::make_pair(dim{{ num_dimensions - 1 }}, num_duplicates),
            0
        )->second;
    }
    // >}

public:
    storage_sparse_{{ name }}() = default;

    storage_sparse_{{ name }}(const column_store<{{ input_types_list }}>& input) {
        data = data_t{input.size()};
        {% for type, index in data_type_indexes %}
            const {{ type }}* input_col{{ loop.index0 }} = input.get<{{ index }}>();
        {% endfor %}
        {% for type, index in dimension_type_indexes %}
            const {{ type }}* input_dim_col{{ loop.index0 }} = input.get<{{ index }}>();
        {% endfor %}
        {% for type in data_types %}
            {{ type }}* data_col{{ loop.index0 }} = data.get<{{ loop.index0 }}>();
        {% endfor %}
        {% for type in dimension_types %}
            {{ type }}* dim_col{{ loop.index0 }} = data.get<
                {{- num_data_columns + loop.index0 -}}
            >();
        {% endfor %}
        for (size_t i = 0; i < input.size(); ++i) {
            {% for _ in data_indexes %}
                data_col{{ loop.index0 }}[i] = input_col{{ loop.index0 }}[i];
            {% endfor %}
            {% for _ in dimension_indexes %}
                dim_col{{ loop.index0 }}[i] = input_dim_col{{ loop.index0 }}[i];
            {% endfor %}
            size_t& index = insert_in_trees(
                {% for index in dimension_indexes %}
                    input.get<{{ index }}>(i){% if not loop.last %},{% endif %}

                {% endfor %}
            );
            index = i;
        }
    }

    void cleanup() {
        if (tree.empty()) {
            return;
        }
        data_t new_data{data.size()};
        tree_type_t<NUM_DIMENSIONS - 1>* current_tree = (
            tree.begin()
            {% for _ in tree_types[:-2] %}
                ->second.tree->begin()
            {% endfor %}
            ->second.tree.get()
        );
        size_t index = 0;
        while (current_tree != nullptr) {
            for (auto key_value : *current_tree) {
                new_data[index] = data[key_value.second];
                key_value.second = index;
                ++index;
            }
            current_tree = current_tree->next;
        }
        assert(index == data.size());
        data = std::move(new_data);
    }

    const data_t& get_data() const {
        return data;
    }

    dimension_container<0> operator[](const first_dim_t dim) {
        return dimension_container<0>{&data, tree.find_greater_eq(dim), std::make_tuple(dim)};
    }

    const_dimension_container<0> operator[](const first_dim_t dim) const {
        return const_dimension_container<0>{&data, tree.find_greater_eq(dim), std::make_tuple(dim)};
    }

    iterator find_greater_eq(const first_dim_t dim) {
        return iterator{dimension_range_container<0>{&data, tree.find_greater_eq(dim)}};
    }

    const_iterator find_greater_eq(const first_dim_t dim) const {
        return const_iterator{
            const_dimension_range_container<0>{&data, tree.find_greater_eq(dim)}
        };
    }

    iterator begin() {
        return iterator{dimension_range_container<0>{&data, tree.cbegin()}};
    }

    iterator end() {
        return iterator{dimension_range_container<0>{&data, tree.cend()}};
    }

    const_iterator cbegin() const {
        return const_iterator{const_dimension_range_container<0>{&data, tree.cbegin()}};
    }

    const_iterator begin() const {
        return cbegin();
    }

    const_iterator cend() const {
        return const_iterator{const_dimension_range_container<0>{&data, tree.cend()}};
    }

    const_iterator end() const {
        return cend();
    }

    link_iterator link_begin() {
        if (tree.empty()) {
            return link_iterator{&data, nullptr, nullptr};
        }
        return get_link_begin(&data, &tree);
    }

    link_iterator link_end() {
        if (tree.empty()) {
            return link_iterator{&data, nullptr, nullptr};
        }
        return get_link_end(&data, &tree);
    }

    const_link_iterator link_begin() const {
        return static_cast<const_link_iterator>(
            const_cast<storage_sparse_{{ name }}*>(this)->link_begin()
        );
    }

    const_link_iterator link_end() const {
        return static_cast<const_link_iterator>(
            const_cast<storage_sparse_{{ name }}*>(this)->link_end()
        );
    }
};

#endif
