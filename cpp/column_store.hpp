#ifndef _COLUMN_STORE_HPP
#define _COLUMN_STORE_HPP

#include <array>
#include <cstring>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
#include "allocator.hpp"
#include "mmap.hpp"
#include "utils.hpp"


class column_store_error
: public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};


template <typename Vectors, typename... Ts>
class generic_column_store {
public:
    using tuple_type = std::tuple<Ts...>;
    static constexpr size_t NUM_COLUMNS = sizeof...(Ts);

private:
    template <bool is_const>
    class reference_base {
    private:
        friend class generic_column_store;

        using store_t = std::conditional_t<
            is_const, const generic_column_store, generic_column_store
        >;
        store_t& store;
        const size_t index;

        reference_base(store_t& store, const size_t index) : store(store), index(index) {}

        template <size_t I, size_t... Is>
        void assign_values(
            const reference_base<true>& ref, std::index_sequence<I, Is...>
        ) const {
            get<I>() = ref.template get<I>();
            assign_values(ref, std::index_sequence<Is...>{});
        }

        void assign_values(const reference_base<true>&, std::index_sequence<>) const {}

    public:
        template <size_t I>
        using value_type = std::conditional_t<
            is_const,
            const std::tuple_element_t<I, tuple_type>,
            std::tuple_element_t<I, tuple_type>
        >;
        using pointer = utils::value_pointer<reference_base>;

        operator reference_base<true>() const {
            return reference_base<true>{store, index};
        }

        template <size_t I>
        value_type<I>& get() const {
            return std::get<I>(store.vectors)[index];
        }

        size_t get_index() const {
            return index;
        }

        pointer operator&() const {
            return pointer{*this};
        }

        const reference_base& operator=(const reference_base<true> ref) const {
            assign_values(ref, std::make_index_sequence<NUM_COLUMNS>{});
            return *this;
        }

        const reference_base& operator=(const reference_base<false> ref) const {
            return *this = static_cast<const reference_base<true>>(ref);
        }
    };

public:
    using tuple_reference = reference_base<false>;
    using const_tuple_reference = reference_base<true>;

private:
    Vectors vectors;

    template <size_t I, size_t... Is>
    void vectors_default_construct(const size_t size, std::index_sequence<I, Is...>) {
        std::get<I>(vectors) = typename std::tuple_element<I, Vectors>::type(size);
        vectors_default_construct(size, std::index_sequence<Is...>{});
    }

    void vectors_default_construct(const size_t, std::index_sequence<>) {}

    template <size_t I, size_t... Is>
    void vectors_reserve(const size_t reserve_size, std::index_sequence<I, Is...>) {
        std::get<I>(vectors).reserve(reserve_size);
        vectors_reserve(reserve_size, std::index_sequence<Is...>{});
    }

    void vectors_reserve(const size_t, std::index_sequence<>) {}

    template <size_t I, size_t... Is>
    void vectors_push_back(const tuple_type& values, std::index_sequence<I, Is...>) {
        std::get<I>(vectors).push_back(std::get<I>(values));
        vectors_push_back(values, std::index_sequence<Is...>{});
    }

    void vectors_push_back(const tuple_type&, std::index_sequence<>) {}

    struct mmap_header {
        char magic[8];
        size_t size;
        size_t num_columns;
        size_t num_tuples;
        size_t column_offsets[NUM_COLUMNS];
    };

    template <size_t I, size_t... Is>
    void vectors_write_to_file(
        Mmap& file,
        std::array<size_t, NUM_COLUMNS>& offsets,
        const size_t offset,
        std::index_sequence<I, Is...>
    ) const {
        size_t aligned_offset = \
            utils::align<typename std::tuple_element<I, tuple_type>::type>(offset);
        offsets[I] = aligned_offset;
        size_t vector_size = (
            std::get<I>(vectors).size() *
            sizeof(typename std::tuple_element<I, tuple_type>::type)
        );
        if (aligned_offset + vector_size > file.size()) {
            file.resize(aligned_offset + vector_size, true);
        }
        std::memcpy(file.data() + aligned_offset, std::get<I>(vectors).data(), vector_size);
        vectors_write_to_file(
            file, offsets, aligned_offset + vector_size, std::index_sequence<Is...>{}
        );
    }

    void vectors_write_to_file(
        Mmap&, std::array<size_t, NUM_COLUMNS>&, const size_t, std::index_sequence<>
    ) const {}

    template <size_t I, size_t... Is>
    void vectors_load_from_file(
        const Mmap& file,
        const std::array<size_t, NUM_COLUMNS>& offsets,
        const size_t num_tuples,
        std::index_sequence<I, Is...>
    ) {
        auto& vec = std::get<I>(vectors);
        size_t old_size = vec.size();
        vec.resize(old_size + num_tuples);
        std::memcpy(
            vec.data() + old_size,
            file.data() + offsets[I],
            num_tuples * sizeof(typename std::tuple_element<I, tuple_type>::type)
        );
        vectors_load_from_file(file, offsets, num_tuples, std::index_sequence<Is...>{});
    }

    void vectors_load_from_file(
        const Mmap&, const std::array<size_t, NUM_COLUMNS>&, const size_t, std::index_sequence<>
    ) {}

public:
    generic_column_store() = default;

    generic_column_store(const size_t size) {
        vectors_default_construct(size, std::make_index_sequence<NUM_COLUMNS>{});
    }

    size_t size() const {
        return std::get<0>(vectors).size();
    }

    void reserve(const size_t reserve_size) {
        vectors_reserve(reserve_size, std::make_index_sequence<NUM_COLUMNS>{});
    }

    void push_back(const tuple_type& values) {
        vectors_push_back(values, std::make_index_sequence<NUM_COLUMNS>{});
    }

    void push_back(const Ts... values) {
        push_back(tuple_type{values...});
    }

    tuple_reference operator[](const size_t index) {
        return tuple_reference{*this, index};
    }

    const_tuple_reference operator[](const size_t index) const {
        return const_tuple_reference{*this, index};
    }

    template <size_t I>
    typename std::tuple_element<I, tuple_type>::type& get(const size_t index) {
        return std::get<I>(vectors)[index];
    }

    template <size_t I>
    typename std::tuple_element<I, tuple_type>::type* get() {
        return std::get<I>(vectors).data();
    }

    template <size_t I>
    const typename std::tuple_element<I, tuple_type>::type& get(const size_t index) const {
        return std::get<I>(vectors)[index];
    }

    template <size_t I>
    const typename std::tuple_element<I, tuple_type>::type* get() const {
        return std::get<I>(vectors).data();
    }

    void write_to_file(const std::string& path) const {
        Mmap file;
        file.map_file(path, sizeof(mmap_header), true);
        mmap_header* header = reinterpret_cast<mmap_header*>(file.data());
        std::memcpy(header->magic, "COLSTORE", 8);
        header->num_columns = NUM_COLUMNS;
        header->num_tuples = size();
        std::array<size_t, NUM_COLUMNS> offsets;
        vectors_write_to_file(
            file, offsets, sizeof(mmap_header), std::make_index_sequence<NUM_COLUMNS>{}
        );
        header = reinterpret_cast<mmap_header*>(file.data());
        std::memcpy(header->column_offsets, offsets.data(), sizeof(header->column_offsets));
        header->size = file.size();
    };

    void load_from_file(const std::string& path) {
        Mmap file;
        file.map_file(path, sizeof(mmap_header));
        mmap_header* header = reinterpret_cast<mmap_header*>(file.data());
        if (std::memcmp(header->magic, "COLSTORE", 8) != 0) {
            throw column_store_error{"invalid magic value"};
        }
        if (header->num_columns != NUM_COLUMNS) {
            throw column_store_error{"incompatible number of columns"};
        }
        size_t num_tuples = header->num_tuples;
        std::array<size_t, NUM_COLUMNS> offsets;
        std::memcpy(offsets.data(), header->column_offsets, sizeof(offsets));
        file.resize(header->size);
        vectors_load_from_file(file, offsets, num_tuples, std::make_index_sequence<NUM_COLUMNS>{});
    }
};

template <typename... Ts>
using column_store = generic_column_store<std::tuple<std::vector<Ts>...>, Ts...>;


template <size_t Align, typename... Ts>
using aligned_column_store = generic_column_store<
    std::tuple<std::vector<Ts, utils::aligned_allocator<Ts, Align>>...>,
    Ts...
>;

#endif
