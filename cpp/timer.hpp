#ifndef _ARRAY_DATA_TIMER_HPP
#define _ARRAY_DATA_TIMER_HPP

#include <chrono>
#include <iostream>
#include <string>
#include <utility>


namespace utils {

class timer {
private:
    typename std::chrono::high_resolution_clock::time_point start;

private:
    void print_time() {
        auto end = std::chrono::high_resolution_clock::now();
        auto diff = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        std::cerr << diff << "μs" << std::endl;
    }

public:
    timer()
    : start(std::chrono::high_resolution_clock::now()) {}

    template <typename T>
    timer(T&& message)
    : start(std::chrono::high_resolution_clock::now()) {
        std::cerr << std::forward<T>(message) << ": " << std::flush;
    }

    ~timer() {
        print_time();
    }

    void replace() {
        print_time();
        start = std::chrono::high_resolution_clock::now();
    }

    template <typename T>
    void replace(T&& message) {
        replace();
        std::cerr << std::forward<T>(message) << ": " << std::flush;
    }
};

}

#endif
