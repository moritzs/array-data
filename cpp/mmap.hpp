#ifndef _MMAP_HPP
#define _MMAP_HPP

#include <memory>
#include <string>


class Mmap {
private:
    size_t map_size;
    char* map_data;
    int fd;
    bool is_locked;

public:
    Mmap() : map_size(0), map_data(nullptr), fd(-1), is_locked(false) {}
    ~Mmap() {
        unlock();
        close();
    }

    inline char* data() const {
        return map_data;
    }

    inline size_t size() const {
        return map_size;
    }

    void map_file(const std::string& file, const size_t size, const bool truncate=false);
    void resize(const size_t size, const bool truncate=false);
    void close();
    void lock();
    void unlock();
    std::unique_ptr<char[]> get_copy();
};


#endif
