#ifndef _MIPAS_UTILS_HPP
#define _MIPAS_UTILS_HPP

#include <algorithm>
#include <array>
#include <byteswap.h>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <memory>
#include <ostream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>


namespace utils {

template <typename T>
constexpr T max_inf() {
    using limits = std::numeric_limits<T>;
    if (limits::has_infinity) {
        return limits::infinity();
    } else {
        return limits::max();
    }
}


template <size_t size> struct uint_type;
template <> struct uint_type<1> { using type = uint8_t; };
template <> struct uint_type<2> { using type = uint16_t; };
template <> struct uint_type<4> { using type = uint32_t; };
template <> struct uint_type<8> { using type = uint64_t; };

template <size_t size> using uint_type_t = typename uint_type<size>::type;


template <typename T> struct uint_type_for { using type = uint_type_t<sizeof(T)>; };

template <typename T> using uint_type_for_t = typename uint_type_for<T>::type;


template <typename T>
constexpr size_t align(const size_t offset) {
    return (offset + alignof(T) - 1) & (~alignof(T) + 1);
}


template <typename T> inline void byteswap(T& value);
template <> inline void byteswap<int8_t>(int8_t&) {}
template <> inline void byteswap<int16_t>(int16_t& value) { value = bswap_16(value); }
template <> inline void byteswap<int32_t>(int32_t& value) { value = bswap_32(value); }
template <> inline void byteswap<int64_t>(int64_t& value) { value = bswap_64(value); }
template <> inline void byteswap<uint8_t>(uint8_t&) {}
template <> inline void byteswap<uint16_t>(uint16_t& value) { value = bswap_16(value); }
template <> inline void byteswap<uint32_t>(uint32_t& value) { value = bswap_32(value); }
template <> inline void byteswap<uint64_t>(uint64_t& value) { value = bswap_64(value); }


template <typename T>
std::string to_hex(const T value) {
    static const char chars[] = "0123456789ABCDEF";
    const char* bytes = reinterpret_cast<const char*>(&value);
    std::string s(2 * sizeof(T), '\0');
    for (size_t i = 0; i < sizeof(T); ++i) {
        s[2 * i] = chars[(bytes[i] >> 4) & 0x0f];
        s[2 * i + 1] = chars[bytes[i] & 0x0f];
    }
    return s;
}


template <size_t factor, typename T>
class format_as_fixed_float {
public:
    const T value;

    constexpr format_as_fixed_float(const T value) : value(value) {}
};

template <size_t factor, typename T>
std::ostream& operator<<(std::ostream& stream, format_as_fixed_float<factor, T> f) {
    stream << (f.value / static_cast<T>(factor)) << '.' << (std::abs(f.value) % factor);
    return stream;
}

template <typename T>
constexpr format_as_fixed_float<1000000, T> fixed_float(const T value) {
    return format_as_fixed_float<1000000, T>{value};
}


template <size_t N>
class fixed_string {
private:
    std::array<char, N> chars;

public:
    fixed_string() = default;

    template <size_t M>
    fixed_string(const fixed_string<M>& s) {
        if (N > M) {
            chars.fill(0);
        }
        std::memcpy(chars.data(), s.data(), std::min(N, M));
    }

    fixed_string(const std::string& s) {
        size_t num_chars = s.length();
        if (num_chars < N) {
            chars.fill(0);
        } else {
            num_chars = N;
        }
        std::memcpy(chars.data(), s.data(), num_chars);
    }

    fixed_string(const char* s) {
        std::memcpy(chars.data(), s, N);
    }

    operator std::string() const {
        return std::string{chars.data(), N};
    }

    static constexpr size_t length() {
        return N;
    }

    char* data() {
        return chars.data();
    }
};

template <size_t N>
std::ostream& operator<<(std::ostream& stream, const fixed_string<N>& s) {
    stream << static_cast<std::string>(s);
    return stream;
}


template <typename R>
class value_pointer {
private:
    R reference;

public:
    value_pointer(const R& reference) : reference(reference) {}

    R operator*() {
        return reference;
    }

    R* operator->() {
        return std::addressof(reference);
    }
};


template <typename BiDirIt, typename Compare>
BiDirIt median(BiDirIt begin, BiDirIt end, Compare compare) {
    BiDirIt run[5];
    std::vector<BiDirIt> values;
    auto it = begin;
    while (it != end) {
        size_t run_size = 0;
        while (run_size < 5 && it != end) {
            run[run_size] = it;
            ++it;
            ++run_size;
        }
        std::sort(&run[0], &run[run_size], compare);
        values.push_back(std::move(run[(run_size - 1) / 2]));
    }
    size_t size = values.size();
    while (size > 1) {
        auto it = values.begin();
        auto end = it + size;
        auto insert_it = values.begin();
        while (it != end) {
            size_t run_size = 0;
            while (run_size < 5 && it != end) {
                run[run_size] = std::move(*it);
                ++it;
                ++run_size;
            }
            std::sort(&run[0], &run[run_size], compare);
            *insert_it = std::move(run[(run_size - 1) / 2]);
            ++insert_it;
        }
        size = insert_it - values.begin();
    }
    return values[0];
}


template <typename BiDirIt, typename Compare = void>
BiDirIt median(BiDirIt begin, BiDirIt end) {
    struct median_compare {
        bool operator()(const BiDirIt& first, const BiDirIt& second) const {
            return *first < *second;
        }
    };
    return median<BiDirIt, median_compare>(begin, end, median_compare{});
}


template <typename T, size_t I>
struct sub_tuple {
private:
    template <typename T2, size_t... Is>
    static std::tuple<std::tuple_element_t<Is, T2>...> helper(T2, std::index_sequence<Is...>);

public:
    using type = std::decay_t<decltype(helper(std::declval<T>(), std::make_index_sequence<I>{}))>;
};

template <typename T, size_t I>
using sub_tuple_t = typename sub_tuple<T, I>::type;

}

#endif
