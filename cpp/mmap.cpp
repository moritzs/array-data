#include "mmap.hpp"
#include <cerrno>
#include <cstring>
#include <fcntl.h>
#include <sys/mman.h>
#include <system_error>
#include <unistd.h>


void Mmap::map_file(const std::string& file, const size_t size, const bool truncate) {
    close();
    int flags = O_RDWR;
    if (truncate) {
        flags |= O_CREAT | O_TRUNC;
    }
    int new_fd = open(file.c_str(), flags, 0666);
    if (new_fd < 0) {
        throw std::system_error{errno, std::system_category()};
    }
    if (truncate) {
        if (ftruncate(new_fd, size) < 0) {
            throw std::system_error{errno, std::system_category()};
        }
    }
    void* new_data = mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, new_fd, 0);
    if (new_data == MAP_FAILED) {
        ::close(new_fd);
        throw std::system_error{errno, std::system_category()};
    }
    map_size = size;
    map_data = reinterpret_cast<char*>(new_data);
    fd = new_fd;
}


void Mmap::resize(const size_t size, const bool truncate) {
    if (fd < 0) {
        return;
    }
    void* new_map = mremap(map_data, map_size, size, MREMAP_MAYMOVE);
    if (new_map == MAP_FAILED) {
        throw std::system_error{errno, std::system_category()};
    }
    if (truncate) {
        if (ftruncate(fd, size) < 0) {
            throw std::system_error{errno, std::system_category()};
        }
    }
    map_data = reinterpret_cast<char*>(new_map);
    map_size = size;
}


void Mmap::close() {
    if (fd >= 0) {
        int munmap_ret = munmap(map_data, map_size);
        int close_ret = ::close(fd);
        if (munmap_ret < 0 || close_ret < 0) {
            throw std::system_error{errno, std::system_category()};
        }
        map_size = 0;
        map_data = nullptr;
        fd = -1;
        is_locked = false;
    }
}


void Mmap::lock() {
    if (fd < 0 || is_locked) {
        return;
    }
    if (mlock(map_data, map_size) < 0) {
        throw std::system_error{errno, std::system_category()};
    }
    is_locked = true;
}


void Mmap::unlock() {
    if (fd < 0 || !is_locked) {
        return;
    }
    if (munlock(map_data, map_size) < 0) {
        throw std::system_error{errno, std::system_category()};
    }
}


std::unique_ptr<char[]> Mmap::get_copy() {
    std::unique_ptr<char[]> data{new char[map_size]};
    std::memcpy(data.get(), map_data, map_size);
    return data;
}
