#ifndef _IMAGEDB_HPP
#define _IMAGEDB_HPP

#include "column_store.hpp"


namespace imagedb {

using image_store = column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t>;
using image = column_store<uint8_t, uint8_t, uint8_t>;

}

#endif
