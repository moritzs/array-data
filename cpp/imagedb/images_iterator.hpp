#ifndef _IMAGEDB_IMAGES_ITERATOR_HPP
#define _IMAGEDB_IMAGES_ITERATOR_HPP

#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/jpeg_io.hpp>
#include <boost/gil/extension/io/tiff_io.hpp>


namespace imagedb {

template <typename F>
void iterate_tiff_pixels(const boost::filesystem::path& dir, const F& f) {
    namespace fs = boost::filesystem;
    namespace gil = boost::gil;
    gil::rgb8_image_t img;
    gil::tiff_read_image(dir.string(), img);
    const auto view = gil::const_view(img);
    for (uint16_t y = 0; y < img.height(); ++y) {
        const auto row = view.row_begin(y);
        for (uint16_t x = 0; x < img.width(); ++x) {
            const auto pixel = row[x];
            const auto r = gil::get_color(pixel, gil::red_t{});
            const auto g = gil::get_color(pixel, gil::green_t{});
            const auto b = gil::get_color(pixel, gil::blue_t{});
            f(x, y, r, g, b);
        }
    }
}


template <typename F>
void iterate_directory_pixels(const boost::filesystem::path& dir, const F& f) {
    namespace fs = boost::filesystem;
    namespace gil = boost::gil;
    uint32_t image_id = 1;
    for (const auto& entry : fs::recursive_directory_iterator{dir}) {
        if (!fs::is_regular_file(entry.status())) {
            continue;
        }
        const std::string path_str = entry.path().string();
        if (path_str.substr(path_str.size() - 4) != ".jpg") {
            continue;
        }
        std::cerr << path_str << std::endl;
        gil::rgb8_image_t img;
        gil::jpeg_read_image(path_str, img);
        const auto view = gil::const_view(img);
        for (uint16_t y = 0; y < img.height(); ++y) {
            const auto row = view.row_begin(y);
            for (uint16_t x = 0; x < img.width(); ++x) {
                const auto pixel = row[x];
                const auto r = gil::get_color(pixel, gil::red_t{});
                const auto g = gil::get_color(pixel, gil::green_t{});
                const auto b = gil::get_color(pixel, gil::blue_t{});
                if (!f(image_id, x, y, r, g, b)) {
                    return;
                }
            }
        }
        ++image_id;
    }
}

}

#endif
