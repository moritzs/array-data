#include <atomic>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <utility>
#include <vector>
#include <x86intrin.h>
#include "allocator.hpp"
#include "column_store.hpp"
#include "imagedb.hpp"
#include "image_writer.hpp"
#include "simd.hpp"
#include "storage/storage_dense_image.hpp"
#include "storage/storage_dense_image_double.hpp"
#include "threading.hpp"
#include "timer.hpp"


template <typename T, size_t Align>
using aligned_vector = std::vector<T, utils::aligned_allocator<T, Align>>;


aligned_vector<double, 32> get_gauss_kernel(
    const double stddev, const size_t size
) {
    const size_t middle = size / 2;
    const double sigma_square = 2 * stddev * stddev;
    aligned_vector<double, 32> kernel(size, 0);
    for (size_t i = 0; i < size; ++i) {
        int x = static_cast<int>(i) - static_cast<int>(middle);
        kernel[i] = std::exp(-(x * x) / sigma_square);
    }
    return kernel;
}


class gauss_filter {
private:
    const imagedb::image_store& data;
    const size_t width;
    const size_t height;
    const uint32_t image_id;
    const size_t stddev;
    const size_t kernel_size;
    const size_t kernel_size_half;

    struct runtime_t {
        column_store<double, double, double> source;
        column_store<double, double, double> gauss_filtered_x;
        imagedb::image gauss_filtered;
        aligned_vector<double, 32> kernel;
        std::atomic<size_t> selection_index;
    };

    std::unique_ptr<runtime_t> runtime;

public:
    gauss_filter(
        const imagedb::image_store& data, const size_t width, const size_t height,
        const uint32_t image_id, const size_t stddev
    )
    : data(data), width(width), height(height), image_id(image_id), stddev(stddev),
      kernel_size(stddev * 6 + 1), kernel_size_half(stddev * 3) {}

    imagedb::image run(const size_t num_threads) {
        runtime = std::make_unique<runtime_t>();
        runtime->source = column_store<double, double, double>(width * height);
        runtime->gauss_filtered_x = column_store<double, double, double>(width * height);
        runtime->gauss_filtered = imagedb::image(width * height);
        runtime->kernel = get_gauss_kernel(stddev, kernel_size);
        std::atomic_init(&runtime->selection_index, 0ul);

        threading::barrier<threading::spin_lock> barrier(num_threads);
        std::vector<std::thread> threads;
        threads.reserve(num_threads);
        for (size_t i = 0; i < num_threads; ++i) {
            threads.emplace_back([this, i, num_threads, &barrier] {
                selection(i, num_threads);
                barrier.wait();
                first_pass(i, num_threads);
                barrier.wait();
                second_pass(i, num_threads);
            });
        }
        for (auto& t : threads) {
            t.join();
        }
        imagedb::image result = std::move(runtime->gauss_filtered);
        runtime.reset(nullptr);
        return result;
    }

private:
    void selection(const size_t, const size_t) {
        while (true) {
            size_t base = runtime->selection_index.fetch_add(1000);
            for (size_t offset = 0; offset < 1000; ++offset) {
                size_t i = base + offset;
                if (i >= data.size()) {
                    return;
                }
                if (data.get<0>(i) != image_id) {
                    continue;
                }
                size_t index = data.get<1>(i) + data.get<2>(i) * width;
                runtime->source.get<0>(index) = data.get<3>(i);
                runtime->source.get<1>(index) = data.get<4>(i);
                runtime->source.get<2>(index) = data.get<5>(i);
            }
        }
    }

    void first_pass(const size_t thread_id, const size_t num_threads) {
        const auto& kernel = runtime->kernel;
        double kernel_sum_inverse = 0;
        for (size_t i = 0; i < kernel_size_half; ++i) {
            kernel_sum_inverse += 2 * kernel[i];
        }
        kernel_sum_inverse += kernel[kernel_size_half];
        kernel_sum_inverse = 1 / kernel_sum_inverse;

        auto* source_r = runtime->source.get<0>();
        auto* source_g = runtime->source.get<1>();
        auto* source_b = runtime->source.get<2>();
        auto* dest_r = runtime->gauss_filtered_x.get<0>();
        auto* dest_g = runtime->gauss_filtered_x.get<1>();
        auto* dest_b = runtime->gauss_filtered_x.get<2>();
        for (size_t y = 0; y < height; ++y) {
            size_t x = thread_id;
            for (; x < kernel_size_half; x += num_threads) {
                double r_sum = 0;
                double g_sum = 0;
                double b_sum = 0;
                double sum_weights = 0;
                for (size_t i = kernel_size_half - x; i < kernel_size; ++i) {
                    size_t index = x + i + y * width - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                    sum_weights += kernel[i];
                }
                size_t index = x * height + y;
                dest_r[index] = r_sum / sum_weights;
                dest_g[index] = g_sum / sum_weights;
                dest_b[index] = b_sum / sum_weights;
            }
            for (; x < width - kernel_size_half; x += num_threads) {
                __m256d r_sum_pack = _mm256_setzero_pd();
                __m256d g_sum_pack = _mm256_setzero_pd();
                __m256d b_sum_pack = _mm256_setzero_pd();
                size_t i;
                for (i = 0; i + 3 < kernel_size; i += 4) {
                    size_t index = x + i + y * width - kernel_size_half;
                    __m256d kernel_values = _mm256_load_pd(&kernel[i]);
                    // The pointers "source_r/g/b + index" are NOT 32 byte aligned.
                    // Using vmovapd with a memory location that is not 32 byte aligned
                    // generates an exception. However, we can still use _mm256_load_pd()
                    // (as opposed to _mm256_loadu_pd() for unaligned access) here because the
                    // compiler doesn't actually emit a vmovapd instruction for those loads.
                    // Instead, it sees that the values are used in a multiplication
                    // afterwards and then emits a single vmulpd instruction with the memory
                    // location as a source operand instead of a vmovapd followed by a vmulpd.
                    // Because this only works when the compiler does this optimization, we
                    // only do it when optimizations are enabled.
#ifdef __OPTIMIZE__
                    __m256d r_values = _mm256_load_pd(source_r + index);
                    __m256d g_values = _mm256_load_pd(source_g + index);
                    __m256d b_values = _mm256_load_pd(source_b + index);
#else
                    __m256d r_values = _mm256_loadu_pd(source_r + index);
                    __m256d g_values = _mm256_loadu_pd(source_g + index);
                    __m256d b_values = _mm256_loadu_pd(source_b + index);
#endif
                    r_values = _mm256_mul_pd(r_values, kernel_values);
                    g_values = _mm256_mul_pd(g_values, kernel_values);
                    b_values = _mm256_mul_pd(b_values, kernel_values);
                    r_sum_pack = _mm256_add_pd(r_sum_pack, r_values);
                    g_sum_pack = _mm256_add_pd(g_sum_pack, g_values);
                    b_sum_pack = _mm256_add_pd(b_sum_pack, b_values);
                }
                double r_sum = utils::mm256d_sum(r_sum_pack);
                double g_sum = utils::mm256d_sum(g_sum_pack);
                double b_sum = utils::mm256d_sum(b_sum_pack);
                for (; i < kernel_size; ++i) {
                    size_t index = x + i + y * width - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                }
                size_t index = x * height + y;
                dest_r[index] = r_sum * kernel_sum_inverse;
                dest_g[index] = g_sum * kernel_sum_inverse;
                dest_b[index] = b_sum * kernel_sum_inverse;
            }
            for (; x < width; x += num_threads) {
                double r_sum = 0;
                double g_sum = 0;
                double b_sum = 0;
                double sum_weights = 0;
                for (size_t i = 0; i < kernel_size_half + width - x; ++i) {
                    ssize_t index = x + i + y * width - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                    sum_weights += kernel[i];
                }
                size_t index = x * height + y;
                dest_r[index] = r_sum / sum_weights;
                dest_g[index] = g_sum / sum_weights;
                dest_b[index] = b_sum / sum_weights;
            }
        }
    }

    void second_pass(const size_t thread_id, const size_t num_threads) {
        const auto& kernel = runtime->kernel;
        double kernel_sum_inverse = 0;
        for (size_t i = 0; i < kernel_size_half; ++i) {
            kernel_sum_inverse += 2 * kernel[i];
        }
        kernel_sum_inverse += kernel[kernel_size_half];
        kernel_sum_inverse = 1 / kernel_sum_inverse;

        auto* source_r = runtime->gauss_filtered_x.get<0>();
        auto* source_g = runtime->gauss_filtered_x.get<1>();
        auto* source_b = runtime->gauss_filtered_x.get<2>();
        auto* dest_r = runtime->gauss_filtered.get<0>();
        auto* dest_g = runtime->gauss_filtered.get<1>();
        auto* dest_b = runtime->gauss_filtered.get<2>();
        for (size_t x = 0; x < width; ++x) {
            size_t y = thread_id;
            for (; y < kernel_size_half; y += num_threads) {
                double r_sum = 0;
                double g_sum = 0;
                double b_sum = 0;
                double sum_weights = 0;
                for (size_t i = kernel_size_half - y; i < kernel_size; ++i) {
                    size_t index = x * height + i + y - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                    sum_weights += kernel[i];
                }
                size_t index = x + y * width;
                dest_r[index] = r_sum / sum_weights;
                dest_g[index] = g_sum / sum_weights;
                dest_b[index] = b_sum / sum_weights;
            }
            for (; y < height - kernel_size_half; y += num_threads) {
                __m256d r_sum_pack = _mm256_setzero_pd();
                __m256d g_sum_pack = _mm256_setzero_pd();
                __m256d b_sum_pack = _mm256_setzero_pd();
                size_t i;
                for (i = 0; i + 3 < kernel_size; i += 4) {
                    size_t index = x * height + i + y - kernel_size_half;
                    __m256d kernel_values = _mm256_load_pd(&kernel[i]);
                    // see comment above about aligned loads
#ifdef __OPTIMIZE__
                    __m256d r_values = _mm256_load_pd(source_r + index);
                    __m256d g_values = _mm256_load_pd(source_g + index);
                    __m256d b_values = _mm256_load_pd(source_b + index);
#else
                    __m256d r_values = _mm256_loadu_pd(source_r + index);
                    __m256d g_values = _mm256_loadu_pd(source_g + index);
                    __m256d b_values = _mm256_loadu_pd(source_b + index);
#endif
                    r_values = _mm256_mul_pd(r_values, kernel_values);
                    g_values = _mm256_mul_pd(g_values, kernel_values);
                    b_values = _mm256_mul_pd(b_values, kernel_values);
                    r_sum_pack = _mm256_add_pd(r_sum_pack, r_values);
                    g_sum_pack = _mm256_add_pd(g_sum_pack, g_values);
                    b_sum_pack = _mm256_add_pd(b_sum_pack, b_values);
                }
                double r_sum = utils::mm256d_sum(r_sum_pack);
                double g_sum = utils::mm256d_sum(g_sum_pack);
                double b_sum = utils::mm256d_sum(b_sum_pack);
                for (; i < kernel_size; ++i) {
                    size_t index = x * height + i + y - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                }
                size_t index = x + y * width;
                dest_r[index] = r_sum * kernel_sum_inverse;
                dest_g[index] = g_sum * kernel_sum_inverse;
                dest_b[index] = b_sum * kernel_sum_inverse;
            }
            for (; y < height; y += num_threads) {
                double r_sum = 0;
                double g_sum = 0;
                double b_sum = 0;
                double sum_weights = 0;
                for (size_t i = 0; i < kernel_size_half + height - y; ++i) {
                    ssize_t index = x * height + i + y - kernel_size_half;
                    r_sum += source_r[index] * kernel[i];
                    g_sum += source_g[index] * kernel[i];
                    b_sum += source_b[index] * kernel[i];
                    sum_weights += kernel[i];
                }
                size_t index = x + y * width;
                dest_r[index] = r_sum / sum_weights;
                dest_g[index] = g_sum / sum_weights;
                dest_b[index] = b_sum / sum_weights;
            }
        }
    }
};


template <typename C>
class gauss_filter_generic {
private:
    const imagedb::image_store& data;
    const size_t stddev;
    const size_t kernel_size;
    const size_t kernel_size_half;

    struct runtime_t {
        utils::timer timer;
        C storage;
        storage_dense_image_double storage_rows;
        storage_dense_image_double storage_columns;
        std::unique_ptr<typename C::threaded_loader> loader;
        aligned_vector<double, 32> kernel;

        runtime_t() : timer("allocate data structures") {}
    };

    std::unique_ptr<runtime_t> runtime;

public:
    gauss_filter_generic(const imagedb::image_store& data, const size_t stddev)
    : data(data), stddev(stddev),
      kernel_size(stddev * 6 + 1), kernel_size_half(stddev * 3) {}

    C run(const size_t num_threads) {
        runtime = std::make_unique<runtime_t>();
        runtime->loader = std::move(runtime->storage.load_threaded_ptr(data, num_threads));
        runtime->kernel = get_gauss_kernel(stddev, kernel_size);

        threading::barrier<threading::spin_lock> barrier(num_threads);
        std::vector<std::thread> threads;
        threads.reserve(num_threads);
        for (size_t i = 0; i < num_threads; ++i) {
            threads.emplace_back([this, i, num_threads, &barrier] {
                barrier.wait_run_once([this] {
                    runtime->timer.replace("load data into storage");
                });
                (*runtime->loader)(i);
                barrier.wait_run_once([this] {
                    runtime->loader.reset(nullptr);
                    runtime->timer.replace("allocate tmp storage");
                    std::array<ssize_t, 3> dim_min = runtime->storage.get_dim_min();
                    std::array<ssize_t, 3> dim_max = runtime->storage.get_dim_max();
                    runtime->storage_columns = storage_dense_image_double{dim_min, dim_max};
                    std::swap(dim_min[1], dim_min[2]);
                    std::swap(dim_max[1], dim_max[2]);
                    runtime->storage_rows = storage_dense_image_double{dim_min, dim_max};
                    runtime->timer.replace("load storage into tmp storage");
                });
                load_tmp(i, num_threads);
                barrier.wait_run_once([this] {
                    runtime->timer.replace("first pass");
                });
                first_pass(i, num_threads);
                barrier.wait_run_once([this] {
                    runtime->timer.replace("second pass");
                });
                second_pass(i, num_threads);
                barrier.wait_run_once([this] {
                    runtime->timer.replace("store tmp storage in storage");
                });
                store_tmp(i, num_threads);
            });
        }
        for (auto& t : threads) {
            t.join();
        }
        C result{std::move(runtime->storage)};
        runtime.reset(nullptr);
        return result;
    }

private:
    void load_tmp(const size_t thread_id, const size_t num_threads) {
        for (auto image_id_dim : runtime->storage) {
            auto image = runtime->storage_columns[image_id_dim.first];
            for (auto x_dim : image_id_dim.second) {
                auto y_dim_it = x_dim.second.begin();
                y_dim_it += thread_id;
                auto end = x_dim.second.end();
                for (; y_dim_it < end; y_dim_it += num_threads) {
                    auto pixel = image[x_dim.first][y_dim_it->first];
                    pixel.template get<0>() = y_dim_it->second.template get<0>();
                    pixel.template get<1>() = y_dim_it->second.template get<1>();
                    pixel.template get<2>() = y_dim_it->second.template get<2>();
                }
            }
        }
    }

    void store_tmp(const size_t thread_id, const size_t num_threads) {
        for (auto image_id_dim : runtime->storage) {
            auto image = runtime->storage_columns[image_id_dim.first];
            for (auto x_dim : image_id_dim.second) {
                auto y_dim_it = x_dim.second.begin();
                y_dim_it += thread_id;
                auto end = x_dim.second.end();
                for (; y_dim_it < end; y_dim_it += num_threads) {
                    auto pixel = image[x_dim.first][y_dim_it->first];
                    y_dim_it->second.template get<0>() = pixel.template get<0>();
                    y_dim_it->second.template get<1>() = pixel.template get<1>();
                    y_dim_it->second.template get<2>() = pixel.template get<2>();
                }
            }
        }
    }

    void first_pass(const size_t thread_id, const size_t num_threads) {
        const size_t height = runtime->storage.get_dim_size()[2];
        const auto& kernel = runtime->kernel;
        double kernel_sum_inverse = 0;
        for (size_t i = 0; i < kernel_size_half; ++i) {
            kernel_sum_inverse += 2 * kernel[i];
        }
        kernel_sum_inverse += kernel[kernel_size_half];
        kernel_sum_inverse = 1 / kernel_sum_inverse;

        for (auto image_id_dim : runtime->storage_columns) {
            auto dest_img = runtime->storage_rows[image_id_dim.first];
            for (auto x_dim : image_id_dim.second) {
                const auto x = x_dim.first;
                auto y_dim_it_begin = x_dim.second.begin();
                size_t y = thread_id;
                for (; y < kernel_size_half; y += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    double sum_weights = 0;
                    for (size_t i = kernel_size_half - y; i < kernel_size; ++i) {
                        size_t index = i + y - kernel_size_half;
                        auto pixel = y_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                        sum_weights += kernel[i];
                    }
                    auto dest_pixel = dest_img[y][x];
                    dest_pixel.template get<0>() = r_sum / sum_weights;
                    dest_pixel.template get<1>() = g_sum / sum_weights;
                    dest_pixel.template get<2>() = b_sum / sum_weights;
                }
                for (; y < height - kernel_size_half; y += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    for (size_t i = 0; i < kernel_size; ++i) {
                        size_t index = i + y - kernel_size_half;
                        auto pixel = y_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                    }
                    auto dest_pixel = dest_img[y][x];
                    dest_pixel.template get<0>() = r_sum * kernel_sum_inverse;
                    dest_pixel.template get<1>() = g_sum * kernel_sum_inverse;
                    dest_pixel.template get<2>() = b_sum * kernel_sum_inverse;
                }
                for (; y < height; y += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    double sum_weights = 0;
                    for (size_t i = 0; i < kernel_size_half + height - y; ++i) {
                        size_t index = i + y - kernel_size_half;
                        auto pixel = y_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                        sum_weights += kernel[i];
                    }
                    auto dest_pixel = dest_img[y][x];
                    dest_pixel.template get<0>() = r_sum / sum_weights;
                    dest_pixel.template get<1>() = g_sum / sum_weights;
                    dest_pixel.template get<2>() = b_sum / sum_weights;
                }
            }
        }
    }

    void second_pass(const size_t thread_id, const size_t num_threads) {
        const size_t width = runtime->storage.get_dim_size()[1];
        const auto& kernel = runtime->kernel;
        double kernel_sum_inverse = 0;
        for (size_t i = 0; i < kernel_size_half; ++i) {
            kernel_sum_inverse += 2 * kernel[i];
        }
        kernel_sum_inverse += kernel[kernel_size_half];
        kernel_sum_inverse = 1 / kernel_sum_inverse;

        for (auto image_id_dim : runtime->storage_rows) {
            auto dest_img = runtime->storage_columns[image_id_dim.first];
            for (auto y_dim : image_id_dim.second) {
                const auto y = y_dim.first;
                auto x_dim_it_begin = y_dim.second.begin();
                size_t x = thread_id;
                for (; x < kernel_size_half; x += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    double sum_weights = 0;
                    for (size_t i = kernel_size_half - x; i < kernel_size; ++i) {
                        size_t index = i + x - kernel_size_half;
                        auto pixel = x_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                        sum_weights += kernel[i];
                    }
                    auto dest_pixel = dest_img[x][y];
                    dest_pixel.template get<0>() = r_sum / sum_weights;
                    dest_pixel.template get<1>() = g_sum / sum_weights;
                    dest_pixel.template get<2>() = b_sum / sum_weights;
                }
                for (; x < width - kernel_size_half; x += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    for (size_t i = 0; i < kernel_size; ++i) {
                        size_t index = i + x - kernel_size_half;
                        auto pixel = x_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                    }
                    auto dest_pixel = dest_img[x][y];
                    dest_pixel.template get<0>() = r_sum * kernel_sum_inverse;
                    dest_pixel.template get<1>() = g_sum * kernel_sum_inverse;
                    dest_pixel.template get<2>() = b_sum * kernel_sum_inverse;
                }
                for (; x < width; x += num_threads) {
                    double r_sum = 0;
                    double g_sum = 0;
                    double b_sum = 0;
                    double sum_weights = 0;
                    for (size_t i = 0; i < kernel_size_half + width - x; ++i) {
                        size_t index = i + x - kernel_size_half;
                        auto pixel = x_dim_it_begin[index].second;
                        r_sum += pixel.template get<0>() * kernel[i];
                        g_sum += pixel.template get<1>() * kernel[i];
                        b_sum += pixel.template get<2>() * kernel[i];
                        sum_weights += kernel[i];
                    }
                    auto dest_pixel = dest_img[x][y];
                    dest_pixel.template get<0>() = r_sum / sum_weights;
                    dest_pixel.template get<1>() = g_sum / sum_weights;
                    dest_pixel.template get<2>() = b_sum / sum_weights;
                }
            }
        }
    }
};


int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " db-file query" << std::endl;
        std::cerr << "  Available queries: gauss gauss_dense_storage";
        std::cerr << std::endl;
        return 2;
    }
    std::string query{argv[2]};
    imagedb::image_store data;
    {
        utils::timer t{"load database"};
        data.load_from_file(argv[1]);
    }
    if (query == "gauss") {
        imagedb::image result;
        {
            utils::timer t;
            result = gauss_filter(data, 3088, 2056, 1, 10).run(8);
        }
        imagedb::write_png(result, 3088, 2056, "img.png");
    } else if (query == "gauss_dense_storage") {
        storage_dense_image result;
        {
            utils::timer t;
            result = gauss_filter_generic<storage_dense_image>(data, 10).run(8);
        }
        imagedb::write_png(result[1], 3088, 2056, "img.png");
    } else {
        std::cerr << "invalid query" << std::endl;
        return 2;
    }
    return 0;
}
