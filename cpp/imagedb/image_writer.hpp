#ifndef _IMAGEDB_IMAGE_WRITER_HPP
#define _IMAGEDB_IMAGE_WRITER_HPP

// fix for bug with Boost and libpng, see
// https://github.com/ignf/gilviewer/issues/8#issuecomment-86047808
#define png_infopp_NULL (png_infopp)NULL
#define int_p_NULL (int*)NULL
#define png_bytep_NULL (png_bytep)NULL

#include <string>
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/png_io.hpp>
#include "imagedb.hpp"


namespace imagedb {


template <typename Img>
void write_png(
    const Img& img, const size_t width, const size_t height, const std::string& path
) {
    namespace gil = boost::gil;
    gil::rgb8_image_t png_img(width, height);
    auto view = gil::view(png_img);
    for (size_t x = 0; x < width; ++x) {
        for (size_t y = 0; y < height; ++y) {
            gil::get_color(view(x, y), gil::red_t{}) = img[x][y].template get<0>();
            gil::get_color(view(x, y), gil::green_t{}) = img[x][y].template get<1>();
            gil::get_color(view(x, y), gil::blue_t{}) = img[x][y].template get<2>();
        }
    }
    gil::png_write_view(path, view);
}


template <> void write_png<image>(
    const image& img, const size_t width, const size_t height, const std::string& path
) {
    namespace gil = boost::gil;
    gil::rgb8_image_t png_img(width, height);
    auto view = gil::view(png_img);
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            size_t index = x + y * width;
            gil::get_color(view(x, y), gil::red_t{}) = img.get<0>(index);
            gil::get_color(view(x, y), gil::green_t{}) = img.get<1>(index);
            gil::get_color(view(x, y), gil::blue_t{}) = img.get<2>(index);
        }
    }
    gil::png_write_view(path, view);
}

}

#endif
