#include <iostream>
#include <boost/filesystem.hpp>
#include "imagedb.hpp"
#include "images_iterator.hpp"


namespace fs = boost::filesystem;


int main(int argc, char** argv) {
    bool append;
    const char* dir_file;
    const char* dbfile;
    if (argc == 4 && argv[1] == std::string{"--append"}) {
        append = true;
        dir_file = argv[2];
        dbfile = argv[3];
    } else if (argc == 3) {
        append = false;
        dir_file = argv[1];
        dbfile = argv[2];
    } else {
        std::cerr << "Usage: " << argv[0] << " [--append] directory/image-file db-file";
        std::cerr << std::endl;
        return 2;
    }
    imagedb::image_store data;
    if (append) {
        data.load_from_file(dbfile);
    }
    fs::path path{dir_file};
    if (fs::is_directory(path)) {
        imagedb::iterate_directory_pixels(
            path,
            [&data] (
                const uint32_t image_id, const uint16_t x, const uint16_t y,
                const uint8_t r, const uint8_t g, const uint8_t b
            ) {
                data.push_back(image_id, x, y, r, g, b);
                return true;
            }
        );
    } else {
        imagedb::iterate_tiff_pixels(
            path,
            [&data] (
                const uint16_t x, const uint16_t y,
                const uint8_t r, const uint8_t g, const uint8_t b
            ) {
                data.push_back(0, x, y, r, g, b);
            }
        );
    }
    data.write_to_file(dbfile);
    return 0;
}
