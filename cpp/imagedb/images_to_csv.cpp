#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include "imagedb.hpp"
#include "images_iterator.hpp"


namespace fs = boost::filesystem;


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " directory" << std::endl;
        return 2;
    }
    fs::path dir{argv[1]};
    if (!fs::is_directory(dir)) {
        std::cerr << "Invalid directory: " << dir << std::endl;
        return 2;
    }
    std::string buffer;
    buffer.reserve(1 << 16);
    std::vector<std::string> int_cache;
    int_cache.reserve(4000);
    for (uint16_t i = 0; i < 4000; ++i) {
        int_cache.push_back(std::to_string(i));
    }
    int counter = 0;
    imagedb::iterate_directory_pixels(
        dir,
        [&buffer, &int_cache, &counter] (
            const uint32_t image_id, const uint16_t x, const uint16_t y,
            const uint8_t r, const uint8_t g, const uint8_t b
        ) {
            buffer += int_cache[image_id];
            buffer += ';';
            buffer += int_cache[x];
            buffer += ';';
            buffer += int_cache[y];
            buffer += ';';
            buffer += int_cache[r];
            buffer += ';';
            buffer += int_cache[g];
            buffer += ';';
            buffer += int_cache[b];
            buffer += '\n';
            if (buffer.size() + 100 >= (1 << 16)) {
                std::cout << buffer;
                buffer.clear();
            }
            return true;
        }
    );
    return 0;
}
