#ifndef _MIPAS_MIPAS_TYPES_HPP
#define _MIPAS_MIPAS_TYPES_HPP

#include <array>
#include <cstring>
#include <limits>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include "utils.hpp"


namespace mipas {

constexpr size_t MPH_SIZE = 1247;
constexpr size_t SPH_SIZE = 848;
constexpr size_t DSD_SIZE = 280;
constexpr size_t GEO_LOCATION_SIZE = 100;
constexpr size_t DS_STRUCTURE_SIZE = 1020;
constexpr size_t SPECIES_COUNT = 30;


struct MPH {
    std::string product;
    std::string proc_stage;
    std::string ref_doc;
    std::string acquisition_station;
    std::string proc_center;
    std::string proc_time;
    std::string software_ver;
    std::string sensing_start;
    std::string sensing_stop;
    std::string phase;
    uint8_t cycle;
    int16_t rel_orbit;
    int32_t abs_orbit;
    std::string state_vector_time;
    double delta_ut1;
    double x_position;
    double y_position;
    double z_position;
    double x_velocity;
    double y_velocity;
    double z_velocity;
    std::string vector_source;
    std::string utc_sbt_time;
    uint32_t sat_binary_time;
    uint32_t clock_step;
    std::string leap_utc;
    int8_t leap_sign;
    int32_t leap_err;
    int32_t product_err;
    int64_t tot_size;
    int32_t sph_size;
    int32_t num_dsd;
    int32_t dsd_size;
    int32_t num_data_sets;
};


struct SPH {
    std::string sph_descriptor;
    uint8_t stripline_continuity_indicator;
    uint8_t slice_position;
    uint8_t num_slices;
    std::string start_time;
    std::string stop_time;
    int32_t first_tangent_lat;
    int32_t first_tangent_long;
    int32_t last_tangent_lat;
    int32_t last_tangent_long;
    int16_t num_scans;
    int16_t num_los_geoms;
    int16_t num_scans_per_ds;
    int16_t num_scans_proc;
    int16_t num_sp_not_proc;
    int16_t num_spectra;
    int16_t num_spectr_proc;
    int16_t num_gain_cal;
    int16_t tot_granules;
    float max_path_diff;
    std::string order_of_species;
    int16_t num_sweeps_per_scan;
};


struct DSD {
    std::string ds_name;
    // DS type (http://www.stcorp.nl/coda/codadef/AEOLUS/types/DSD.html)
    // 'M': Measurement Data Set
    // 'A': Annotation Data Set
    // 'G': Global Annotation Data Set
    // 'R': Reference Data Set (no DS attached)
    char ds_type;
    std::string filename;
    int64_t ds_offset;
    int64_t ds_size;
    int32_t num_dsr;
    int32_t dsr_size;
};


const std::array<const char*, 25> DATA_SETS = {
    "SUMMARY QUALITY ADS         ",
    "SCAN GEOLOCATION ADS        ",
    "DATASET STRUCTURE ADS       ",
    "SCAN INFORMATION MDS        ",
    "PT RETRIEVAL MDS            ",
    "H2O RETRIEVAL MDS           ",
    "O3 RETRIEVAL MDS            ",
    "HNO3 RETRIEVAL MDS          ",
    "CH4 RETRIEVAL MDS           ",
    "N2O RETRIEVAL MDS           ",
    "NO2 RETRIEVAL MDS           ",
    "F11 RETRIEVAL MDS           ",
    "CLNO RETRIEVAL MDS          ",
    "N2O5 RETRIEVAL MDS          ",
    "F12 RETRIEVAL MDS           ",
    "COF2 RETRIEVAL MDS          ",
    "CCL4 RETRIEVAL MDS          ",
    "HCN RETRIEVAL MDS           ",
    "F14 RETRIEVAL MDS           ",
    "F22 RETRIEVAL MDS           ",
    "CONTINUUM AND OFFSET MDS    ",
    "PCD INFORMATION ADS         ",
    "MICROWINDOW OCCUPATION ADS  ",
    "RESIDUAL SPECTRA ADS        ",
    "PROCESSING PARAMETERS ADS   ",
};


struct MipasHeader {
    MPH mph;
    SPH sph;
    std::array<DSD, DATA_SETS.size()> dsds;
};


// see: https://earth.esa.int/handbooks/mipas/CNTR5-3-4.html#eph.mipas.gloss.tech.point:ZPD
struct EnvisatTime {
    // days since January 1st, 2000
    int32_t days;
    // seconds since start of day
    uint32_t seconds;
    // microseconds since start of second
    uint32_t microseconds;

    static constexpr EnvisatTime max() {
        return {
            std::numeric_limits<int32_t>::max(),
            std::numeric_limits<uint32_t>::max(),
            std::numeric_limits<uint32_t>::max()
        };
    }

    int64_t as_int() const {
        return (
            (static_cast<uint64_t>(days) << 37) |
            (static_cast<uint64_t>(seconds) << 20) |
            microseconds
        );
    }

    static EnvisatTime from_int(const int64_t date_int) {
        return EnvisatTime{
            static_cast<int32_t>(date_int >> 37),
            static_cast<uint32_t>((static_cast<uint64_t>(date_int) >> 20) & ((1ul << 17) - 1)),
            static_cast<uint32_t>(static_cast<uint64_t>(date_int) & ((1ul << 20) - 1))
        };
    }

    std::string to_iso() const;
    static EnvisatTime from_date(
        uint16_t year, uint8_t month, uint8_t day, uint8_t hours, uint8_t minutes, uint8_t seconds
    );
};

#define make_EnvisatTime_rel_operator(op) \
    inline bool operator op (const EnvisatTime& t1, const EnvisatTime& t2) { \
        return ( \
            std::tie(t1.days, t1.seconds, t1.microseconds) op \
            std::tie(t2.days, t2.seconds, t2.microseconds) \
        ); \
    }

make_EnvisatTime_rel_operator(==)
make_EnvisatTime_rel_operator(!=)
make_EnvisatTime_rel_operator(>)
make_EnvisatTime_rel_operator(>=)
make_EnvisatTime_rel_operator(<)
make_EnvisatTime_rel_operator(<=)


struct SummaryQuality {
    struct macro_micro {
        uint16_t macro;
        uint16_t micro;
    };

    EnvisatTime dsr_time;
    macro_micro p_t_term_macro_micro;
    macro_micro vmr_term_macro_micro[SPECIES_COUNT];
    uint16_t p_t_term_run_time;
    uint16_t vmr_term_run_time[SPECIES_COUNT];
    uint8_t attach_flag;
};


struct LocationPoint {
    int32_t latitude;
    int32_t longitude;
};

struct GeoLocation {
    double first_alt;
    double last_alt;
    EnvisatTime dsr_time;
    LocationPoint loc_first;
    LocationPoint loc_last;
    LocationPoint loc_mid;
    int32_t local_solar_time;
    int32_t sat_target_azi;
    int32_t target_sun_azi;
    int32_t target_sun_elev;
    uint8_t attach_flag;
};


struct ds_pointer_t {
    int32_t dsr_offset;
    uint32_t dsr_length;
};

struct DSStructure {
    EnvisatTime dsr_time;
    ds_pointer_t ds_pointer[37];
    uint16_t num_sweeps;
    uint16_t num_p_t_pts;
    uint16_t num_vmr_pts[30];
    uint16_t flags_p_t_error_flag[30];
    uint16_t num_con_params_p_t;
    uint16_t num_con_params_vmr[30];
    uint16_t num_instr_offset_p_t;
    uint16_t num_instr_offset_vmr[30];
    uint16_t max_num_micro_p_t;
    uint16_t max_num_micro_vmr[30];
    uint16_t tot_num_p_t_micro_all_alt;
    uint16_t tot_num_vmr_micro_all_alt[30];
    uint16_t tot_num_spect_grid_p_t;
    uint16_t tot_num_spect_grid_vmr[30];
    uint16_t num_grid_con_p_t;
    uint16_t num_grid_con_vmr[30];
    uint16_t num_evo_steps_p_t;
    uint16_t num_evo_steps_vmr[30];
    uint16_t num_pcd_info;
    uint16_t num_base_p_t_pts;
    uint16_t num_base_vmr_pts[30];
    uint16_t num_mw_labels_p_t;
    uint16_t num_mw_labels_vmr[30];
    uint8_t attach_flag;
};


#define ScanInfo_offset(type, name, prev_type, prev_name) \
static constexpr size_t name##_offset(const size_t num_sweeps) { \
    return utils::align<type>(prev_name##_offset(num_sweeps) + num_sweeps * sizeof(prev_type)); \
}

#define ScanInfo_array(type, name) type name() const { \
    return reinterpret_cast<type>(data.get() + name##_offset(num_sweeps)); \
}

#define ScanInfo_ret_array(type, name) type name() const { \
    return reinterpret_cast<type>(si.data.get() + name##_offset(si.num_sweeps)); \
}

#define ScanInfo_ret_array_species(type, name) type name() const { \
    return reinterpret_cast<type>( \
        si.data.get() + \
        species * size(si.num_sweeps) + \
        name##_offset(si.num_sweeps) \
    ); \
}

#define COMMA ,

struct ScanInfo {
public:
    struct base_data_t {
        EnvisatTime dsr_time;
        uint32_t dsr_length;
        uint16_t appl_process_id;
        int8_t quality_flag;
        uint8_t retrieval_p_t_flag;
        uint8_t retrieval_vmr_flag[SPECIES_COUNT];
        uint8_t marq_p_t_flag;
        uint8_t marq_vmr_flag[SPECIES_COUNT];
        uint8_t chi2_p_t_flag;
        uint8_t chi2_vmr_flag[SPECIES_COUNT];
    };

    class retrieval_p_t_t {
    private:
        friend class ScanInfo;

        const ScanInfo& si;

        static constexpr size_t lrv_p_t_flag_offset(const size_t num_sweeps) {
            return utils::align<uint8_t>(ScanInfo::retrieval_p_t_offset(num_sweeps));
        }

        ScanInfo_offset(float, pressure, uint8_t, lrv_p_t_flag)
        ScanInfo_offset(float, pressure_variance, float, pressure)
        ScanInfo_offset(float, tangent_altitude, float, pressure_variance)
        ScanInfo_offset(float, height_cor_variance, float, tangent_altitude)
        ScanInfo_offset(float, temp, float, height_cor_variance)
        ScanInfo_offset(float, temp_variance, float, temp)
        ScanInfo_offset(float, ecmwf_corr_altitude, float, temp_variance)

        static constexpr size_t size(const size_t num_sweeps) {
            return ecmwf_corr_altitude_offset(num_sweeps) + num_sweeps * sizeof(float);
        }

        retrieval_p_t_t(const ScanInfo& si) : si(si) {}

    public:
        ScanInfo_ret_array(uint8_t*, lrv_p_t_flag)
        ScanInfo_ret_array(float*, pressure)
        ScanInfo_ret_array(float*, pressure_variance)
        ScanInfo_ret_array(float*, tangent_altitude)
        ScanInfo_ret_array(float*, height_cor_variance)
        ScanInfo_ret_array(float*, temp)
        ScanInfo_ret_array(float*, temp_variance)
        ScanInfo_ret_array(float*, ecmwf_corr_altitude)
    };

    class retrieval_vmr_t {
    private:
        friend class ScanInfo;

        const ScanInfo& si;
        const size_t species;

        static constexpr size_t lrv_vmr_flag_offset(const size_t num_sweeps) {
            return utils::align<uint8_t>(ScanInfo::retrieval_vmr_offset(num_sweeps));
        }

        ScanInfo_offset(float, vmr, uint8_t, lrv_vmr_flag)
        ScanInfo_offset(float, vmr_variance, float, vmr)
        ScanInfo_offset(float, concentration, float, vmr_variance)
        ScanInfo_offset(double, concentration_variance, float, concentration)
        ScanInfo_offset(float, vertical_col_density, double, concentration_variance)
        ScanInfo_offset(double, vcd_variance, float, vertical_col_density)

        static constexpr size_t size(const size_t num_sweeps) {
            return vcd_variance_offset(num_sweeps) + num_sweeps * sizeof(double);
        }

        retrieval_vmr_t(const ScanInfo& si, const size_t species) : si(si), species(species) {}

    public:
        ScanInfo_ret_array_species(uint8_t*, lrv_vmr_flag)
        ScanInfo_ret_array_species(float*, vmr)
        ScanInfo_ret_array_species(float*, vmr_variance)
        ScanInfo_ret_array_species(float*, concentration)
        ScanInfo_ret_array_species(float*, concentration_variance)
        ScanInfo_ret_array_species(float*, vertical_col_density)
        ScanInfo_ret_array_species(float*, vcd_variance)
    };


private:
    size_t num_sweeps;
    std::unique_ptr<char[]> data;

    static constexpr size_t zpd_crossing_time_offset(const size_t) {
        return utils::align<EnvisatTime>(sizeof(base_data_t));
    }

    ScanInfo_offset(LocationPoint, geolocation_los_tangent, EnvisatTime, zpd_crossing_time)
    ScanInfo_offset(double, tangent_altitude_los, LocationPoint, geolocation_los_tangent)

    static constexpr size_t retrieval_p_t_offset(const size_t num_sweeps) {
        return tangent_altitude_los_offset(num_sweeps) + num_sweeps * sizeof(double);
    }

    static constexpr size_t retrieval_vmr_offset(const size_t num_sweeps) {
        return retrieval_p_t_offset(num_sweeps) + retrieval_p_t_t::size(num_sweeps);
    }

    static constexpr size_t cloud_det_mw_label_offset(const size_t num_sweeps) {
        return utils::align<std::array<utils::fixed_string<8>, 3>>(
            retrieval_vmr_offset(num_sweeps) +
            SPECIES_COUNT * retrieval_vmr_t::size(num_sweeps) +
            retrieval_vmr_t::size(num_sweeps)
        );
    }

    ScanInfo_offset(
        std::array<float COMMA 3>, cloud_index,
        std::array<utils::fixed_string<8> COMMA 3>, cloud_det_mw_label
    )
    ScanInfo_offset(
        std::array<float COMMA 3>, cloud_index_threshold,
        std::array<float COMMA 3>, cloud_index
    )
    ScanInfo_offset(
        std::array<uint8_t COMMA 3>, cloud_detect_flag,
        std::array<float COMMA 3>, cloud_index_threshold
    )

    static constexpr size_t size(const size_t num_sweeps) {
        return cloud_detect_flag_offset(num_sweeps) + num_sweeps * sizeof(std::array<uint8_t, 3>);
    }

public:
    ScanInfo() : num_sweeps(0) {}
    ScanInfo(const ScanInfo& si) : num_sweeps(si.num_sweeps) {
        data = std::make_unique<char[]>(size(num_sweeps));
        std::memcpy(data.get(), si.data.get(), size(num_sweeps));
    }
    ScanInfo& operator=(ScanInfo& si) {
        num_sweeps = si.num_sweeps;
        data = std::make_unique<char[]>(size(num_sweeps));
        std::memcpy(data.get(), si.data.get(), size(num_sweeps));
        return *this;
    }

    ScanInfo(ScanInfo&& si) : num_sweeps(si.num_sweeps), data(std::move(si.data)) {}
    ScanInfo& operator=(ScanInfo&& si) {
        num_sweeps = si.num_sweeps;
        data = std::move(si.data);
        return *this;
    }

    ScanInfo(const size_t num_sweeps) : num_sweeps(num_sweeps) {
        data = std::make_unique<char[]>(size(num_sweeps));
    }

    inline size_t get_num_sweeps() const {
        return num_sweeps;
    }

    base_data_t& base_data() const {
        return *reinterpret_cast<base_data_t*>(data.get());
    }

    ScanInfo_array(EnvisatTime*, zpd_crossing_time)
    ScanInfo_array(LocationPoint*, geolocation_los_tangent)
    ScanInfo_array(double*, tangent_altitude_los)

    retrieval_p_t_t retrieval_p_t() const {
        return retrieval_p_t_t{*this};
    }

    retrieval_vmr_t retrieval_vmr(const size_t species) const {
        return retrieval_vmr_t{*this, species};
    }

    ScanInfo_array(std::array<utils::fixed_string<8> COMMA 3>*, cloud_det_mw_label)
    ScanInfo_array(std::array<float COMMA 3>*, cloud_index)
    ScanInfo_array(std::array<float COMMA 3>*, cloud_index_threshold)
    ScanInfo_array(std::array<uint8_t COMMA 3>*, cloud_detect_flag)
};


inline std::ostream& operator<<(std::ostream& stream, const EnvisatTime& et) {
    stream << "days: " << et.days << "\tseconds: " << et.seconds;
    stream << "\tmicroseconds: " << et.microseconds;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const SummaryQuality& sq);
std::ostream& operator<<(std::ostream& stream, const LocationPoint& lp);
std::ostream& operator<<(std::ostream& stream, const GeoLocation& gl);

}


namespace utils {

template <>
constexpr mipas::EnvisatTime max_inf<mipas::EnvisatTime>() {
    return mipas::EnvisatTime::max();
}

}

#endif
