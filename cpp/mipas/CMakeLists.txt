include_directories(${CMAKE_CURRENT_SOURCE_DIR})

add_library(mipas OBJECT mipas.cpp mipas_types.cpp ../mmap.cpp)

add_executable(mipas_to_csv mipas_to_csv.cpp $<TARGET_OBJECTS:mipas>)
target_link_libraries(mipas_to_csv ${Boost_DATE_TIME_LIBRARY})

add_executable(create-db create-db.cpp $<TARGET_OBJECTS:mipas>)
target_link_libraries(
    create-db ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_DATE_TIME_LIBRARY}
)

add_executable(mipas-queries queries.cpp $<TARGET_OBJECTS:mipas>)
add_dependencies(mipas-queries generate-storage)
target_link_libraries(mipas-queries ${Boost_DATE_TIME_LIBRARY})
