#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include "column_store.hpp"
#include "exceptions.hpp"
#include "mipas.hpp"


namespace fs = boost::filesystem;
using mipas_store = column_store<mipas::EnvisatTime, int32_t, int32_t, double, float, float>;


void parse_file(mipas_store& data, const fs::path& file) {
    mipas::MipasFile m(file.string());
    for (const auto& si : m.get_scan_infos()) {
        const auto pt = si.retrieval_p_t();
        for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
            data.push_back(
                si.zpd_crossing_time()[i],
                si.geolocation_los_tangent()[i].latitude,
                si.geolocation_los_tangent()[i].longitude,
                si.tangent_altitude_los()[i],
                pt.pressure()[i],
                pt.temp()[i]
            );
        }
    }
}


void process_file(mipas_store& data, const fs::path& file) {
    if (!fs::is_regular_file(file)) {
        return;
    }
    const std::string path_str = file.string();
    if (path_str.substr(path_str.size() - 3) != ".N1") {
        return;
    }
    std::cout << path_str << std::endl;
    try {
        parse_file(data, file);
    } catch (mipas::parse_error& e) {
        std::cerr << "parse error for file " << file << ": " << e.what() << std::endl;
    }
}


int main(int argc, char** argv) {
    bool append = false;
    const char* directory = nullptr;
    const char* dbfile = nullptr;
    if ((argc == 3 || argc == 4) && argv[1] == std::string{"--append"}) {
        append = true;
        if (argc == 3) {
            dbfile = argv[2];
        } else if (argc == 4) {
            directory = argv[2];
            dbfile = argv[3];
        }
    } else if (argc == 2) {
        dbfile = argv[1];
    } else if (argc == 3) {
        directory = argv[1];
        dbfile = argv[2];
    } else {
        std::cerr << "Usage: " << argv[0] << " [--append] [directory] db-file" << std::endl;
        return 2;
    }
    if (directory != nullptr) {
        fs::path dir{directory};
        if (!fs::is_directory(dir)) {
            std::cerr << "Invalid directory: " << dir << std::endl;
            return 2;
        }
    }
    mipas_store data;
    if (append && fs::is_regular_file(fs::path{dbfile})) {
        data.load_from_file(dbfile);
    }
    if (directory != nullptr) {
        for (const auto& entry : fs::recursive_directory_iterator{fs::path{directory}}) {
            process_file(data, entry.path());
        }
    } else {
        std::string str_path;
        while (std::getline(std::cin, str_path)) {
            process_file(data, fs::path{str_path});
        }
    }
    std::cerr << data.size() << std::endl;
    data.write_to_file(dbfile);
    return 0;
}
