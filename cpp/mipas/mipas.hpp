// Parser for the MIP_NL__2P version 4 file format, see also:
// http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/products/MIP_NL__2P_v4.html
#ifndef _MIPAS_HPP
#define _MIPAS_HPP

#include <iterator>
#include <limits>
#include <memory>
#include <ostream>
#include <string>
#include "mipas_types.hpp"


namespace mipas {

struct MipasFileImpl;
class MipasFile;


template <typename T, typename S>
class ScanContainer {
public:
    class Iterator {
    public:
        using iterator_category = std::input_iterator_tag;
        using value_type = const T;
        using difference_type = int64_t;
        using pointer = value_type*;
        using reference = value_type&;

    private:
        const ScanContainer& container;
        size_t index;
        static constexpr size_t NOT_CACHED = std::numeric_limits<size_t>::max();
        size_t cached_index;
        std::unique_ptr<T> cache;

    public:
        Iterator(const ScanContainer& container)
        : container(container), index(0), cached_index(NOT_CACHED) {}
        Iterator(const ScanContainer& container, const size_t index)
        : container(container), index(index), cached_index(NOT_CACHED) {}
        Iterator(const Iterator& it)
        : container(it.container), index(it.index), cached_index(NOT_CACHED) {}

        Iterator& operator+=(const difference_type n) {
            index += n;
            return *this;
        }

        Iterator operator+(const difference_type n) const {
            Iterator new_it{*this};
            new_it.index += n;
            return new_it;
        }

        Iterator& operator++() {
            return operator+=(1);
        }

        Iterator operator++(int) const {
            return operator+(1);
        }

        Iterator& operator-=(const difference_type n) {
            index -= n;
            return *this;
        }

        Iterator operator-(const difference_type n) const {
            Iterator new_it{*this};
            new_it.index -= n;
            return new_it;
        }

        difference_type operator-(const Iterator& other) const {
            return index - other.index;
        }

        bool operator==(const Iterator& other) const {
            return index == other.index;
        }

        bool operator!=(const Iterator& other) const {
            return !operator==(other);
        }

        reference operator*() {
            if (index != cached_index) {
                if (cached_index == NOT_CACHED) {
                    cache = std::make_unique<T>();
                }
                *cache = container[index];
                cached_index = index;
            }
            return *cache;
        }
    };

    Iterator begin() const {
        return Iterator{*this};
    }

    Iterator end() const {
        return Iterator{*this, size()};
    }

    inline size_t size() const {
        return static_cast<const S*>(this)->size();
    }

    inline T operator[](const size_t index) const {
        return static_cast<const S*>(this)->operator[](index);
    }
};

class GeoLocationContainer
: public ScanContainer<GeoLocation, GeoLocationContainer> {
private:
    const MipasFile& file;

public:
    GeoLocationContainer(const MipasFile& file);
    size_t size() const;
    GeoLocation operator[](const size_t index) const;
};

class ScanInfoContainer
: public ScanContainer<ScanInfo, ScanInfoContainer> {
private:
    const MipasFile& file;
    std::unique_ptr<uint16_t[]> num_sweeps;
    std::unique_ptr<uint32_t[]> offset;

public:
    ScanInfoContainer(const MipasFile& file);
    size_t size() const;
    ScanInfo operator[](const size_t index) const;
};


class MipasFile {
private:
    friend class GeoLocationContainer;
    friend class ScanInfoContainer;
    std::unique_ptr<MipasFileImpl> impl;

public:
    MipasFile(const std::string& filename);
    ~MipasFile();
    const MipasHeader& get_header() const;
    SummaryQuality get_summary_quality() const;
    inline GeoLocationContainer get_geo_locations() const {
        return GeoLocationContainer{*this};
    };
    inline ScanInfoContainer get_scan_infos() const {
        return ScanInfoContainer{*this};
    }
};

}

#endif
