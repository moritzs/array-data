#ifndef _MIPAS_READERS_HPP
#define _MIPAS_READERS_HPP

#include <cassert>
#include <cstring>
#include <limits>
#include <string>
#include "mipas_types.hpp"
#include "utils.hpp"


namespace mipas {

class Reader {
protected:
    const char* data;
    const size_t base_offset;
    size_t offset;

public:
    Reader(const char* data, const size_t offset)
    : data(data), base_offset(offset), offset(offset) {}

    void advance(const size_t count) {
        offset += count;
    }

    size_t get_size() const {
        return offset - base_offset;
    }

    template <size_t N>
    utils::fixed_string<N> read_fixed_string() {
        advance(N);
        utils::fixed_string<N> s;
        std::memcpy(s.data(), data + offset - N, N);
        return s;
    }
};


class AsciiReader : public Reader {
public:
    using Reader::Reader;

    void expect_ascii(const std::string& str) {
        if (std::memcmp(str.c_str(), data + offset, str.length()) != 0) {
            throw unexpected_string{offset, str};
        }
        advance(str.length());
    }

    void expect_nl() {
        expect_ascii("\n");
    }

    std::string read_ascii(const size_t size) {
        advance(size);
        return std::string{data + offset - size, size};
    }

    char read_char(const size_t size) {
        advance(size);
        return data[offset - 1];
    }

    template <typename T>
    T read_int(const size_t size) {
        assert(size > 0);
        std::string str_number = read_ascii(size);
        size_t pos = 0;
        long long value = std::stoll(str_number, &pos);
        if (pos < str_number.length()) {
            throw parse_number_error{offset - size, "Invalid integer"};
        }
        if (value < std::numeric_limits<T>::min() || value > std::numeric_limits<T>::max()) {
            throw parse_number_error{offset - size, "Integer out ouf bounds"};
        }
        return static_cast<T>(value);
    }

    template <typename T>
    T read_float(const size_t size) {
        assert(size > 0);
        std::string str_number = read_ascii(size);
        size_t pos = 0;
        double value = std::stod(str_number, &pos);
        if (pos < str_number.length()) {
            throw parse_number_error{offset - size, "Invalid double"};
        }
        return static_cast<T>(value);
    }
};


class BinaryReader : public Reader {
public:
    using Reader::Reader;

    template <typename T>
    T read_int() {
        T value;
        std::memcpy(&value, data + offset, sizeof(T));
        offset += sizeof(T);
        utils::byteswap(value);
        return value;
    }

    template <typename T>
    T read_float() {
        alignas(T) typename utils::uint_type<sizeof(T)>::type int_value;
        std::memcpy(&int_value, data + offset, sizeof(T));
        offset += sizeof(T);
        utils::byteswap(int_value);
        T value;
        std::memcpy(&value, &int_value, sizeof(T));
        return value;
    }

    EnvisatTime read_envisat_time() {
        EnvisatTime value;
        value.days = read_int<int32_t>();
        value.seconds = read_int<uint32_t>();
        value.microseconds = read_int<uint32_t>();
        return value;
    }

    LocationPoint read_location_point() {
        LocationPoint value;
        value.latitude = read_int<int32_t>();
        value.longitude = read_int<int32_t>();
        return value;
    }
};

}

#endif
