#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#include <utility>
#include <vector>
#include <boost/geometry.hpp>
#include <boost/geometry/index/rtree.hpp>
#include "column_store.hpp"
#include "mipas.hpp"
#include "storage/storage_sparse_mipas_altitude.hpp"
#include "timer.hpp"
#include "utils.hpp"

#ifndef RTREE_VARIANT
#define RTREE_VARIANT quadratic
#endif


namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;


static constexpr uint32_t WKB_TYPE = 0x20000003;
static constexpr uint32_t SRID = 4326;
static const std::string WKB_TYPE_STR = utils::to_hex(WKB_TYPE);
static const std::string SRID_STR = utils::to_hex(SRID);

using mipas_store = column_store<mipas::EnvisatTime, int32_t, int32_t, double, float, float>;


template <typename T>
void print_grid(
    std::ostream& stream, const std::vector<T> grid,
    const size_t grid_x_res, const size_t grid_y_res
) {
    const auto x_res = static_cast<int64_t>(grid_x_res);
    const auto y_res = static_cast<int64_t>(grid_y_res);
    const float x_step = 360.0 / grid_x_res;
    const float x_half = x_step / 2;
    const float y_step = 180.0 / grid_y_res;
    const float y_half = y_step / 2;
    for (int64_t y = y_step; y < (y_res + 1) - y_step; ++y) {
        for (int64_t x = x_step; x < (x_res + 1) - x_step; ++x) {
            double longitude = (x - x_res / 2) * x_step;
            double latitude = (y - y_res / 2) * y_step;
            stream << "01" << WKB_TYPE_STR << SRID_STR;
            stream << utils::to_hex<uint32_t>(1);
            stream << utils::to_hex<uint32_t>(5);
            stream << utils::to_hex<double>(longitude - x_half);
            stream << utils::to_hex<double>(latitude  - y_half);
            stream << utils::to_hex<double>(longitude + x_half);
            stream << utils::to_hex<double>(latitude  - y_half);
            stream << utils::to_hex<double>(longitude + x_half);
            stream << utils::to_hex<double>(latitude  + y_half);
            stream << utils::to_hex<double>(longitude - x_half);
            stream << utils::to_hex<double>(latitude  + y_half);
            stream << utils::to_hex<double>(longitude - x_half);
            stream << utils::to_hex<double>(latitude  - y_half);
            stream << ';';
            stream << grid[x + y * (x_res + 1)] << std::endl;
        }
    }
}


void print_time_coordinates(
    std::ostream& stream,
    const std::vector<std::tuple<mipas::EnvisatTime, int32_t, int32_t>>& coordinates
) {
    for (const auto& coord : coordinates) {
        stream << std::get<0>(coord).to_iso() << ';';
        stream << utils::fixed_float(std::get<1>(coord)) << ';';
        stream << utils::fixed_float(std::get<2>(coord)) << std::endl;
    }
}


std::vector<float> max_temp_stratosphere(
    const mipas_store& data, const size_t grid_x_res, const size_t grid_y_res
) {
    static const auto date_from = mipas::EnvisatTime::from_date(2003, 1, 1, 0, 0, 0);
    static const auto date_to = mipas::EnvisatTime::from_date(2004, 1, 1, 0, 0, 0);
    const auto x_res = static_cast<int64_t>(grid_x_res);
    const auto y_res = static_cast<int64_t>(grid_y_res);
    std::vector<float> grid(
        (grid_x_res + 1) * (grid_y_res + 1),
        std::numeric_limits<float>::quiet_NaN()
    );
    const float x_step = 360.0 / x_res;
    const float y_step = 180.0 / y_res;
    for (size_t i = 0; i < data.size(); ++i) {
        const auto& date = data.get<0>(i);
        if (date < date_from || date >= date_to) {
            continue;
        }
        const double altitude = data.get<3>(i);
        if (altitude < 10 || altitude > 50) {
            continue;
        }
        const double latitude = data.get<1>(i) / 1000000.0;
        const double longitude = data.get<2>(i) / 1000000.0;
        int32_t x = std::lround(longitude / x_step) + x_res / 2;
        int32_t y = std::lround(latitude / y_step) + y_res / 2;
        size_t index = x + y * (x_res + 1);
        float temperature = data.get<5>(i);
        float old_value = grid[index];
        if (std::isnan(old_value) || temperature > old_value) {
            grid[index] = temperature;
        }
    }
    return grid;
}


inline double distance_measure(
    const double x_1, const double y_1, const double z_1,
    const double x_2, const double y_2, const double z_2
) {
    double x = x_2 - x_1;
    double y = y_2 - y_1;
    double z = z_2 - z_1;
    return std::sqrt(x * x + y * y + z * z);
}


std::vector<double> weighted_avg_pressure(
    const mipas_store& data, const size_t grid_x_res, const size_t grid_y_res
) {
    static const auto date_from = mipas::EnvisatTime::from_date(2003, 7, 1, 0, 0, 0);
    static const auto date_to = mipas::EnvisatTime::from_date(2003, 8, 1, 0, 0, 0);
    const auto x_res = static_cast<int64_t>(grid_x_res);
    const auto y_res = static_cast<int64_t>(grid_y_res);
    std::vector<double> sums((grid_x_res + 1) * (grid_y_res + 1), 0);
    std::vector<double> distances((grid_x_res + 1) * (grid_y_res + 1), 0);
    const float x_step = 360.0 / x_res;
    const float y_step = 180.0 / y_res;
    for (size_t i = 0; i < data.size(); ++i) {
        const auto& date = data.get<0>(i);
        if (date < date_from || date >= date_to) {
            continue;
        }
        const double altitude = data.get<3>(i);
        if (altitude < 15 || altitude > 25) {
            continue;
        }
        const double latitude = data.get<1>(i) / 1000000.0;
        const double longitude = data.get<2>(i) / 1000000.0;
        int32_t x = std::lround(longitude / x_step);
        int32_t y = std::lround(latitude / y_step);
        float pressure = data.get<4>(i);
        if (!std::isnan(pressure)) {
            for (int x_offset = -2; x_offset <= 2; ++x_offset) {
                for (int y_offset = -2; y_offset <= 2; ++y_offset) {
                    int32_t x_neighbour = x + x_offset;
                    int32_t y_neighbour = y + y_offset;
                    int32_t x_index = x_neighbour + x_res / 2;
                    int32_t y_index = y_neighbour + y_res / 2;
                    if (
                        x_index < 0 || x_index > x_res ||
                        y_index < 0 || y_index > y_res
                    ) {
                        continue;
                    }
                    double distance = 16 - distance_measure(
                        x_neighbour * x_step, y_neighbour * y_step, 20,
                        longitude, latitude, altitude
                    );
                    size_t index = x_index + y_index * (x_res + 1);
                    sums[index] += pressure * distance;
                    distances[index] += distance;
                }
            }
        }
    }
    for (size_t i = 0; i < sums.size(); ++i) {
        double distance = distances[i];
        if (distance > 0) {
            sums[i] /= distance;
        } else {
            sums[i] = std::numeric_limits<float>::quiet_NaN();
        }
    }
    return sums;
}


template <typename C>
class weighted_avg_pressure_generic {
public:
    using result_t = std::vector<std::vector<double>>;

private:
    const mipas_store& data;
    const size_t grid_x_res;
    const size_t grid_y_res;

    struct runtime_t {
        utils::timer timer;
        std::vector<mipas::EnvisatTime> month_boundaries;
        result_t month_sums;
        result_t month_distances;
        C storage;

        runtime_t() : timer("allocate data structures") {}
    };

    std::unique_ptr<runtime_t> runtime;

public:
    weighted_avg_pressure_generic(
        const mipas_store& data, const size_t grid_x_res, const size_t grid_y_res
    ) : data(data), grid_x_res(grid_x_res), grid_y_res(grid_y_res) {}

    result_t run() {
        runtime = std::make_unique<runtime_t>();
        runtime->month_boundaries.reserve(13);
        for (size_t i = 1; i <= 12; ++i) {
            runtime->month_boundaries.push_back(
                mipas::EnvisatTime::from_date(2003, i, 1, 0, 0, 0)
            );
        }
        runtime->month_boundaries.push_back(mipas::EnvisatTime::from_date(2004, 1, 1, 0, 0, 0));
        runtime->month_sums.resize(12);
        runtime->month_distances.resize(12);
        for (auto& grid : runtime->month_sums) {
            grid.resize((grid_x_res + 1) * (grid_y_res + 1), 0);
        }
        for (auto& grid : runtime->month_distances) {
            grid.resize((grid_x_res + 1) * (grid_y_res + 1), 0);
        }
        runtime->timer.replace("load data into storage");
        runtime->storage = C{data};
        runtime->timer.replace("calculate sums and distances");
        for (size_t month = 1; month <= 12; ++month) {
            calculate_sums(month);
        }
        runtime->timer.replace("divide sums by distance");
        for (size_t month = 1; month <= 12; ++month) {
            divide_sums(month);
        }
        result_t result{std::move(runtime->month_sums)};
        runtime->timer.replace("destruct data structures");
        runtime.reset(nullptr);
        return result;
    }

private:
    void calculate_sums(const size_t month) {
        const auto x_res = static_cast<int64_t>(grid_x_res);
        const auto y_res = static_cast<int64_t>(grid_y_res);
        const float x_step = 360.0 / x_res;
        const float y_step = 180.0 / y_res;
        const auto& date_from = runtime->month_boundaries[month - 1];
        const auto& date_to = runtime->month_boundaries[month];
        auto& sums = runtime->month_sums[month - 1];
        auto& distances = runtime->month_distances[month - 1];
        auto date_range_it = runtime->storage.find_greater_eq(date_from);
        auto date_end_it = runtime->storage.end();
        for (; date_range_it != date_end_it && date_range_it->min() < date_to; ++date_range_it) {
            auto altitude_range_it = date_range_it->find_greater_eq(15);
            auto altitude_end_it = date_range_it->end();
            for (;
                altitude_range_it != altitude_end_it && altitude_range_it->min() <= 25;
                ++altitude_range_it
            ) {
                auto link_it = altitude_range_it->link_begin();
                auto link_end = altitude_range_it->link_end();
                for (; link_it != link_end; ++link_it) {
                    auto ref = *link_it;
                    const auto& date = ref.template get<2>();
                    if (date < date_from || date >= date_to) {
                        continue;
                    }
                    const double altitude = ref.template get<3>();
                    if (altitude < 15 || altitude > 25) {
                        continue;
                    }
                    const double latitude = ref.template get<4>() / 1000000.0;
                    const double longitude = ref.template get<5>() / 1000000.0;
                    int32_t x = std::lround(longitude / x_step);
                    int32_t y = std::lround(latitude / y_step);
                    float pressure = ref.template get<0>();
                    if (!std::isnan(pressure)) {
                        for (int x_offset = -2; x_offset <= 2; ++x_offset) {
                            for (int y_offset = -2; y_offset <= 2; ++y_offset) {
                                int32_t x_neighbour = x + x_offset;
                                int32_t y_neighbour = y + y_offset;
                                int32_t x_index = x_neighbour + x_res / 2;
                                int32_t y_index = y_neighbour + y_res / 2;
                                if (
                                    x_index < 0 || x_index > x_res ||
                                    y_index < 0 || y_index > y_res
                                ) {
                                    continue;
                                }
                                double distance = 16 - distance_measure(
                                    x_neighbour * x_step, y_neighbour * y_step, 20,
                                    longitude, latitude, altitude
                                );
                                size_t index = x_index + y_index * (grid_x_res + 1);
                                sums[index] += pressure * distance;
                                distances[index] += distance;
                            }
                        }
                    }
                }
            }
        }
    }

    void divide_sums(const size_t month) {
        auto& sums = runtime->month_sums[month - 1];
        auto& distances = runtime->month_distances[month - 1];
        for (size_t i = 0; i < sums.size(); ++i) {
            sums[i] /= distances[i];
        }
    }
};


class weighted_avg_pressure_rtree {
public:
    using result_t = std::vector<std::vector<double>>;

private:
    using point_t = bg::model::point<int64_t, 4, bg::cs::cartesian>;
    using box_t = bg::model::box<point_t>;
    using value_t = std::pair<point_t, float>;
    using rtree_t = bgi::rtree<value_t, bgi::RTREE_VARIANT<16>>;

    const mipas_store& data;
    const size_t grid_x_res;
    const size_t grid_y_res;

    struct runtime_t {
        utils::timer timer;
        std::vector<mipas::EnvisatTime> month_boundaries;
        result_t month_sums;
        result_t month_distances;
        rtree_t rtree;

        runtime_t() : timer("allocate data structures") {}
    };

    std::unique_ptr<runtime_t> runtime;

public:
    weighted_avg_pressure_rtree(
        const mipas_store& data, const size_t grid_x_res, const size_t grid_y_res
    ) : data(data), grid_x_res(grid_x_res), grid_y_res(grid_y_res) {}

    result_t run() {
        runtime = std::make_unique<runtime_t>();
        runtime->month_boundaries.reserve(13);
        for (size_t i = 1; i <= 12; ++i) {
            runtime->month_boundaries.push_back(
                mipas::EnvisatTime::from_date(2003, i, 1, 0, 0, 0)
            );
        }
        runtime->month_boundaries.push_back(mipas::EnvisatTime::from_date(2004, 1, 1, 0, 0, 0));
        runtime->month_sums.resize(12);
        runtime->month_distances.resize(12);
        for (auto& grid : runtime->month_sums) {
            grid.resize((grid_x_res + 1) * (grid_y_res + 1), 0);
        }
        for (auto& grid : runtime->month_distances) {
            grid.resize((grid_x_res + 1) * (grid_y_res + 1), 0);
        }
        runtime->timer.replace("load data into rtree");
        load_rtree();
        runtime->timer.replace("calculate sums and distances");
        for (size_t month = 1; month <= 12; ++month) {
            calculate_sums(month);
        }
        runtime->timer.replace("divide sums by distance");
        for (size_t month = 1; month <= 12; ++month) {
            divide_sums(month);
        }
        result_t result{std::move(runtime->month_sums)};
        runtime->timer.replace("destruct data structures");
        runtime.reset(nullptr);
        return result;
    }

private:
    static point_t make_point(
        const mipas::EnvisatTime& time, const double altitude,
        const int32_t latitude, const int32_t longitude
    ) {
        point_t point;
        bg::set<0>(point, time.as_int());
        bg::set<1>(point, altitude * 100000);
        bg::set<2>(point, latitude);
        bg::set<3>(point, longitude);
        return point;
    }

    void load_rtree() {
        auto* col_timestamp = data.get<0>();
        auto* col_altitude = data.get<3>();
        auto* col_latitude = data.get<1>();
        auto* col_longitude = data.get<2>();
        auto* col_pressure = data.get<4>();
        for (size_t i = 0; i < data.size(); ++i) {
            runtime->rtree.insert(
                std::make_pair(
                    make_point(
                        col_timestamp[i],
                        col_altitude[i],
                        col_latitude[i],
                        col_longitude[i]
                    ),
                    col_pressure[i]
                )
            );
        }
    }

    void calculate_sums(const size_t month) {
        const auto x_res = static_cast<int64_t>(grid_x_res);
        const auto y_res = static_cast<int64_t>(grid_y_res);
        const float x_step = 360.0 / x_res;
        const float y_step = 180.0 / y_res;
        const auto& date_from = runtime->month_boundaries[month - 1];
        const auto& date_to = runtime->month_boundaries[month];
        auto& sums = runtime->month_sums[month - 1];
        auto& distances = runtime->month_distances[month - 1];
        box_t search_box(
            make_point(date_from, 15, -90000000, -180000000),
            make_point(date_to, 25, 90000000, 180000000)
        );
        for (
            auto it = runtime->rtree.qbegin(bgi::intersects(search_box));
            it != runtime->rtree.qend();
            ++it
        ) {
            auto point = it->first;
            const double altitude = bg::get<1>(point) / 100000.0;
            const double latitude = bg::get<2>(point) / 1000000.0;
            const double longitude = bg::get<3>(point) / 1000000.0;
            int32_t x = std::lround(longitude / x_step);
            int32_t y = std::lround(latitude / y_step);
            float pressure = it->second;
            if (!std::isnan(pressure)) {
                for (int x_offset = -2; x_offset <= 2; ++x_offset) {
                    for (int y_offset = -2; y_offset <= 2; ++y_offset) {
                        int32_t x_neighbour = x + x_offset;
                        int32_t y_neighbour = y + y_offset;
                        int32_t x_index = x_neighbour + x_res / 2;
                        int32_t y_index = y_neighbour + y_res / 2;
                        if (
                            x_index < 0 || x_index > x_res ||
                            y_index < 0 || y_index > y_res
                        ) {
                            continue;
                        }
                        double distance = 16 - distance_measure(
                            x_neighbour * x_step, y_neighbour * y_step, 20,
                            longitude, latitude, altitude
                        );
                        size_t index = x_index + y_index * (grid_x_res + 1);
                        sums[index] += pressure * distance;
                        distances[index] += distance;
                    }
                }
            }
        }
    }

    void divide_sums(const size_t month) {
        auto& sums = runtime->month_sums[month - 1];
        auto& distances = runtime->month_distances[month - 1];
        for (size_t i = 0; i < sums.size(); ++i) {
            sums[i] /= distances[i];
        }
    }
};


template <typename C>
class satellite_path_hourly_generic {
public:
    using result_tuple_t = std::tuple<mipas::EnvisatTime, int32_t, int32_t>;
    using result_t = std::vector<result_tuple_t>;

private:
    const mipas_store& data;

    struct runtime_t {
        utils::timer timer;
        result_t result;
        C storage;

        runtime_t() : timer("allocate data structures") {}
    };

    std::unique_ptr<runtime_t> runtime;

public:
    satellite_path_hourly_generic(const mipas_store& data) : data(data) {}

    result_t run() {
        runtime = std::make_unique<runtime_t>();
        std::vector<mipas::EnvisatTime> start_hours;
        start_hours.reserve(61 * 24 + 1);
        result_t result;
        result.reserve(61 * 24);
        for (size_t day = 1; day <= 31; ++day) {
            for (size_t hour = 0; hour < 24; ++hour) {
                start_hours.push_back(mipas::EnvisatTime::from_date(2003, 3, day, hour, 0, 0));
            }
        }
        for (size_t day = 1; day <= 30; ++day) {
            for (size_t hour = 0; hour < 24; ++hour) {
                start_hours.push_back(mipas::EnvisatTime::from_date(2003, 4, day, hour, 0, 0));
            }
        }
        start_hours.push_back(mipas::EnvisatTime::from_date(2003, 5, 1, 0, 0, 0));
        runtime->timer.replace("load data into storage");
        runtime->storage = C{data};
        runtime->timer.replace("get satellite locations");
        for (size_t i = 0; i < start_hours.size() - 1; ++i) {
            result_tuple_t result_tuple = get_satellite_location(
                start_hours[i], start_hours[i + 1]
            );
            if (
                std::get<0>(result_tuple) != mipas::EnvisatTime::max() && (
                    result.empty() ||
                    std::get<0>(result_tuple) > std::get<0>(result.back())
                )
            ) {
                result.push_back(result_tuple);
            }
        }
        runtime->timer.replace("destruct data structures");
        runtime.reset(nullptr);
        return result;
    }

private:
    result_tuple_t get_satellite_location(
        const mipas::EnvisatTime& start_hour,
        const mipas::EnvisatTime&
    ) {
        constexpr int32_t max_int = std::numeric_limits<int32_t>::max();
        auto date_range_it = runtime->storage.find_greater_eq(start_hour);
        result_tuple_t result{mipas::EnvisatTime::max(), max_int, max_int};
        if (date_range_it == runtime->storage.end()) {
            return result;
        }
        auto link_it = date_range_it->link_begin();
        auto link_end = date_range_it->link_end();
        for (; link_it != link_end; ++link_it) {
            auto ref = *link_it;
            const auto& date = ref.template get<2>();
            if (date >= start_hour && date < std::get<0>(result)) {
                std::get<0>(result) = date;
                std::get<1>(result) = ref.template get<4>();
                std::get<2>(result) = ref.template get<5>();
            }
        }
        return result;
    }
};


class satellite_path_hourly_rtree {
public:
    using result_tuple_t = std::tuple<mipas::EnvisatTime, int32_t, int32_t>;
    using result_t = std::vector<result_tuple_t>;

private:
    using point_t = bg::model::point<int64_t, 4, bg::cs::cartesian>;
    using box_t = bg::model::box<point_t>;
    using value_t = std::pair<point_t, float>;
    using rtree_t = bgi::rtree<value_t, bgi::RTREE_VARIANT<16>>;

    const mipas_store& data;

    struct runtime_t {
        utils::timer timer;
        result_t result;
        rtree_t rtree;

        runtime_t() : timer("allocate data structures") {}
    };

    std::unique_ptr<runtime_t> runtime;

public:
    satellite_path_hourly_rtree(const mipas_store& data) : data(data) {}

    result_t run() {
        runtime = std::make_unique<runtime_t>();
        std::vector<mipas::EnvisatTime> start_hours;
        start_hours.reserve(61 * 24 + 1);
        result_t result;
        result.reserve(61 * 24);
        for (size_t day = 1; day <= 31; ++day) {
            for (size_t hour = 0; hour < 24; ++hour) {
                start_hours.push_back(mipas::EnvisatTime::from_date(2003, 3, day, hour, 0, 0));
            }
        }
        for (size_t day = 1; day <= 30; ++day) {
            for (size_t hour = 0; hour < 24; ++hour) {
                start_hours.push_back(mipas::EnvisatTime::from_date(2003, 4, day, hour, 0, 0));
            }
        }
        start_hours.push_back(mipas::EnvisatTime::from_date(2003, 5, 1, 0, 0, 0));
        runtime->timer.replace("load data into rtree");
        load_rtree();
        runtime->timer.replace("get satellite locations");
        for (size_t i = 0; i < start_hours.size() - 1; ++i) {
            result_tuple_t result_tuple = get_satellite_location(
                start_hours[i], start_hours[i + 1]
            );
            if (
                std::get<0>(result_tuple) != mipas::EnvisatTime::max() && (
                    result.empty() ||
                    std::get<0>(result_tuple) > std::get<0>(result.back())
                )
            ) {
                result.push_back(result_tuple);
            }
        }
        runtime->timer.replace("destruct data structures");
        runtime.reset(nullptr);
        return result;
    }

private:
    static point_t make_point(
        const mipas::EnvisatTime& time, const double altitude,
        const int32_t latitude, const int32_t longitude
    ) {
        point_t point;
        bg::set<0>(point, time.as_int());
        bg::set<1>(point, altitude * 100000);
        bg::set<2>(point, latitude);
        bg::set<3>(point, longitude);
        return point;
    }

    void load_rtree() {
        auto* col_timestamp = data.get<0>();
        auto* col_altitude = data.get<3>();
        auto* col_latitude = data.get<1>();
        auto* col_longitude = data.get<2>();
        auto* col_pressure = data.get<4>();
        for (size_t i = 0; i < data.size(); ++i) {
            runtime->rtree.insert(
                std::make_pair(
                    make_point(
                        col_timestamp[i],
                        col_altitude[i],
                        col_latitude[i],
                        col_longitude[i]
                    ),
                    col_pressure[i]
                )
            );
        }
    }

    result_tuple_t get_satellite_location(
        const mipas::EnvisatTime& start_hour,
        const mipas::EnvisatTime& end_hour
    ) {
        box_t search_box(
            make_point(start_hour, 0, -90000000, -180000000),
            make_point(end_hour, 200, 90000000, 180000000)
        );
        constexpr int32_t max_int = std::numeric_limits<int32_t>::max();
        result_tuple_t result{mipas::EnvisatTime::max(), max_int, max_int};
        for (
            auto it = runtime->rtree.qbegin(bgi::covered_by(search_box));
            it != runtime->rtree.qend();
            ++it
        ) {
            auto point = it->first;
            int64_t date_int = bg::get<0>(point);
            auto date = mipas::EnvisatTime::from_int(date_int);
            if (date < std::get<0>(result)) {
                std::get<0>(result) = date;
                std::get<1>(result) = bg::get<2>(point);
                std::get<2>(result) = bg::get<3>(point);
            }
        }
        return result;
    }
};


int main(int argc, char** argv) {
    if (!(argc == 3 || argc == 4)) {
        std::cerr << "Usage: " << argv[0] << " [--shuffle] db-file query" << std::endl;
        std::cerr << "  Available queries: max_temp_stratosphere weighted_avg_pressure ";
        std::cerr << std::endl;
        std::cerr << "                     weighted_avg_pressure_sparse_storage" << std::endl;
        std::cerr << "                     weighted_avg_pressure_rtree" << std::endl;
        std::cerr << "                     satellite_path_hourly_sparse_storage" << std::endl;
        std::cerr << "                     satellite_path_hourly_rtree" << std::endl;
        std::cerr << "                     insert_only" << std::endl;
        return 2;
    }
    std::string db_file{argv[1]};
    std::string query{argv[2]};
    bool shuffle = false;
    if (argc == 4) {
        if (db_file == "--shuffle") {
            shuffle = true;
            db_file = std::move(query);
            query = std::string{argv[3]};
        } else {
            std::cerr << "Expected option, got \"" << db_file << '"' << std::endl;
            return 2;
        }
    }
    mipas_store data;
    {
        utils::timer t{"load database"};
        data.load_from_file(db_file);
    }
    if (shuffle) {
        utils::timer t{"shuffle data"};
        mipas_store old_data(data);
        std::vector<size_t> indexes(old_data.size());
        for (size_t i = 0; i < indexes.size(); ++i) {
            indexes[i] = i;
        }
        std::mt19937 rand_gen{0};
        std::shuffle(indexes.begin(), indexes.end(), rand_gen);
        for (size_t i = 0; i < indexes.size(); ++i) {
            data[i] = old_data[indexes[i]];
        }
    }
    if (query == "max_temp_stratosphere") {
        std::vector<float> grid;
        {
            utils::timer t;
            grid = max_temp_stratosphere(data, 180, 90);
        }
        print_grid(std::cout, grid, 180, 90);
    } else if (query == "weighted_avg_pressure") {
        std::vector<double> grid;
        {
            utils::timer t;
            grid = weighted_avg_pressure(data, 180, 90);
        }
        print_grid(std::cout, grid, 180, 90);
    } else if (query == "weighted_avg_pressure_sparse_storage") {
        std::vector<std::vector<double>> result;
        {
            utils::timer t;
            result = weighted_avg_pressure_generic<storage_sparse_mipas_altitude>(data, 180, 90) \
                .run();
        }
        print_grid(std::cout, result[6], 180, 90);
    } else if (query == "weighted_avg_pressure_rtree") {
        std::vector<std::vector<double>> result;
        {
            utils::timer t;
            result = weighted_avg_pressure_rtree(data, 180, 90).run();
        }
        print_grid(std::cout, result[6], 180, 90);
    } else if (query == "satellite_path_hourly_sparse_storage") {
        std::vector<std::tuple<mipas::EnvisatTime, int32_t, int32_t>> result;
        {
            utils::timer t;
            result = satellite_path_hourly_generic<storage_sparse_mipas_altitude>(data).run();
        }
        print_time_coordinates(std::cout, result);
    } else if (query == "satellite_path_hourly_rtree") {
        std::vector<std::tuple<mipas::EnvisatTime, int32_t, int32_t>> result;
        {
            utils::timer t;
            result = satellite_path_hourly_rtree(data).run();
        }
        print_time_coordinates(std::cout, result);
    } else if (query == "insert_only") {
        {
            utils::timer t;
            storage_sparse_mipas_altitude s{data};
        }
    } else {
        std::cerr << "invalid query" << std::endl;
        return 2;
    }
    return 0;
}
