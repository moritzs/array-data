#include "mipas.hpp"
#include <memory>
#include <ostream>
#include <string>
#include <utility>
#include "exceptions.hpp"
#include "mmap.hpp"
#include "readers.hpp"
#include "utils.hpp"


namespace mipas {

inline void mipas_assert(const bool cond, const char* message) {
    if (!cond) {
        throw file_incosistency{message};
    }
}


class Parser {
private:
    friend class MipasFile;
    friend class GeoLocationContainer;
    friend class ScanInfoContainer;

    Mmap mmap;
    std::unique_ptr<MipasHeader> header;

public:
    Parser(const std::string& file) {
        mmap.map_file(file, MPH_SIZE + SPH_SIZE);
        parse_header();
    }

    const MipasHeader& get_header() const {
        return *header;
    }

private:
    AsciiReader get_ascii_reader(const size_t offset) const {
        return AsciiReader{mmap.data(), offset};
    }

    BinaryReader get_binary_reader(const size_t offset) const {
        return BinaryReader{mmap.data(), offset};
    }

    void parse_mph(const size_t offset, MPH& mph) const {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MPH.html
        AsciiReader r = get_ascii_reader(offset);
        /*  id */
        /*   0 */ r.expect_ascii("PRODUCT=");
        /*   1 */ r.expect_ascii("\"");
        /*   2 */ mph.product = r.read_ascii(62);
        /*   3 */ r.expect_ascii("\"");
        /*   4 */ r.expect_nl();
        /*   5 */ r.expect_ascii("PROC_STAGE=");
        /*   6 */ mph.proc_stage = r.read_ascii(1);
        /*   7 */ r.expect_nl();
        /*   8 */ r.expect_ascii("REF_DOC=");
        /*   9 */ r.expect_ascii("\"");
        /*  10 */ mph.ref_doc = r.read_ascii(23);
        /*  11 */ r.expect_ascii("\"");
        /*  12 */ r.expect_nl();
        /*  13 */ r.advance(40);
        /*  14 */ r.expect_nl();
        /*  15 */ r.expect_ascii("ACQUISITION_STATION=");
        /*  16 */ r.expect_ascii("\"");
        /*  17 */ mph.acquisition_station = r.read_ascii(20);
        /*  18 */ r.expect_ascii("\"");
        /*  19 */ r.expect_nl();
        /*  20 */ r.expect_ascii("PROC_CENTER=");
        /*  21 */ r.expect_ascii("\"");
        /*  22 */ mph.proc_center = r.read_ascii(6);
        /*  23 */ r.expect_ascii("\"");
        /*  24 */ r.expect_nl();
        /*  25 */ r.expect_ascii("PROC_TIME=");
        /*  26 */ r.expect_ascii("\"");
        /*  27 */ mph.proc_time = r.read_ascii(27);
        /*  28 */ r.expect_ascii("\"");
        /*  29 */ r.expect_nl();
        /*  30 */ r.expect_ascii("SOFTWARE_VER=");
        /*  31 */ r.expect_ascii("\"");
        /*  32 */ mph.software_ver = r.read_ascii(14);
        /*  33 */ r.expect_ascii("\"");
        /*  34 */ r.expect_nl();
        /*  35 */ r.advance(40);
        /*  36 */ r.expect_nl();
        /*  37 */ r.expect_ascii("SENSING_START=");
        /*  38 */ r.expect_ascii("\"");
        /*  39 */ mph.sensing_start = r.read_ascii(27);
        /*  40 */ r.expect_ascii("\"");
        /*  41 */ r.expect_nl();
        /*  42 */ r.expect_ascii("SENSING_STOP=");
        /*  43 */ r.expect_ascii("\"");
        /*  44 */ mph.sensing_stop = r.read_ascii(27);
        /*  45 */ r.expect_ascii("\"");
        /*  46 */ r.expect_nl();
        /*  47 */ r.advance(40);
        /*  48 */ r.expect_nl();
        /*  49 */ r.expect_ascii("PHASE=");
        /*  50 */ mph.phase = r.read_ascii(1);
        /*  51 */ r.expect_nl();
        /*  52 */ r.expect_ascii("CYCLE=");
        /*  53 */ mph.cycle = r.read_int<uint8_t>(4);
        /*  54 */ r.expect_nl();
        /*  55 */ r.expect_ascii("REL_ORBIT=");
        /*  56 */ mph.rel_orbit = r.read_int<int16_t>(6);
        /*  57 */ r.expect_nl();
        /*  58 */ r.expect_ascii("ABS_ORBIT=");
        /*  59 */ mph.abs_orbit = r.read_int<int32_t>(6);
        /*  60 */ r.expect_nl();
        /*  61 */ r.expect_ascii("STATE_VECTOR_TIME=");
        /*  62 */ r.expect_ascii("\"");
        /*  63 */ mph.state_vector_time = r.read_ascii(27);
        /*  64 */ r.expect_ascii("\"");
        /*  65 */ r.expect_nl();
        /*  66 */ r.expect_ascii("DELTA_UT1=");
        /*  67 */ mph.delta_ut1 = r.read_float<double>(8);
        /*  68 */ r.expect_ascii("<s>");
        /*  69 */ r.expect_nl();
        /*  70 */ r.expect_ascii("X_POSITION=");
        /*  71 */ mph.x_position = r.read_float<double>(12);
        /*  72 */ r.expect_ascii("<m>");
        /*  73 */ r.expect_nl();
        /*  74 */ r.expect_ascii("Y_POSITION=");
        /*  75 */ mph.y_position = r.read_float<double>(12);
        /*  76 */ r.expect_ascii("<m>");
        /*  77 */ r.expect_nl();
        /*  78 */ r.expect_ascii("Z_POSITION=");
        /*  79 */ mph.z_position = r.read_float<double>(12);
        /*  80 */ r.expect_ascii("<m>");
        /*  81 */ r.expect_nl();
        /*  82 */ r.expect_ascii("X_VELOCITY=");
        /*  83 */ mph.x_velocity = r.read_float<double>(12);
        /*  84 */ r.expect_ascii("<m/s>");
        /*  85 */ r.expect_nl();
        /*  86 */ r.expect_ascii("Y_VELOCITY=");
        /*  87 */ mph.y_velocity = r.read_float<double>(12);
        /*  88 */ r.expect_ascii("<m/s>");
        /*  89 */ r.expect_nl();
        /*  90 */ r.expect_ascii("Z_VELOCITY=");
        /*  91 */ mph.z_velocity = r.read_float<double>(12);
        /*  92 */ r.expect_ascii("<m/s>");
        /*  93 */ r.expect_nl();
        /*  94 */ r.expect_ascii("VECTOR_SOURCE=");
        /*  95 */ r.expect_ascii("\"");
        /*  96 */ mph.vector_source = r.read_ascii(2);
        /*  97 */ r.expect_ascii("\"");
        /*  98 */ r.expect_nl();
        /*  99 */ r.advance(40);
        /* 100 */ r.expect_nl();
        /* 101 */ r.expect_ascii("UTC_SBT_TIME=");
        /* 102 */ r.expect_ascii("\"");
        /* 103 */ mph.utc_sbt_time = r.read_ascii(27);
        /* 104 */ r.expect_ascii("\"");
        /* 105 */ r.expect_nl();
        /* 106 */ r.expect_ascii("SAT_BINARY_TIME=");
        /* 107 */ mph.sat_binary_time = r.read_int<uint32_t>(11);
        /* 108 */ r.expect_nl();
        /* 109 */ r.expect_ascii("CLOCK_STEP=");
        /* 110 */ mph.clock_step = r.read_int<uint32_t>(11);
        /* 111 */ r.expect_ascii("<ps>");
        /* 112 */ r.expect_nl();
        /* 113 */ r.advance(32);
        /* 114 */ r.expect_nl();
        /* 115 */ r.expect_ascii("LEAP_UTC=");
        /* 116 */ r.expect_ascii("\"");
        /* 117 */ mph.leap_utc = r.read_ascii(27);
        /* 118 */ r.expect_ascii("\"");
        /* 119 */ r.expect_nl();
        /* 120 */ r.expect_ascii("LEAP_SIGN=");
        /* 121 */ mph.leap_sign = r.read_int<int8_t>(4);
        /* 122 */ r.expect_nl();
        /* 123 */ r.expect_ascii("LEAP_ERR=");
        /* 124 */ mph.leap_err = r.read_int<int32_t>(1);
        /* 125 */ r.expect_nl();
        /* 126 */ r.advance(40);
        /* 127 */ r.expect_nl();
        /* 128 */ r.expect_ascii("PRODUCT_ERR=");
        /* 129 */ mph.product_err = r.read_int<int32_t>(1);
        /* 130 */ r.expect_nl();
        /* 131 */ r.expect_ascii("TOT_SIZE=");
        /* 132 */ mph.tot_size = r.read_int<int64_t>(21);
        /* 133 */ r.expect_ascii("<bytes>");
        /* 134 */ r.expect_nl();
        /* 135 */ r.expect_ascii("SPH_SIZE=");
        /* 136 */ mph.sph_size = r.read_int<int32_t>(11);
        /* 137 */ r.expect_ascii("<bytes>");
        /* 138 */ r.expect_nl();
        /* 139 */ r.expect_ascii("NUM_DSD=");
        /* 140 */ mph.num_dsd = r.read_int<int32_t>(11);
        /* 141 */ r.expect_nl();
        /* 142 */ r.expect_ascii("DSD_SIZE=");
        /* 143 */ mph.dsd_size = r.read_int<int32_t>(11);
        /* 144 */ r.expect_ascii("<bytes>");
        /* 145 */ r.expect_nl();
        /* 146 */ r.expect_ascii("NUM_DATA_SETS=");
        /* 147 */ mph.num_data_sets = r.read_int<int32_t>(11);
        /* 148 */ r.expect_nl();
        /* 149 */ r.advance(40);
        /* 150 */ r.expect_nl();
        mipas_assert(r.get_size() == MPH_SIZE, "MPH size");
    }

    void parse_sph(const size_t offset, SPH& sph) {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MIP_NL__2P_SPH_v2.html
        AsciiReader r = get_ascii_reader(offset);
        /* id */
        /*  0 */ r.expect_ascii("SPH_DESCRIPTOR=");
        /*  1 */ r.expect_ascii("\"");
        /*  2 */ sph.sph_descriptor = r.read_ascii(28);
        /*  3 */ r.expect_ascii("\"");
        /*  4 */ r.expect_nl();
        /*  5 */ r.expect_ascii("STRIPLINE_CONTINUITY_INDICATOR=");
        /*  6 */ sph.stripline_continuity_indicator = r.read_int<uint8_t>(4);
        /*  7 */ r.expect_nl();
        /*  8 */ r.expect_ascii("SLICE_POSITION=");
        /*  9 */ sph.slice_position = r.read_int<uint8_t>(4);
        /* 10 */ r.expect_nl();
        /* 11 */ r.expect_ascii("NUM_SLICES=");
        /* 12 */ sph.num_slices = r.read_int<uint8_t>(4);
        /* 13 */ r.expect_nl();
        /* 14 */ r.expect_ascii("START_TIME=");
        /* 15 */ r.expect_ascii("\"");
        /* 16 */ sph.start_time = r.read_ascii(27);
        /* 17 */ r.expect_ascii("\"");
        /* 18 */ r.expect_nl();
        /* 19 */ r.expect_ascii("STOP_TIME=");
        /* 20 */ r.expect_ascii("\"");
        /* 21 */ sph.stop_time = r.read_ascii(27);
        /* 22 */ r.expect_ascii("\"");
        /* 23 */ r.expect_nl();
        /* 24 */ r.expect_ascii("FIRST_TANGENT_LAT=");
        /* 25 */ sph.first_tangent_lat = r.read_int<int32_t>(11);
        /* 26 */ r.expect_ascii("<10-6degN>");
        /* 27 */ r.expect_nl();
        /* 28 */ r.expect_ascii("FIRST_TANGENT_LONG=");
        /* 29 */ sph.first_tangent_long = r.read_int<int32_t>(11);
        /* 30 */ r.expect_ascii("<10-6degE>");
        /* 31 */ r.expect_nl();
        /* 32 */ r.expect_ascii("LAST_TANGENT_LAT=");
        /* 33 */ sph.last_tangent_lat = r.read_int<int32_t>(11);
        /* 34 */ r.expect_ascii("<10-6degN>");
        /* 35 */ r.expect_nl();
        /* 36 */ r.expect_ascii("LAST_TANGENT_LONG=");
        /* 37 */ sph.last_tangent_long = r.read_int<int32_t>(11);
        /* 38 */ r.expect_ascii("<10-6degE>");
        /* 39 */ r.expect_nl();
        /* 40 */ r.advance(48);
        /* 41 */ r.expect_nl();
        /* 42 */ r.expect_ascii("NUM_SCANS=");
        /* 43 */ sph.num_scans = r.read_int<int16_t>(6);
        /* 44 */ r.expect_nl();
        /* 45 */ r.expect_ascii("NUM_LOS_GEOMS=");
        /* 46 */ sph.num_los_geoms = r.read_int<int16_t>(6);
        /* 47 */ r.expect_nl();
        /* 48 */ r.expect_ascii("NUM_SCANS_PER_DS=");
        /* 49 */ sph.num_scans_per_ds = r.read_int<int16_t>(6);
        /* 50 */ r.expect_nl();
        /* 51 */ r.expect_ascii("NUM_SCANS_PROC=");
        /* 52 */ sph.num_scans_proc = r.read_int<int16_t>(6);
        /* 53 */ r.expect_nl();
        /* 54 */ r.expect_ascii("NUM_SP_NOT_PROC=");
        /* 55 */ sph.num_sp_not_proc = r.read_int<int16_t>(6);
        /* 56 */ r.expect_nl();
        /* 57 */ r.expect_ascii("NUM_SPECTRA=");
        /* 58 */ sph.num_spectra = r.read_int<int16_t>(6);
        /* 59 */ r.expect_nl();
        /* 60 */ r.expect_ascii("NUM_SPECTR_PROC=");
        /* 61 */ sph.num_spectr_proc = r.read_int<int16_t>(6);
        /* 62 */ r.expect_nl();
        /* 63 */ r.expect_ascii("NUM_GAIN_CAL=");
        /* 64 */ sph.num_gain_cal = r.read_int<int16_t>(6);
        /* 65 */ r.expect_nl();
        /* 66 */ r.expect_ascii("TOT_GRANULES=");
        /* 67 */ sph.tot_granules = r.read_int<int16_t>(6);
        /* 68 */ r.expect_nl();
        /* 69 */ r.expect_ascii("MAX_PATH_DIFF=");
        /* 70 */ sph.max_path_diff = r.read_float<float>(15);
        /* 71 */ r.expect_ascii("<cm>");
        /* 72 */ r.expect_nl();
        /* 73 */ r.expect_ascii("ORDER_OF_SPECIES=");
        /* 74 */ r.expect_ascii("\"");
        /* 75 */ sph.order_of_species = r.read_ascii(149);
        /* 76 */ r.expect_ascii("\"");
        /* 77 */ r.expect_nl();
        /* 78 */ r.expect_ascii("NUM_SWEEPS_PER_SCAN=");
        /* 79 */ sph.num_sweeps_per_scan = r.read_int<int16_t>(6);
        /* 80 */ r.expect_nl();
        /* 81 */ r.advance(20);
        /* 82 */ r.expect_nl();
        mipas_assert(r.get_size() == SPH_SIZE, "SPH size");
    }

    void parse_dsd(const size_t offset, DSD& dsd) {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/DSD.html
        AsciiReader r = get_ascii_reader(offset);
        /* id */
        /*  0 */ r.expect_ascii("DS_NAME=");
        /*  1 */ r.expect_ascii("\"");
        /*  2 */ dsd.ds_name = r.read_ascii(28);
        /*  3 */ r.expect_ascii("\"");
        /*  4 */ r.expect_nl();
        /*  5 */ r.expect_ascii("DS_TYPE=");
        /*  6 */ dsd.ds_type = r.read_char(1);
        /*  7 */ r.expect_nl();
        /*  8 */ r.expect_ascii("FILENAME=");
        /*  9 */ r.expect_ascii("\"");
        /* 10 */ dsd.filename = r.read_ascii(62);
        /* 11 */ r.expect_ascii("\"");
        /* 12 */ r.expect_nl();
        /* 13 */ r.expect_ascii("DS_OFFSET=");
        /* 14 */ dsd.ds_offset = r.read_int<int64_t>(21);
        /* 15 */ r.expect_ascii("<bytes>");
        /* 16 */ r.expect_nl();
        /* 17 */ r.expect_ascii("DS_SIZE=");
        /* 18 */ dsd.ds_size = r.read_int<int64_t>(21);
        /* 19 */ r.expect_ascii("<bytes>");
        /* 20 */ r.expect_nl();
        /* 21 */ r.expect_ascii("NUM_DSR=");
        /* 22 */ dsd.num_dsr = r.read_int<int32_t>(11);
        /* 23 */ r.expect_nl();
        /* 24 */ r.expect_ascii("DSR_SIZE=");
        /* 25 */ dsd.dsr_size = r.read_int<int32_t>(11);
        /* 26 */ r.expect_ascii("<bytes>");
        /* 27 */ r.expect_nl();
        /* 28 */ r.advance(32);
        /* 29 */ r.expect_nl();
        mipas_assert(r.get_size() == DSD_SIZE, "DSD size");
    }

    void parse_header() {
        header = std::make_unique<MipasHeader>();
        parse_mph(0, header->mph);
        parse_sph(MPH_SIZE, header->sph);
        mmap.resize(header->mph.tot_size);
        for (size_t i = 0; i < DATA_SETS.size(); ++i) {
            size_t offset = MPH_SIZE + SPH_SIZE + i * DSD_SIZE;
            parse_dsd(offset, header->dsds[i]);
        }
    }

    SummaryQuality parse_summary_quality(const size_t offset, const size_t size) const {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MIP_NL__2P_ADSR_summary_quality.html
        SummaryQuality sq;
        BinaryReader r = get_binary_reader(offset);
        /* id */
        /* 0 */ sq.dsr_time = r.read_envisat_time();
        /* 1 */ sq.attach_flag = r.read_int<uint8_t>();
        /* 2 */ sq.p_t_term_macro_micro.macro = r.read_int<uint16_t>();
        /*   */ sq.p_t_term_macro_micro.micro = r.read_int<uint16_t>();
        /* 3 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*   */     sq.vmr_term_macro_micro[i].macro = r.read_int<uint16_t>();
        /*   */     sq.vmr_term_macro_micro[i].micro = r.read_int<uint16_t>();
        /*   */ }
        /* 4 */ sq.p_t_term_run_time = r.read_int<uint16_t>();
        /* 5 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*   */     sq.vmr_term_run_time[i] = r.read_int<uint16_t>();
        /*   */ }
        /* 6 */ r.advance(65);
        mipas_assert(r.get_size() == size, "summary quality size");
        return sq;
    }

    GeoLocation parse_geo_location(const size_t offset) const {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MIP_NL__2P_ADSR_geolocation_v1.html
        GeoLocation gl;
        BinaryReader r = get_binary_reader(offset);
        /* id */
        /*  0 */ gl.dsr_time = r.read_envisat_time();
        /*  1 */ gl.attach_flag = r.read_int<uint8_t>();
        /*  2 */ gl.loc_first = r.read_location_point();
        /*  3 */ gl.first_alt = r.read_float<double>();
        /*  4 */ gl.loc_last = r.read_location_point();
        /*  5 */ gl.last_alt = r.read_float<double>();
        /*  6 */ gl.loc_mid = r.read_location_point();
        /*  7 */ gl.local_solar_time = r.read_int<int32_t>();
        /*  8 */ gl.sat_target_azi = r.read_int<int32_t>();
        /*  9 */ gl.target_sun_azi = r.read_int<int32_t>();
        /* 10 */ gl.target_sun_elev = r.read_int<int32_t>();
        /* 11 */ r.advance(31);
        mipas_assert(r.get_size() == GEO_LOCATION_SIZE, "geolocation size");
        return gl;
    }

    void parse_ds_structure(const size_t offset, DSStructure& st) const {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MIP_NL__2P_ADSR_structure_v3.html
        BinaryReader r = get_binary_reader(offset);
        /* id */
        /*  0 */ st.dsr_time = r.read_envisat_time();
        /*  1 */ st.attach_flag = r.read_int<uint8_t>();
        /*  2 */ st.num_sweeps = r.read_int<uint16_t>();
        /*  3 */ st.num_p_t_pts = r.read_int<uint16_t>();
        /*  4 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_vmr_pts[i] = r.read_int<uint16_t>();
        /*    */ }
        /*  5 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.flags_p_t_error_flag[i] = r.read_int<uint16_t>();
        /*    */ }
        /*  6 */ st.num_con_params_p_t = r.read_int<uint16_t>();
        /*  7 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_con_params_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /*  8 */ st.num_instr_offset_p_t = r.read_int<uint16_t>();
        /*  9 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_instr_offset_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 10 */ st.max_num_micro_p_t = r.read_int<uint16_t>();
        /* 11 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.max_num_micro_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 12 */ st.tot_num_p_t_micro_all_alt = r.read_int<uint16_t>();
        /* 13 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.tot_num_vmr_micro_all_alt[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 14 */ st.tot_num_spect_grid_p_t = r.read_int<uint16_t>();
        /* 15 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.tot_num_spect_grid_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 16 */ st.num_grid_con_p_t = r.read_int<uint16_t>();
        /* 17 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_grid_con_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 18 */ st.num_evo_steps_p_t = r.read_int<uint16_t>();
        /* 19 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_evo_steps_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 20 */ st.num_pcd_info = r.read_int<uint16_t>();
        /* 21 */ st.num_base_p_t_pts = r.read_int<uint16_t>();
        /* 22 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_base_vmr_pts[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 23 */ st.num_mw_labels_p_t = r.read_int<uint16_t>();
        /* 24 */ for (size_t i = 0; i < 30; ++i) {
        /*    */     st.num_mw_labels_vmr[i] = r.read_int<uint16_t>();
        /*    */ }
        /* 25 */ for (size_t i = 0; i < 37; ++i) {
        /*    */     st.ds_pointer[i].dsr_offset = r.read_int<int32_t>();
        /*    */     st.ds_pointer[i].dsr_length = r.read_int<uint32_t>();
        /*    */ }
        /* 26 */ r.advance(27);
        mipas_assert(r.get_size() == DS_STRUCTURE_SIZE, "ds structure size");
    }

    void parse_scan_info(const size_t offset, ScanInfo& si) {
        // see http://www.stcorp.nl/beat/documentation/codadef/ENVISAT_MIPAS/types/MIP_NL__2P_MDSR_info_v2.html
        BinaryReader r = get_binary_reader(offset);
        /* id */
        /*  0 */ si.base_data().dsr_time = r.read_envisat_time();
        /*  1 */ si.base_data().dsr_length = r.read_int<uint32_t>();
        /*  2 */ si.base_data().quality_flag = r.read_int<int8_t>();
        /*  3 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     si.zpd_crossing_time()[i] = r.read_envisat_time();
        /*    */ }
        /*  4 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     si.geolocation_los_tangent()[i] = r.read_location_point();
        /*    */ }
        /*  5 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     si.tangent_altitude_los()[i] = r.read_float<double>();
        /*    */ }
        /*  6 */ si.base_data().appl_process_id = r.read_int<uint16_t>();
        /*  7 */ si.base_data().retrieval_p_t_flag = r.read_int<uint8_t>();
        /*  8 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*    */     si.base_data().retrieval_vmr_flag[i] = r.read_int<uint8_t>();
        /*    */ }
        /*  9 */ si.base_data().marq_p_t_flag = r.read_int<uint8_t>();
        /* 10 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*    */     si.base_data().marq_vmr_flag[i] = r.read_int<uint8_t>();
        /*    */ }
        /* 11 */ si.base_data().chi2_p_t_flag = r.read_int<uint8_t>();
        /* 12 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*    */     si.base_data().chi2_vmr_flag[i] = r.read_int<uint8_t>();
        /*    */ }
        /* 13 */ if (SPECIES_COUNT == 2) {
        /*    */     r.advance(52);
        /*    */ } else {
        /*    */     r.advance(40);
        /*    */ }
        /* 14 */ auto retrieval_p_t = si.retrieval_p_t();
        /*    */ /* 0 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.lrv_p_t_flag()[i] = r.read_int<uint8_t>();
        /*    */ /*   */ }
        /*    */ /* 1 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.pressure()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 2 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.pressure_variance()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 3 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.tangent_altitude()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 4 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.height_cor_variance()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 5 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.temp()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 6 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.temp_variance()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 7 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */ /*   */     retrieval_p_t.ecmwf_corr_altitude()[i] = r.read_float<float>();
        /*    */ /*   */ }
        /* 15 */ for (size_t i = 0; i < SPECIES_COUNT; ++i) {
        /*    */ /* 0 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).lrv_vmr_flag()[j] = r.read_int<uint8_t>();
        /*    */ /*   */ }
        /*    */ /* 1 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).vmr()[j] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 2 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).vmr_variance()[j] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 3 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).concentration()[j] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 4 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).concentration_variance()[j] = \
        /*    */ /*   */        r.read_float<double>();
        /*    */ /*   */ }
        /*    */ /* 5 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).vertical_col_density()[j] = r.read_float<float>();
        /*    */ /*   */ }
        /*    */ /* 6 */ for (size_t j = 0; j < si.get_num_sweeps(); ++j) {
        /*    */ /*   */     si.retrieval_vmr(i).vcd_variance()[j] = r.read_float<double>();
        /*    */ /*   */ }
        /*    */ }
        /* 16 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     for (size_t j = 0; j < 3; ++j) {
        /*    */         si.cloud_det_mw_label()[i][j] = r.read_fixed_string<8>();
        /*    */     }
        /*    */ }
        /* 17 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     for (size_t j = 0; j < 3; ++j) {
        /*    */         si.cloud_index()[i][j] = r.read_float<float>();
        /*    */     }
        /*    */ }
        /* 18 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     for (size_t j = 0; j < 3; ++j) {
        /*    */         si.cloud_index_threshold()[i][j] = r.read_float<float>();
        /*    */     }
        /*    */ }
        /* 19 */ for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
        /*    */     for (size_t j = 0; j < 3; ++j) {
        /*    */         si.cloud_detect_flag()[i][j] = r.read_int<uint8_t>();
        /*    */     }
        /*    */ }
        mipas_assert(r.get_size() == si.base_data().dsr_length, "scan info size");
    }

public:
    SummaryQuality get_summary_quality() const {
        const DSD& dsd = header->dsds[0];
        mipas_assert(dsd.ds_name == DATA_SETS[0], "summary quality DSD");
        mipas_assert(dsd.ds_offset >= 0, "summary quality ds_offset");
        mipas_assert(dsd.ds_size >= 0, "summary quality ds_size");
        return parse_summary_quality(dsd.ds_offset, dsd.ds_size);
    }
};


struct MipasFileImpl {
    Parser parser;

    MipasFileImpl(const std::string& filename) : parser(filename) {}
};


MipasFile::MipasFile(const std::string& filename) {
    impl = std::make_unique<MipasFileImpl>(filename);
}

MipasFile::~MipasFile() {}

const MipasHeader& MipasFile::get_header() const {
    return impl->parser.get_header();
}

SummaryQuality MipasFile::get_summary_quality() const {
    return impl->parser.get_summary_quality();
}


GeoLocationContainer::GeoLocationContainer(const MipasFile& file) : file(file) {
    const DSD& gl_dsd = file.get_header().dsds[1];
    mipas_assert(gl_dsd.ds_name == DATA_SETS[1], "geolocation DSD");
    mipas_assert(gl_dsd.num_dsr >= 0, "geolocation num_dsr");
    mipas_assert(gl_dsd.ds_offset >= 0, "geolocation ds_offset");
    mipas_assert(gl_dsd.dsr_size > 0, "geolocation dsr_size");
}

size_t GeoLocationContainer::size() const {
    const DSD& gl_dsd = file.get_header().dsds[1];
    return gl_dsd.num_dsr;
}

GeoLocation GeoLocationContainer::operator[](const size_t index) const {
    const DSD& gl_dsd = file.get_header().dsds[1];
    const size_t offset = gl_dsd.ds_offset + index * gl_dsd.dsr_size;
    return file.impl->parser.parse_geo_location(offset);
}


ScanInfoContainer::ScanInfoContainer(const MipasFile& file) : file(file) {
    const DSD& st_dsd = file.get_header().dsds[2];
    mipas_assert(st_dsd.ds_name == DATA_SETS[2], "ds structure DSD");
    mipas_assert(st_dsd.num_dsr >= 0, "ds structure num_dsr");
    mipas_assert(st_dsd.ds_offset >= 0, "ds structure ds_offset");
    mipas_assert(st_dsd.dsr_size > 0, "ds structure dsr_size");
    if (st_dsd.num_dsr == 0) {
        return;
    }
    const size_t num_st = static_cast<size_t>(st_dsd.num_dsr);
    const DSD& si_dsd = file.get_header().dsds[3];
    mipas_assert(si_dsd.ds_name == DATA_SETS[3], "scan info DSD");
    mipas_assert(si_dsd.num_dsr >= 0, "scan info num_dsr");
    mipas_assert(si_dsd.ds_offset >= 0, "scan info ds_offset");
    if (si_dsd.num_dsr == 0) {
        return;
    }
    const size_t num_si = static_cast<size_t>(si_dsd.num_dsr);
    num_sweeps = std::make_unique<uint16_t[]>(si_dsd.num_dsr);
    offset = std::make_unique<uint32_t[]>(si_dsd.num_dsr);
    {
        size_t st_index = 0;
        size_t si_index = 0;
        std::unique_ptr<DSStructure> st;
        std::unique_ptr<DSStructure> st_next = std::make_unique<DSStructure>();
        do {
            if (st_index >= num_st) {
                break;
            }
            file.impl->parser.parse_ds_structure(
                st_dsd.ds_offset + st_index * st_dsd.dsr_size, *st_next
            );
            ++st_index;
        } while (st_next->ds_pointer[0].dsr_offset == -1);
        mipas_assert(st_next->ds_pointer[0].dsr_offset >= 0, "scan info first DSR");
        while (st_index <= num_st) {
            st = std::move(st_next);
            if (st_index >= num_st) {
                break;
            }
            st_next = std::make_unique<DSStructure>();
            do {
                if (st_index >= num_st) {
                    break;
                }
                file.impl->parser.parse_ds_structure(
                    st_dsd.ds_offset + st_index * st_dsd.dsr_size, *st_next
                );
                ++st_index;
            } while (st_next->ds_pointer[0].dsr_offset == -1);
            if (st_index <= num_st) {
                mipas_assert(st_next->ds_pointer[0].dsr_offset >= 0, "scan info next dsr_offset");
                const size_t num_dsr = (
                    (st_next->ds_pointer[0].dsr_offset - st->ds_pointer[0].dsr_offset) /
                    st->ds_pointer[0].dsr_length
                );
                for (size_t i = 0; i < num_dsr; ++i) {
                    num_sweeps[si_index] = st->num_sweeps;
                    offset[si_index] = (
                        st->ds_pointer[0].dsr_offset + i * st->ds_pointer[0].dsr_length
                    );
                    ++si_index;
                }
            } else {
                st_next.reset(nullptr);
            }
        }
        mipas_assert(si_index >= num_si || static_cast<bool>(st), "scan info DSRs end");
        for (size_t i = 0; si_index < num_si; ++si_index, ++i) {
            num_sweeps[si_index] = st->num_sweeps;
            offset[si_index] = (st->ds_pointer[0].dsr_offset + i * st->ds_pointer[0].dsr_length);
        }
    }
}

size_t ScanInfoContainer::size() const {
    const DSD& si_dsd = file.get_header().dsds[3];
    return si_dsd.num_dsr;
}

ScanInfo ScanInfoContainer::operator[](const size_t index) const {
    const DSD& si_dsd = file.get_header().dsds[3];
    ScanInfo si{num_sweeps[index]};
    const size_t si_offset = si_dsd.ds_offset + offset[index];
    file.impl->parser.parse_scan_info(si_offset, si);
    return si;
}


std::ostream& operator<<(std::ostream& stream, const SummaryQuality& sq) {
    stream << "dsr_time:" << std::endl;
    stream << "    days: " << sq.dsr_time.days << std::endl;
    stream << "    seconds: " << sq.dsr_time.seconds << std::endl;
    stream << "    microseconds: " << sq.dsr_time.microseconds << std::endl;
    stream << "attach_flag: " << +sq.attach_flag << std::endl;
    stream << "p_t_term_macro_micro:" << std::endl;
    stream << "    macro: " << sq.p_t_term_macro_micro.macro << std::endl;
    stream << "    micro: " << sq.p_t_term_macro_micro.micro << std::endl;
    stream << "p_t_term_run_time: " << sq.p_t_term_run_time;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const LocationPoint& lp) {
    stream << "latitude: " << utils::fixed_float(lp.latitude) << 'N' << \
        " longitude: " << utils::fixed_float(lp.longitude) << 'E';
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const GeoLocation& gl) {
    stream << "dsr_time:" << std::endl;
    stream << "    days: " << gl.dsr_time.days << std::endl;
    stream << "    seconds: " << gl.dsr_time.seconds << std::endl;
    stream << "    microseconds: " << gl.dsr_time.microseconds << std::endl;
    stream << "attach_flag: " << +gl.attach_flag << std::endl;
    stream << "loc_first: " << gl.loc_first << std::endl;
    stream << "first_alt: " << gl.first_alt << "km" << std::endl;
    stream << "loc_last: " << gl.loc_last << std::endl;
    stream << "last_alt: " << gl.last_alt << "km" << std::endl;
    stream << "loc_mid: " << gl.loc_mid << std::endl;
    stream << "local_solar_time: " << utils::fixed_float(gl.local_solar_time) << 'h' << std::endl;
    stream << "sat_target_azi: " << utils::fixed_float(gl.sat_target_azi) << "deg" << std::endl;
    stream << "target_sun_azi: " << utils::fixed_float(gl.target_sun_azi) << "deg" << std::endl;
    stream << "target_sun_elev: " << utils::fixed_float(gl.target_sun_elev) << "deg";
    return stream;
}

}
