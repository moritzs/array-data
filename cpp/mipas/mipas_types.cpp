#include "mipas_types.hpp"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>


namespace mipas {

std::string EnvisatTime::to_iso() const {
    using namespace boost;
    auto date = gregorian::date{2000, 1, 1} + gregorian::date_duration{days};
    posix_time::ptime dt{
        date, posix_time::seconds(seconds) + posix_time::microseconds(microseconds)
    };
    return to_iso_extended_string(dt);
}


EnvisatTime EnvisatTime::from_date(
    uint16_t year, uint8_t month, uint8_t day, uint8_t hours, uint8_t minutes, uint8_t seconds
) {
    using namespace boost;
    EnvisatTime t;
    auto duration = gregorian::date{year, month, day} - gregorian::date{2000, 1, 1};
    t.days = duration.days();
    auto time = (
        posix_time::hours(hours) + posix_time::minutes(minutes) + posix_time::seconds(seconds)
    );
    t.seconds = time.total_seconds();
    t.microseconds = 0;
    return t;
}


}
