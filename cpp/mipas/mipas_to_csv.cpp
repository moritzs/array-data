#include <iostream>
#include "exceptions.hpp"
#include "mipas.hpp"
#include "utils.hpp"


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
        return 2;
    }
    try {
        mipas::MipasFile m(argv[1]);
        const std::string& species = m.get_header().sph.order_of_species;
        size_t O3_index = 0;
        {
            size_t last = 0;
            for (size_t i = 1; i < species.length() && species[i] != ' '; ++i) {
                if (species[i] == ',') {
                    if (species.substr(last, i - last) == "O3") {
                        break;
                    }
                    ++O3_index;
                    last = i + 1;
                    ++i;
                }
            }
        }
        for (const auto& si : m.get_scan_infos()) {
            const auto pt = si.retrieval_p_t();
            const auto vmr = si.retrieval_vmr(O3_index);
            for (size_t i = 0; i < si.get_num_sweeps(); ++i) {
                std::cout << si.zpd_crossing_time()[i].to_iso() << ';';
                std::cout << utils::fixed_float(si.geolocation_los_tangent()[i].latitude) << ';';
                std::cout << utils::fixed_float(si.geolocation_los_tangent()[i].longitude) << ';';
                std::cout << si.tangent_altitude_los()[i] << ';';
                std::cout << pt.pressure()[i] << ';';
                std::cout << pt.pressure_variance()[i] << ';';
                std::cout << pt.temp()[i] << ';';
                std::cout << pt.temp_variance()[i] << ';';
                std::cout << vmr.vmr()[i] << ';';
                std::cout << vmr.vmr_variance()[i] << ';';
                std::cout << vmr.concentration()[i] << ';';
                std::cout << vmr.concentration_variance()[i] << std::endl;
            }
        }
    } catch (mipas::parse_error& e) {
        std::cerr << "parse error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
