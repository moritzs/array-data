#ifndef _MIPAS_EXCEPTIONS_HPP
#define _MIPAS_EXCEPTIONS_HPP

#include <exception>
#include <sstream>
#include <string>


namespace mipas {

class parse_error
: public std::exception {
protected:
    std::string message;

public:
    parse_error() = default;
    template <typename T>
    parse_error(T&& message) : message{message} {}

    virtual const char* what() const noexcept {
        return message.c_str();
    }
};


class unexpected_string
: public parse_error {
public:
    const size_t offset;
    const std::string expected;

public:
    unexpected_string(const size_t offset, const std::string expected)
    : offset(offset), expected(expected) {
        std::stringstream msg;
        msg << "Expected '" << expected << "' at offset " << offset;
        message = msg.str();
    }
};


class parse_number_error
: public parse_error {
public:
    const size_t offset;

    parse_number_error(const size_t offset, const std::string& error) : offset(offset) {
        std::stringstream msg;
        msg  << error << " at offset " << offset;
        message = msg.str();
    }
};


class file_incosistency
: public parse_error {
public:
    file_incosistency(const char* message) : parse_error(message) {}
};

}

#endif
