#include <random>
#include <sstream>
#include <vector>
#include "gtest/gtest.h"
#include "utils.hpp"


TEST(utils, median) {
    std::vector<int> values;
    values.reserve(1000);
    for (int i = 0; i < 1000; ++i) {
        values.push_back(i);
    }
    std::mt19937 rand_gen(0);
    for (size_t i = 1; i <= 500; ++i) {
        std::stringstream msg;
        msg << i << ". iteration";
        SCOPED_TRACE(msg.str());
        std::shuffle(values.begin(), values.end(), rand_gen);
        auto median = *utils::median(values.begin(), values.end());
        EXPECT_GE(median, 300);
        EXPECT_LT(median, 700);
    }
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
