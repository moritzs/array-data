#include "gtest/gtest.h"
#include "column_store.hpp"


TEST(column_store, reference) {
    column_store<int, char> data;
    data.push_back(1, 'a');
    data.push_back(2, 'b');

    auto ref1 = data[0];
    auto ref2 = data[1];
    EXPECT_EQ(1, ref1.get<0>());
    EXPECT_EQ('a', ref1.get<1>());
    EXPECT_EQ(2, ref2.get<0>());
    EXPECT_EQ('b', ref2.get<1>());

    ref1.get<0>() = 5;
    ref2.get<1>() = 'q';
    EXPECT_EQ(5, data.get<0>(0));
    EXPECT_EQ('q', data.get<1>(1));

    auto cref = static_cast<const column_store<int, char>&>(data)[0];
    EXPECT_EQ(5, cref.get<0>());
    EXPECT_EQ('a', cref.get<1>());

    auto pointer = &ref1;
    EXPECT_EQ(5, pointer->get<0>());
    EXPECT_EQ('a', pointer->get<1>());

    pointer->get<1>() = 'x';
    EXPECT_EQ('x', ref1.get<1>());
}


TEST(column_store, reference_assignment) {
    column_store<int, char> data1;
    data1.push_back(1, 'a');
    column_store<int, char> data2;
    data2.push_back(2, 'b');
    column_store<int, char> data3;
    data3.push_back(3, 'c');
    const column_store<int, char>& data3_const = data3;
    data1[0] = data2[0];
    EXPECT_EQ(2, data1[0].get<0>());
    EXPECT_EQ('b', data1[0].get<1>());
    data1[0] = data3_const[0];
    EXPECT_EQ(3, data1[0].get<0>());
    EXPECT_EQ('c', data1[0].get<1>());
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
