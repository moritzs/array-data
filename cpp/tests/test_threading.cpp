#include "gtest/gtest.h"
#include <atomic>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>
#include "threading.hpp"


TEST(threading, barrier_deadlock) {
    const size_t num_threads = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    std::stringstream numbers;
    threading::spin_lock mutex;
    threading::barrier<threading::spin_lock> barrier{num_threads};
    for (size_t i = 0; i < num_threads; ++i) {
        threads.emplace_back([&mutex, &numbers, &barrier] {
            for (int j = 1; j <= 10; ++j) {
                {
                    std::unique_lock<threading::spin_lock> lock{mutex};
                    numbers << j << ';';
                }
                barrier.wait();
            }
        });
    }
    for (auto& t : threads) {
        t.join();
    }
    std::stringstream expected_numbers;
    for (int j = 1; j <= 10; ++j) {
        for (size_t i = 0; i < num_threads; ++i) {
            expected_numbers << j << ';';
        }
    }
    ASSERT_EQ(expected_numbers.str(), numbers.str());
}


TEST(threading, barrier_run_once) {
    const size_t num_threads = std::thread::hardware_concurrency();
    std::atomic<size_t> counter;
    std::atomic_init(&counter, 0ul);
    std::vector<std::thread> threads;
    threading::barrier<threading::spin_lock> barrier{num_threads};
    for (size_t i = 0; i < num_threads; ++i) {
        threads.emplace_back([&counter, &barrier] {
            for (int j = 0; j < 1000; ++j) {
                barrier.wait_run_once([&counter] { ++counter; });
            }
        });
    }
    for (auto& t : threads) {
        t.join();
    }
    ASSERT_EQ(1000, counter.load());
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
