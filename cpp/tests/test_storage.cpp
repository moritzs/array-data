#include <algorithm>
#include <cmath>
#include <iterator>
#include <random>
#include <sstream>
#include <thread>
#include <set>
#include <tuple>
#include <vector>
#include "gtest/gtest.h"
#include "column_store.hpp"
#include "storage/storage_dense_image.hpp"
#include "storage/storage_sparse_image.hpp"
#include "storage/btree.hpp"


TEST(storage_dense, construct_from_column_store) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    data.push_back(2, 0, 0, 30, 99, 99);
    data.push_back(1, 1, 0, 20, 99, 99);
    data.push_back(1, 0, 0, 10, 99, 99);
    data.push_back(2, 1, 0, 40, 99, 99);
    storage_dense_image storage{data};
    auto& storage_data = storage.get_data();
    std::vector<uint8_t> linearized_data;
    for (size_t i = 0; i < storage_data.size(); ++i) {
        linearized_data.push_back(storage_data.get<0>(i));
    }
    std::vector<uint8_t> expected_data{10, 20, 30, 40};
    ASSERT_EQ(expected_data, linearized_data);
}


TEST(storage_dense, iterator) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    data.push_back(123, 0, 0, 10, 99, 99);
    data.push_back(123, 1, 0, 20, 99, 99);
    data.push_back(124, 0, 0, 30, 99, 99);
    data.push_back(124, 1, 0, 40, 99, 99);
    storage_dense_image storage{data};
    const storage_dense_image& const_storage = storage;
    std::vector<uint32_t> ids;
    std::vector<uint16_t> xs;
    std::vector<uint16_t> ys;
    std::vector<uint8_t> values;
    for (auto image_id_dim : storage) {
        for (auto x_dim : image_id_dim.second) {
            for (auto y_dim : x_dim.second) {
                ids.push_back(image_id_dim.first);
                xs.push_back(x_dim.first);
                ys.push_back(y_dim.first);
                values.push_back(y_dim.second.get<0>());
            }
        }
    }
    std::vector<uint32_t> expected_ids{123, 123, 124, 124};
    std::vector<uint16_t> expected_xs{0, 1, 0, 1};
    std::vector<uint16_t> expected_ys{0, 0, 0, 0};
    std::vector<uint8_t> expected_values{10, 20, 30, 40};
    EXPECT_EQ(expected_ids, ids);
    EXPECT_EQ(expected_xs, xs);
    EXPECT_EQ(expected_ys, ys);
    EXPECT_EQ(expected_values, values);
    auto const_it = const_storage.end();
    --const_it;
    EXPECT_EQ(124, const_it->first);
    ++const_it;
    EXPECT_EQ(const_storage.end(), const_it);
    std::advance(const_it, -2);
    EXPECT_EQ(123, const_it->first);
    EXPECT_EQ(const_storage.begin(), const_it);
}


TEST(storage_dense, operator_brackets) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    data.push_back(123, 0, 0, 99, 99, 10);
    data.push_back(123, 1, 0, 99, 99, 20);
    data.push_back(124, 0, 0, 99, 99, 30);
    data.push_back(124, 1, 0, 99, 99, 40);
    storage_dense_image storage{data};
    const storage_dense_image& const_storage = storage;
    EXPECT_EQ(10, storage[123][0][0].get<2>());
    EXPECT_EQ(20, storage[123][1][0].get<2>());
    EXPECT_EQ(30, storage[124][0][0].get<2>());
    EXPECT_EQ(40, storage[124][1][0].get<2>());
    EXPECT_EQ(10, const_storage[123][0][0].get<2>());
    EXPECT_EQ(20, const_storage[123][1][0].get<2>());
    EXPECT_EQ(30, const_storage[124][0][0].get<2>());
    EXPECT_EQ(40, const_storage[124][1][0].get<2>());
    storage[124][0][0].get<1>() = 100;
    EXPECT_EQ(100, storage[124][0][0].get<1>());
    EXPECT_EQ(100, const_storage[124][0][0].get<1>());
}


TEST(storage_dense, load_threaded) {
    constexpr size_t NUM_I = 3;
    constexpr size_t NUM_X = 10;
    constexpr size_t NUM_Y = 8;
    constexpr size_t TOTAL_SIZE = NUM_I * NUM_X * NUM_Y;
    constexpr size_t NUM_TESTS = 10;
    const size_t num_threads = std::thread::hardware_concurrency();
    using values_t = std::vector<
        std::tuple<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t>
    >;

    values_t values;
    uint8_t val = 0;
    for (uint32_t i = 0; i < NUM_I; ++i) {
        for (uint16_t x = 0; x < NUM_X; ++x) {
            for (uint16_t y = 0; y < NUM_Y; ++y) {
                values.emplace_back(i, x, y, val, val, val);
                ++val;
            }
        }
    }
    std::vector<uint8_t> expected_data(TOTAL_SIZE);
    for (size_t i = 0; i < TOTAL_SIZE; ++i) {
        expected_data[i] = i;
    }

    std::mt19937 rand_gen(0);
    for (size_t test_id = 0; test_id < NUM_TESTS; ++test_id) {
        std::stringstream msg;
        msg << test_id << ". iteration";
        SCOPED_TRACE(msg.str());
        values_t shuffled_values(values);
        std::shuffle(shuffled_values.begin(), shuffled_values.end(), rand_gen);

        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (auto& val : shuffled_values) {
            data.push_back(val);
        }

        storage_dense_image storage;
        auto threaded_loader = storage.load_threaded(data, num_threads);
        std::vector<std::thread> threads;
        for (size_t i = 0; i < num_threads; ++i) {
            threads.emplace_back([i, &threaded_loader] {
                threaded_loader(i);
            });
        }
        for (auto& t : threads) {
            t.join();
        }

        auto& storage_data = storage.get_data();
        std::vector<uint8_t> linearized_data;
        for (size_t i = 0; i < storage_data.size(); ++i) {
            linearized_data.push_back(storage_data.get<0>(i));
        }
        EXPECT_EQ(expected_data, linearized_data);
    }
}


TEST(storage_sparse, construct_from_column_store) {
    std::mt19937 rand_gen(0);
    std::vector<uint8_t> input_data;
    std::set<uint8_t> expected_data;
    for (size_t i = 1; i <= 30; ++i) {
        input_data.push_back(i);
        expected_data.insert(100 + i);
    }
    std::shuffle(input_data.begin(), input_data.end(), rand_gen);
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    for (uint8_t i : input_data) {
        data.push_back(i, i * 2, i * 3, 100 + i, 0, 0);
    }
    storage_sparse_image storage{data};
    auto& storage_data = storage.get_data();
    std::set<uint8_t> storage_values;
    for (size_t i = 0; i < storage_data.size(); ++i) {
        storage_values.insert(storage_data.get<0>(i));
    }
    ASSERT_EQ(expected_data, storage_values);
}


TEST(storage_sparse, duplicates_in_range) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    data.push_back(2, 0, 0, 30, 99, 99);
    data.push_back(1, 1, 0, 20, 99, 99);
    data.push_back(1, 0, 0, 10, 99, 99);
    data.push_back(2, 1, 0, 40, 99, 99);
    storage_sparse_image storage{data};
    auto& storage_data = storage.get_data();
    std::set<uint8_t> storage_values;
    for (size_t i = 0; i < storage_data.size(); ++i) {
        storage_values.insert(storage_data.get<0>(i));
    }
    std::set<uint8_t> expected_values{10, 20, 30, 40};
    ASSERT_EQ(expected_values, storage_values);
}


TEST(storage_sparse, iterator) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    std::set<uint32_t> expected_ids;
    std::set<uint8_t> expected_values;
    for (size_t i = 1; i <= 30; ++i) {
        data.push_back(i, (i * 2) % 5, 0, 100 + i, 0, 0);
        expected_ids.insert(i);
        expected_values.insert(100 + i);
    }
    storage_sparse_image storage{data};
    std::set<uint32_t> ids;
    std::set<uint8_t> values;
    for (auto id_range : storage) {
        for (auto x_range : id_range) {
            for (auto tuple_ref : x_range) {
                ids.insert(tuple_ref.get<3>());
                values.insert(tuple_ref.get<0>());
            }
        }
    }
    EXPECT_EQ(expected_ids, ids);
    EXPECT_EQ(expected_values, values);
}


TEST(storage_sparse, operator_brackets) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    for (size_t i = 1; i <= 30; ++i) {
        data.push_back(i, (i * 2) % 5, 0, 100 + i, 0, 0);
    }
    storage_sparse_image storage{data};
    const storage_sparse_image& const_storage = storage;
    for (size_t i = 1; i <= 30; ++i) {
        std::stringstream msg;
        msg << "i = " << i;
        SCOPED_TRACE(msg.str());
        EXPECT_EQ(100 + i, storage[i][(i * 2) % 5][0].get<0>());
        EXPECT_EQ(100 + i, const_storage[i][(i * 2) % 5][0].get<0>());
    }
    storage[14][3][0].get<1>() = 7;
    EXPECT_EQ(7, storage[14][3][0].get<1>());
    EXPECT_EQ(7, const_storage[14][3][0].get<1>());
}


TEST(storage_sparse, range_split) {
    {
        SCOPED_TRACE("split in dimension 1");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (size_t i = 0; i < 210; ++i) {
            data.push_back(i, i, i, 99, 99, 99);
        }
        storage_sparse_image storage{data};
        EXPECT_EQ(210, std::distance(storage.link_begin(), storage.link_end()));
    }
    {
        SCOPED_TRACE("split in dimension 2");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (size_t i = 0; i < 1010; ++i) {
            data.push_back(i, i, i, 99, 99, 99);
        }
        storage_sparse_image storage{data};
        EXPECT_EQ(1010, std::distance(storage.link_begin(), storage.link_end()));
    }
    {
        SCOPED_TRACE("multiple splits in dimension 2");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (size_t i = 0; i < 11000; ++i) {
            data.push_back(i, i, i, 99, 99, 99);
        }
        storage_sparse_image storage{data};
        EXPECT_EQ(11000, std::distance(storage.link_begin(), storage.link_end()));
    }
    {
        SCOPED_TRACE("split range where median is equal to max");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        data.push_back(10, 0, 0, 99, 99, 99);
        for (size_t i = 1; i < 1001; ++i) {
            data.push_back(20, i, i, 99, 99, 99);
        }
        storage_sparse_image storage{data};
        EXPECT_EQ(1001, std::distance(storage.link_begin(), storage.link_end()));
    }
}


TEST(storage_sparse, find_greater_eq) {
    column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
    for (uint32_t i : {5, 10, 15, 20}) {
        for (uint16_t x = 0; x < 80; ++x) {
            for (uint16_t y = 0; y < 80; ++y) {
                data.push_back(i, x, y, 99, 0, 0);
            }
        }
    }
    storage_sparse_image storage{data};
    {
        SCOPED_TRACE("first dimension");
        EXPECT_EQ(15, storage.find_greater_eq(12)->min());
        EXPECT_EQ(10, storage.find_greater_eq(10)->min());
        EXPECT_EQ(5, storage.find_greater_eq(2)->min());
        EXPECT_EQ(20, storage.find_greater_eq(21)->max());
    }
    {
        SCOPED_TRACE("second dimension");
        auto range = storage.find_greater_eq(15);
        for (uint16_t x = 0; x < 80; ++x) {
            auto x_range = range->find_greater_eq(x);
            EXPECT_LE(x_range->min(), x);
            EXPECT_GE(x_range->max(), x);
        }
        auto x_range = range->find_greater_eq(100);
        EXPECT_LE(x_range->min(), 79);
        EXPECT_GE(x_range->max(), 79);
    }
    {
        SCOPED_TRACE("third dimension");
        auto range = storage.find_greater_eq(15)->find_greater_eq(30);
        for (uint16_t y = 0; y < 80; ++y) {
            EXPECT_EQ(y, range->find_greater_eq(y)->get<5>());
        }
        auto next_range = range++;
        EXPECT_EQ(next_range->begin(), range->find_greater_eq(100));
    }
}


TEST(storage_sparse, element_order) {
    auto run_test = [](storage_sparse_image& storage) {
        EXPECT_EQ(20 * 80 * 80, std::distance(storage.link_begin(), storage.link_end()));
        {
            SCOPED_TRACE("sorted runs");
            auto it = storage.link_begin();
            auto link_end = storage.link_end();
            uint16_t last_y = it->get<5>();
            ++it;
            size_t run_length = 1;
            std::set<uint16_t> xs;
            for (; it != link_end; ++it) {
                std::stringstream msg;
                msg << "i = " << it->get<3>() << ", " \
                    "x = " << it->get<4>() << ", " \
                    "y = " << it->get<5>();
                SCOPED_TRACE(msg.str());
                uint16_t y = it->get<5>();
                if (y >= last_y) {
                    ++run_length;
                    xs.insert(it->get<4>());
                } else {
                    bool failed = false;
                    if (run_length < 80) {
                        ADD_FAILURE() << "run_length (" << run_length << ") < 80";
                        failed = true;
                    }
                    if (xs.size() > 2) {
                        ADD_FAILURE() << "number of xs (" << xs.size() << ") > 2";
                        failed = true;
                    }
                    if (failed) {
                        break;
                    }
                    run_length = 1;
                    xs.clear();
                }
                last_y = y;
            }
        }
        {
            SCOPED_TRACE("sorted image ids");
            auto it = storage.link_begin();
            auto link_end = storage.link_end();
            uint32_t last_id = it->get<3>();
            uint16_t last_x = it->get<4>();
            uint16_t last_y = it->get<5>();
            ++it;
            for (; it != link_end; ++it) {
                uint32_t id = it->get<3>();
                uint16_t x = it->get<4>();
                uint16_t y = it->get<5>();
                if (id < last_id) {
                    ADD_FAILURE() << "unsorted pair of image ids:" << std::endl << \
                        "last pair: i = " << last_id << ", x = " << last_x << \
                            ", y = " << last_y << std::endl << \
                        "next pair: i = " << id << ", x = " << x << ", y = " << y;
                    break;
                }
                last_id = id;
                last_x = x;
                last_y = y;
            }
        }
    };

    std::vector<std::tuple<uint32_t, uint16_t, uint16_t>> pixels;
    for (uint32_t i = 0; i < 20; ++i) {
        for (uint16_t x = 0; x < 80; ++x) {
            for (uint16_t y = 0; y < 80; ++y) {
                pixels.emplace_back(i, x, y);
            }
        }
    }

    {
        SCOPED_TRACE("sorted");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (const auto& pixel : pixels) {
            data.push_back(
                std::get<0>(pixel), std::get<1>(pixel), std::get<2>(pixel),
                99, 99, 99
            );
        }
        storage_sparse_image storage{data};
        run_test(storage);
    }
    {
        SCOPED_TRACE("reverse sorted");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (auto pixel_it = pixels.rbegin(); pixel_it != pixels.rend(); ++pixel_it) {
            data.push_back(
                std::get<0>(*pixel_it), std::get<1>(*pixel_it), std::get<2>(*pixel_it),
                99, 99, 99
            );
        }
        storage_sparse_image storage{data};
        run_test(storage);
    }
    {
        SCOPED_TRACE("shuffled");
        std::mt19937 rand_gen(0);
        std::shuffle(pixels.begin(), pixels.end(), rand_gen);
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (const auto& pixel : pixels) {
            data.push_back(
                std::get<0>(pixel), std::get<1>(pixel), std::get<2>(pixel),
                99, 99, 99
            );
        }
        storage_sparse_image storage{data};
        run_test(storage);
    }
    {
        SCOPED_TRACE("cleanup");
        column_store<uint32_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t> data;
        for (const auto& pixel : pixels) {
            data.push_back(
                std::get<0>(pixel), std::get<1>(pixel), std::get<2>(pixel),
                99, 99, 99
            );
        }
        storage_sparse_image storage{data};
        storage.cleanup();
    }
}


TEST(btree, bulk_load) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    std::vector<size_t> sizes{4, 5, 24, 101, 2500, 2501, 12500, 12501};
    std::vector<int> keys;
    keys.reserve(12501);
    for (size_t i = 1; i <= 12501; ++i) {
        keys.push_back(i);
    }
    for (const size_t size: sizes) {
        std::stringstream msg;
        msg << "Size: " << size;
        SCOPED_TRACE(msg.str());
        auto tree = Tree::bulk_load(keys.begin(), keys.begin(), size);
        double lower_bound = std::log(size) / std::log(2 * K + 1);
        double upper_bound = std::log(size) / std::log(K);
        EXPECT_GE(tree.get_depth(), lower_bound);
        EXPECT_LE(tree.get_depth(), upper_bound);
        EXPECT_TRUE(tree.check_consistency());
        std::vector<int> tree_values;
        for (auto elem : tree) {
            tree_values.push_back(elem.second);
        }
        EXPECT_TRUE(std::equal(tree_values.begin(), tree_values.end(), keys.begin()));
    }
}


TEST(btree, iterators) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    {
        SCOPED_TRACE("empty tree");
        Tree tree;
        EXPECT_EQ(tree.begin(), tree.end());
        EXPECT_EQ(tree.cbegin(), tree.cend());
    }
    {
        SCOPED_TRACE("empty const tree");
        const Tree const_tree;
        EXPECT_EQ(const_tree.begin(), const_tree.end());
        EXPECT_EQ(const_tree.cbegin(), const_tree.cend());
    }
    {
        SCOPED_TRACE("tree size 4");
        std::vector<int> keys{1, 2, 3, 4};
        auto tree = Tree::bulk_load(keys.begin(), keys.rbegin(), 4);
        EXPECT_EQ(4, tree.begin()->second);
        EXPECT_EQ(3, (++tree.begin())->second);
        EXPECT_EQ(4, std::distance(tree.begin(), tree.end()));
        EXPECT_EQ(1, (--tree.end())->second);
    }
    {
        SCOPED_TRACE("tree size 5");
        std::vector<int> keys{1, 2, 3, 4, 5};
        auto tree = Tree::bulk_load(keys.begin(), keys.rbegin(), 5);
        EXPECT_EQ(5, tree.begin()->second);
        EXPECT_EQ(5, std::distance(tree.begin(), tree.end()));
        EXPECT_EQ(1, (--tree.end())->second);
        auto it = tree.begin();
        std::advance(it, 2);
        EXPECT_EQ(3, it->second);
        ++it;
        EXPECT_EQ(2, it->second);
        --it;
        EXPECT_EQ(3, it->second);
    }
}


TEST(btree, find) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    {
        SCOPED_TRACE("empty tree");
        Tree tree;
        EXPECT_EQ(tree.end(), tree.find(1));
    }
    {
        SCOPED_TRACE("empty const tree");
        const Tree const_tree;
        EXPECT_EQ(const_tree.end(), const_tree.find(1));
    }
    std::vector<int> keys{1, 2, 3, 4, 5};
    for (const size_t size: {4, 5}) {
        std::stringstream msg;
        msg << "Size: " << size;
        SCOPED_TRACE(msg.str());
        auto tree = Tree::bulk_load(keys.begin(), keys.rbegin(), size);
        auto values = keys.rbegin();
        for (size_t i = 1; i <= size; ++i) {
            EXPECT_EQ(*values, tree.find(i)->second);
            ++values;
        }
        EXPECT_EQ(tree.end(), tree.find(0));
        EXPECT_EQ(tree.end(), tree.find(size + 1));
    }
}


TEST(btree, find_greater_eq) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    {
        SCOPED_TRACE("empty tree");
        Tree tree;
        EXPECT_EQ(tree.end(), tree.find_greater_eq(1));
    }
    {
        SCOPED_TRACE("empty const tree");
        const Tree const_tree;
        EXPECT_EQ(const_tree.end(), const_tree.find_greater_eq(1));
    }

    std::vector<int> keys{2, 4, 6, 8, 10};
    auto tree = Tree::bulk_load(keys.begin(), keys.rbegin(), 5);
    EXPECT_EQ(tree.find(4), tree.find_greater_eq(4));
    EXPECT_EQ(tree.find(6), tree.find_greater_eq(5));
    EXPECT_EQ(tree.find(8), tree.find_greater_eq(7));
    EXPECT_EQ(tree.find(10), tree.find_greater_eq(10));
    EXPECT_EQ(tree.end(), tree.find_greater_eq(11));
}


TEST(btree, insert) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    std::vector<int> sizes{4, 5, 16, 17, 53, 161, 485};
    std::vector<int> reverse_sizes{4, 5, 12, 13, 37, 109, 325};

    for (const int size: sizes) {
        std::stringstream msg;
        msg << "Size: " << size;
        SCOPED_TRACE(msg.str());
        Tree tree;
        for (int i = 1; i <= size; ++i) {
            std::stringstream msg;
            msg << "Insert key: " << i;
            SCOPED_TRACE(msg.str());
            auto it = tree.insert(i, 100 + i);
            if (!tree.check_consistency()) {
                ADD_FAILURE() << "incosistent tree";
                goto outer_for_end;
            }
            EXPECT_EQ(100 + i, it->second);
        }
        {
            auto end = tree.end();
            for (int i = 1; i <= size; ++i) {
                std::stringstream msg;
                msg << "Find key: " << i;
                SCOPED_TRACE(msg.str());
                auto result = tree.find(i);
                if (result == end) {
                    ADD_FAILURE() << "key not found";
                } else {
                    EXPECT_EQ(100 + i, result->second);
                }
            }
        }
        outer_for_end:;
    }

    for (const int size: reverse_sizes) {
        std::stringstream msg;
        msg << "Backwards, size: " << size;
        SCOPED_TRACE(msg.str());
        Tree tree;
        for (int i = size; i >= 1; --i) {
            std::stringstream msg;
            msg << "Insert key: " << i;
            SCOPED_TRACE(msg.str());
            auto it = tree.insert(i, 100 + i);
            if (!tree.check_consistency()) {
                ADD_FAILURE() << "incosistent tree";
                goto backwards_outer_for_end;
            }
            EXPECT_EQ(100 + i, it->second);
        }
        {
            auto end = tree.end();
            for (int i = 1; i <= size; ++i) {
                std::stringstream msg;
                msg << "Find key: " << i;
                SCOPED_TRACE(msg.str());
                auto result = tree.find(i);
                if (result == end) {
                    ADD_FAILURE() << "key not found";
                } else {
                    EXPECT_EQ(100 + i, result->second);
                }
            }
        }
        backwards_outer_for_end:;
    }

    {
        SCOPED_TRACE("iterate tree");
        Tree tree;
        std::vector<int> expected_values;
        expected_values.reserve(53);
        for (int i = 1; i <= 53; ++i) {
            tree.insert(i, 100 + i);
            expected_values.push_back(100 + i);
        }
        EXPECT_EQ(53, std::distance(tree.begin(), tree.end()));
        std::vector<int> values;
        for (auto elem : tree) {
            values.push_back(elem.second);
        }
        EXPECT_EQ(expected_values, values);
    }

    {
        SCOPED_TRACE("depth 2 vector tree");
        using VectorTree = BTree<int, std::vector<int>, K, K>;
        VectorTree tree;
        std::vector<int> v{1, 2, 3, 4};
        tree.insert(4, std::vector<int>{});
        tree.insert(3, std::vector<int>{});
        tree.insert(2, std::vector<int>{});
        tree.insert(1, v);
        EXPECT_EQ(1, tree.get_depth());
        EXPECT_EQ(v, tree.begin()->second);
        v[0] = 100;
        tree.insert(1, v);
        EXPECT_EQ(1, tree.get_depth());
        EXPECT_EQ(v, tree.begin()->second);
    }
}


TEST(btree, insert_not_copyable) {
    struct Value {
        int v;

        Value() : v(0) {}
        Value(const int v) : v(v) {}
        Value(const Value&) = delete;
        Value(Value&&) = default;

        Value& operator=(Value&& other) {
            v = other.v;
            return *this;
        }
    };

    constexpr size_t K = 2;
    using Tree = BTree<int, Value, K, K>;

    {
        SCOPED_TRACE("insert forwards");
        Tree tree;
        for (int i = 1; i <= 100; ++i) {
            tree.insert(i, Value{i * 100});
        }
        EXPECT_EQ(100, tree.find(1)->second.v);
        EXPECT_EQ(2000, tree.find(20)->second.v);
        EXPECT_EQ(7500, tree.find(75)->second.v);
        EXPECT_EQ(10000, tree.find(100)->second.v);
    }
    {
        SCOPED_TRACE("insert backwards");
        Tree tree;
        for (int i = 100; i >= 1; --i) {
            tree.insert(i, Value{i * 100});
        }
        EXPECT_EQ(100, tree.find(1)->second.v);
        EXPECT_EQ(2000, tree.find(20)->second.v);
        EXPECT_EQ(7500, tree.find(75)->second.v);
        EXPECT_EQ(10000, tree.find(100)->second.v);
    }
}


TEST(btree, decrement_insert_iterator) {
    constexpr size_t K = 2;
    using Tree = BTree<int, int, K, K>;

    Tree tree;
    tree.insert(2, 20);
    tree.insert(4, 40);
    tree.insert(6, 60);
    tree.insert(8, 80);
    auto it = tree.insert(7, 70);
    ASSERT_EQ(60, (--it)->second);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
